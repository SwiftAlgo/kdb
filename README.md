# README #

Currently implemented are:

* kdb-api contains the KDB Q domain objects (QInt, QChar, QDict etc.) and their array counterparts (QIntV, QCharV, QDictV etc.).  These also encapsulate the KDB wire binary protocol format ie. each type knows how to write itself to a ByteBuffer.

* kdb-parser contains the ANTLR 4 parser for simple Q expressions. The grammar is in Q.g4.  The best way to see the usage is to examine the QxxxParserTest unit tests eg. QBooleanParserTest.

I promise to build a REPL soon!

Gordon

//PS Below is TODO.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact