package com.swiftalgo.kdb.q.parser;

/*-
 * #%L
 * KDB+ Parser
 * %%
 * Copyright (C) 2017 SwiftAlgo Limited
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.swiftalgo.kdb.q.QType;
import com.swiftalgo.kdb.q.ToStringContext;
import com.swiftalgo.kdb.q.factory.DefaultQFactory;
import com.swiftalgo.kdb.q.factory.QFactory;
import com.swiftalgo.util.DataUtil;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.PredictionMode;
import org.antlr.v4.runtime.tree.ParseTree;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;
import java.util.Iterator;

import static org.junit.Assert.assertEquals;

public abstract class QTypeParserTest<T extends QType> {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private final ByteBuffer buffer1 = ByteBuffer.allocate(256).order(ByteOrder.LITTLE_ENDIAN);
    private final ByteBuffer buffer2 = ByteBuffer.allocate(256).order(ByteOrder.LITTLE_ENDIAN);
    private final StringBuilder sb1 = new StringBuilder(256);
    private final StringBuilder sb2 = new StringBuilder(256);
    private final QFactory factory = new DefaultQFactory();

    protected final T parseExpr(String text) {
        logger.info("Parsing '{}'", text);
        ANTLRInputStream input = new ANTLRInputStream(text);
        final QLexer lexer = new BailQLexer(input);
        final CommonTokenStream tokens = new CommonTokenStream(lexer);
        logTokens(tokens);
        final QParser parser = new QParser(tokens);
        parser.addErrorListener(new DiagnosticErrorListener());
        parser.setErrorHandler(new BailErrorStrategy());
        parser.getInterpreter().setPredictionMode(PredictionMode.LL_EXACT_AMBIG_DETECTION);
        final ParseTree tree = parser.singleExprEOF(); // parse
        final String parseTree = tree.toStringTree(parser);
        logger.info(parseTree);
        final QTypeParser qParser = new QTypeParser();
        final T result = (T) qParser.visit(tree);
        return result;
    }

    private final void logTokens(CommonTokenStream tokens) {
        tokens.fill();
        final Iterator<Token> tokenIter = tokens.getTokens().iterator();
        final StringBuilder sb = new StringBuilder("Tokens:\n");
        while (tokenIter.hasNext()) {
            Object tree = tokenIter.next();
            sb.append(tree).append("\n");
        }
        logger.info(sb.toString());
    }

    protected void clearBuffers() {
        buffer1.clear();
        Arrays.fill(buffer1.array(), 0, buffer1.capacity(), QType.ZERO);
        buffer2.clear();
        Arrays.fill(buffer2.array(), 0, buffer2.capacity(), QType.ZERO);
        sb1.setLength(0);
        sb2.setLength(0);
        sb1.append("0x");
        sb2.append("0x");
    }

    protected void checkEquals(String expectedToString, String expectedHexBinary, T x) {
        checkEquals(expectedToString, expectedHexBinary, x, new ToStringContext(false, 0));
    }

    protected void checkEquals(String expectedToString, String expectedHexBinary, T x, ToStringContext ctx) {
        clearBuffers();
        assertEquals(expectedToString, x.toString(ctx));
        x.toBinary(buffer1);
        final int byteCount = buffer1.position();
        buffer1.flip();
        final String actualHex1 = DataUtil.byteBufferToHex(buffer1, sb1).toString();
        //logger.info("{} -> \"{}\"", expectedToString, actualHex1);
        assertEquals(expectedHexBinary, actualHex1);
        assertEquals(byteCount, buffer1.position());
        buffer1.flip();

        T fromX = (T) factory.newType(buffer1);
        assertEquals(expectedToString, fromX.toString(ctx));
        fromX.toBinary(buffer2);
        assertEquals(byteCount, buffer2.position());
        buffer2.flip();

        final String actualHex2 = DataUtil.byteBufferToHex(buffer2, sb2).toString();
        assertEquals(expectedHexBinary, actualHex2);
        assertEquals(byteCount, buffer2.position());
    }

}
