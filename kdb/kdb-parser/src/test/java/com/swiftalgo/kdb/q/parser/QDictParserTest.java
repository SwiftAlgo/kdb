package com.swiftalgo.kdb.q.parser;

/*-
 * #%L
 * KDB+ Parser
 * %%
 * Copyright (C) 2017 SwiftAlgo Limited
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.swiftalgo.kdb.q.QDict;
import org.junit.Test;

public class QDictParserTest extends QTypeParserTest<QDict> {

    @Test
    public void testDict() {
        final QDict t1 = parseExpr("`a`b!2 3h");
        checkEquals("`a`b!2 3h", "0x630b00020000006100620005000200000002000300", t1);

    }

    @Test
    public void testAttrDict() {
        final QDict t1 = parseExpr("`s#`a`b!2 3h");
        checkEquals("`s#`a`b!2 3h", "0x7f0b01020000006100620005000200000002000300", t1);

        final QDict t2 = parseExpr("`s#(`a`b!2 3h)");
        checkEquals("`s#`a`b!2 3h", "0x7f0b01020000006100620005000200000002000300", t2);
    }

    @Test(expected = IllegalStateException.class)
    public void testInvalidDictCount() {
        parseExpr("`a`b!1 2 3");
    }


}
