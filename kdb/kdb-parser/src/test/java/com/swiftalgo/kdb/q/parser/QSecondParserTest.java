package com.swiftalgo.kdb.q.parser;

/*-
 * #%L
 * KDB+ Parser
 * %%
 * Copyright (C) 2017 SwiftAlgo Limited
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.swiftalgo.kdb.q.QSecond;
import org.junit.Test;

public class QSecondParserTest extends QTypeParserTest<QSecond> {

    @Test
    public void testNull() {
        final String lowerNullText = "0nv";
        final String upperNullText = "0Nv";
        final String nullHex = "0xee00000080";
        final QSecond t1 = parseExpr(upperNullText);
        checkEquals(upperNullText, nullHex, t1);
        final QSecond t2 = parseExpr(lowerNullText);
        checkEquals(upperNullText, nullHex, t2);
    }

    @Test
    public void testPositiveInfinity() {
        final String lowerText = "0wv";
        final String upperText = "0Wv";
        final String hex = "0xeeffffff7f";
        final QSecond t1 = parseExpr(upperText);
        checkEquals(upperText, hex, t1);
        final QSecond t2 = parseExpr(lowerText);
        checkEquals(upperText, hex, t2);
    }

    @Test
    public void testNegativeInfinity() {
        final String lowerText = "-0wv";
        final String upperText = "-0Wv";
        final String hex = "0xee01000080";
        final QSecond t1 = parseExpr(upperText);
        checkEquals(upperText, hex, t1);
        final QSecond t2 = parseExpr(lowerText);
        checkEquals(upperText, hex, t2);
    }

    @Test
    public void testZero() {
        final String text = "00:00:00";
        final String hex = "0xee00000000";
        final QSecond t1 = parseExpr(text);
        checkEquals(text, hex, t1);
        final QSecond t2 = parseExpr(text + 'v');
        checkEquals(text, hex, t2);
        final QSecond t3 = parseExpr("00:00:00.000000000v");
        checkEquals(text, hex, t3);
        final QSecond t4 = parseExpr("00:00v");
        checkEquals(text, hex, t4);
        final QSecond t5 = parseExpr("00:00:00.v");
        checkEquals(text, hex, t5);
        final QSecond t6 = parseExpr("00:00:00.0v");
        checkEquals(text, hex, t6);
        final QSecond t7 = parseExpr("00:00:00.0000000000v");
        checkEquals(text, hex, t7);
    }

    @Test
    public void testNegativeZero() {
        final String outText = "00:00:00";
        final String hex = "0xee00000000";
        final QSecond t1 = parseExpr("-00:00:00");
        checkEquals(outText, hex, t1);
        final QSecond t2 = parseExpr("-00:00:00v");
        checkEquals(outText, hex, t2);
        final QSecond t3 = parseExpr("-00:00:00.000000000v");
        checkEquals(outText, hex, t3);
        final QSecond t4 = parseExpr("-00:00v");
        checkEquals(outText, hex, t4);
        final QSecond t5 = parseExpr("-00:00:00.v");
        checkEquals(outText, hex, t5);
        final QSecond t6 = parseExpr("-00:00:00.0v");
        checkEquals(outText, hex, t6);
        final QSecond t7 = parseExpr("-00:00:00.0000000000v");
        checkEquals(outText, hex, t7);
    }

    @Test
    public void testNegative() {
        final String text1 = "-01:23:45";
        final String hex1 = "0xee5fecffff";
        final QSecond t1 = parseExpr(text1);
        checkEquals(text1, hex1, t1);
        final String text2 = "-00:45:15";
        final String hex2 = "0xee65f5ffff";
        final QSecond t2 = parseExpr(text2);
        checkEquals(text2, hex2, t2);
    }

    @Test
    public void testMaximum() {
        final String text = "23:59:59";
        final String hex = "0xee7f510100";
        final QSecond t1 = parseExpr(text);
        checkEquals(text, hex, t1);
        final QSecond t2 = parseExpr(text + 'v');
        checkEquals(text, hex, t2);
        final QSecond t3 = parseExpr("23:59:59.999999999v");
        checkEquals(text, hex, t3);
    }

    @Test
    public void testMinimum() {
        final String text = "-23:59:59";
        final String hex = "0xee81aefeff";
        final QSecond t1 = parseExpr(text);
        checkEquals(text, hex, t1);
        final QSecond t2 = parseExpr(text + 'v');
        checkEquals(text, hex, t2);
        final QSecond t3 = parseExpr("-23:59:59.999999999v");
        checkEquals(text, hex, t3);
    }

}
