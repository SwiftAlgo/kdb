package com.swiftalgo.kdb.q.parser;

/*-
 * #%L
 * KDB+ Parser
 * %%
 * Copyright (C) 2017 SwiftAlgo Limited
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.swiftalgo.kdb.q.QTimestamp;
import org.junit.Test;

public class QTimestampParserTest extends QTypeParserTest<QTimestamp> {

    @Test
    public void testJavaEpoch() {
        final String text = "1970.01.01D00:00:00.000000000";
        final String hex = "0xf40000bdad30b3dcf2";
        final QTimestamp t1 = parseExpr(text);
        checkEquals(text, hex, t1);
        final QTimestamp t2 = parseExpr("1970.01.01D00:00:00.000000000p");
        checkEquals(text, hex, t2);
    }

    @Test
    public void testNearJavaEpoch() {
        final String text = "1970.01.01D12:34:56.123456789";
        final String hex = "0xf4152d160163dcdcf2";
        final QTimestamp t1 = parseExpr(text);
        checkEquals(text, hex, t1);
        final QTimestamp t2 = parseExpr(text + 'p');
        checkEquals(text, hex, t2);
    }


    @Test
    public void testQEpoch() {
        final String text = "2000.01.01D00:00:00.000000000";
        final String hex = "0xf40000000000000000";
        final QTimestamp t1 = parseExpr(text);
        checkEquals(text, hex, t1);
        final QTimestamp t2 = parseExpr(text + 'p');
        checkEquals(text, hex, t2);
        final QTimestamp t3 = parseExpr('-' + text);
        checkEquals(text, hex, t3);
        final QTimestamp t4 = parseExpr('-' + text + 'p');
        checkEquals(text, hex, t4);
    }

    @Test
    public void testNull() {
        final String nullText = "0Np";
        final String nullHex = "0xf40000000000000080";
        final QTimestamp t1 = parseExpr(nullText);
        checkEquals(nullText, nullHex, t1);
        final String lowerNullText = "0np";
        final QTimestamp t2 = parseExpr(lowerNullText);
        checkEquals(nullText, nullHex, t2);
    }

    @Test
    public void testPositiveInfinity() {
        final String posInfText = "0Wp";
        final String posInfHex = "0xf4ffffffffffffff7f";
        final QTimestamp t1 = parseExpr(posInfText);
        checkEquals(posInfText, posInfHex, t1);
        final String lowerInfText = "0wp";
        final QTimestamp t2 = parseExpr(lowerInfText);
        checkEquals(posInfText, posInfHex, t2);
    }

    @Test
    public void testNegativeInfinity() {
        final String negInfText = "-0Wp";
        final String negInfHex = "0xf40100000000000080";
        final QTimestamp t1 = parseExpr(negInfText);
        checkEquals(negInfText, negInfHex, t1);
        final String lowerNegInfText = "-0wp";
        final QTimestamp t2 = parseExpr(lowerNegInfText);
        checkEquals(negInfText, negInfHex, t2);
    }

    @Test
    public void testMinimum() {
        final String text = "1709.01.01D00:00:00.000000000";
        final String hex = "0xf400001d6453588f80";
        final QTimestamp t1 = parseExpr(text);
        checkEquals(text, hex, t1);
        final QTimestamp t2 = parseExpr("1709.01.01D00:00:00.000000000p");
        checkEquals(text, hex, t2);
    }

    @Test
    public void testNegativeMinimum() {
        final String inText = "-1709.01.01D00:00:00.000000000";
        final String outText = "2290.12.31D00:00:00.000000000";
        final String hex = "0xf40000e39baca7707f";
        final QTimestamp t1 = parseExpr(inText);
        checkEquals(outText, hex, t1);
        final QTimestamp t2 = parseExpr(inText + 'p');
        checkEquals(outText, hex, t2);
    }

    @Test
    public void testMaximum() {
        final String text = "2290.12.31D23:59:59.999999999";
        final String hex = "0xf4ffff312d41f6707f";
        final QTimestamp t1 = parseExpr(text);
        checkEquals(text, hex, t1);
        final QTimestamp t2 = parseExpr("2290.12.31D23:59:59.999999999p");
        checkEquals(text, hex, t2);
    }

    @Test
    public void testNegativeMaximum() {
        final String inText = "-2290.12.31D23:59:59.999999999";
        final String outText = "1708.12.31D00:00:00.000000001";
        final String hex = "0xf40100ced2be098f80";
        final QTimestamp t1 = parseExpr(inText);
        checkEquals(outText, hex, t1);
        final QTimestamp t2 = parseExpr(inText + 'p');
        checkEquals(outText, hex, t2);
    }

    @Test
    public void testNegative() {
        final String inText = "-2001.02.03D04:05:06.123456789";
        final String outText = "1998.11.27D19:54:53.876543211";
        final String hex = "0xf4eb3e9b7c117985ff";
        final QTimestamp t1 = parseExpr(inText);
        checkEquals(outText, hex, t1);
    }
}
