package com.swiftalgo.kdb.q.parser;

/*-
 * #%L
 * KDB+ Parser
 * %%
 * Copyright (C) 2017 SwiftAlgo Limited
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.swiftalgo.kdb.q.QLong;
import org.junit.Test;

public class QLongParserTest extends QTypeParserTest<QLong> {

    @Test
    public void testLongA() {

        final QLong t1 = parseExpr("0j");
        checkEquals("0", "0xf90000000000000000", t1);

        final QLong t2 = parseExpr("1j");
        checkEquals("1", "0xf90100000000000000", t2);

        final QLong t3 = parseExpr("-1j");
        checkEquals("-1", "0xf9ffffffffffffffff", t3);

        //null
        final String nullHex = "0xf90000000000000080";
        final QLong t8 = parseExpr("0N");
        checkEquals("0N", nullHex, t8);
        final QLong t8b = parseExpr("0Nj");
        checkEquals("0N", nullHex, t8b);

        //+/- infinity
        final QLong t9 = parseExpr("-0Wj");
        checkEquals("-0W", "0xf90100000000000080", t9);

        final QLong t10 = parseExpr("-0wj");
        checkEquals("-0W", "0xf90100000000000080", t10);

        final QLong t11 = parseExpr("-9223372036854775807j"); //Long.MIN_VALUE + 1
        checkEquals("-0W", "0xf90100000000000080", t11);

        final QLong t12 = parseExpr("0Wj");
        checkEquals("0W", "0xf9ffffffffffffff7f", t12);

        final QLong t13 = parseExpr("0wj");
        checkEquals("0W", "0xf9ffffffffffffff7f", t13);

        final QLong t14 = parseExpr("9223372036854775807j"); //Long.MAX_VALUE
        checkEquals("0W", "0xf9ffffffffffffff7f", t14);

    }

    @Test(expected = NumberFormatException.class)
    public void testLongALowerBoundException() {
        parseExpr(Long.MIN_VALUE + "j");
    }

    @Test(expected = NumberFormatException.class)
    public void testLongAUpperBoundException() {
        parseExpr(Long.MAX_VALUE + "0j");
    }

}
