package com.swiftalgo.kdb.q.parser;

/*-
 * #%L
 * KDB+ Parser
 * %%
 * Copyright (C) 2017 SwiftAlgo Limited
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.swiftalgo.kdb.q.QSymbolV;
import org.junit.Test;

public class QSymbolVParserTest extends QTypeParserTest<QSymbolV> {

    @Test
    public void testCompactSymbolV() {
        final QSymbolV t1 = parseExpr("`IBM`MSFT");
        checkEquals("`IBM`MSFT", "0x0b000200000049424d004d53465400", t1);
    }

    @Test
    public void testListSymbolV() {
        final QSymbolV t1 = parseExpr("(`IBM;`MSFT)");
        checkEquals("`IBM`MSFT", "0x0b000200000049424d004d53465400", t1);
    }

    @Test
    public void testSingletonSymbolV() {
        final QSymbolV t1 = parseExpr("enlist `IBM");
        checkEquals(",`IBM", "0x0b000100000049424d00", t1);
    }

    @Test
    public void testAttrSymbolV() {
        final QSymbolV t1 = parseExpr("`s#`IBM`MSFT");
        checkEquals("`s#`IBM`MSFT", "0x0b010200000049424d004d53465400", t1);
        final QSymbolV t2 = parseExpr("`s#enlist `IBM");
        checkEquals("`s#,`IBM", "0x0b010100000049424d00", t2);
    }

}
