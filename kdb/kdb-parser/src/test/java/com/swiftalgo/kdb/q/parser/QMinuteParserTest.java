package com.swiftalgo.kdb.q.parser;

/*-
 * #%L
 * KDB+ Parser
 * %%
 * Copyright (C) 2017 SwiftAlgo Limited
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.swiftalgo.kdb.q.QMinute;
import org.junit.Test;

public class QMinuteParserTest extends QTypeParserTest<QMinute> {

    @Test
    public void testNull() {
        final String lowerNullText = "0nu";
        final String upperNullText = "0Nu";
        final String nullHex = "0xef00000080";
        final QMinute t1 = parseExpr(upperNullText);
        checkEquals(upperNullText, nullHex, t1);
        final QMinute t2 = parseExpr(lowerNullText);
        checkEquals(upperNullText, nullHex, t2);
    }

    @Test
    public void testPositiveInfinity() {
        final String lowerText = "0wu";
        final String upperText = "0Wu";
        final String hex = "0xefffffff7f";
        final QMinute t1 = parseExpr(upperText);
        checkEquals(upperText, hex, t1);
        final QMinute t2 = parseExpr(lowerText);
        checkEquals(upperText, hex, t2);
    }

    @Test
    public void testNegativeInfinity() {
        final String lowerText = "-0wu";
        final String upperText = "-0Wu";
        final String hex = "0xef01000080";
        final QMinute t1 = parseExpr(upperText);
        checkEquals(upperText, hex, t1);
        final QMinute t2 = parseExpr(lowerText);
        checkEquals(upperText, hex, t2);
    }

    @Test
    public void testZero() {
        final String text = "00:00";
        final String hex = "0xef00000000";
        final QMinute t1 = parseExpr(text);
        checkEquals(text, hex, t1);
        final QMinute t2 = parseExpr(text + 'u');
        checkEquals(text, hex, t2);
        final QMinute t3 = parseExpr("00:00:00.000000000u");
        checkEquals(text, hex, t3);
        final QMinute t4 = parseExpr("00:00:00u");
        checkEquals(text, hex, t4);
        final QMinute t5 = parseExpr("00:00:00.u");
        checkEquals(text, hex, t5);
        final QMinute t6 = parseExpr("00:00:00.0u");
        checkEquals(text, hex, t6);
        final QMinute t7 = parseExpr("00:00:00.0000000000u");
        checkEquals(text, hex, t7);
    }

    @Test
    public void testNegativeZero() {
        final String outText = "00:00";
        final String hex = "0xef00000000";
        final QMinute t1 = parseExpr("-00:00");
        checkEquals(outText, hex, t1);
        final QMinute t2 = parseExpr("-00:00u");
        checkEquals(outText, hex, t2);
        final QMinute t3 = parseExpr("-00:00:00.000000000u");
        checkEquals(outText, hex, t3);
        final QMinute t4 = parseExpr("-00:00:00u");
        checkEquals(outText, hex, t4);
        final QMinute t5 = parseExpr("-00:00:00.u");
        checkEquals(outText, hex, t5);
        final QMinute t6 = parseExpr("-00:00:00.0u");
        checkEquals(outText, hex, t6);
        final QMinute t7 = parseExpr("-00:00:00.0000000000u");
        checkEquals(outText, hex, t7);
    }

    @Test
    public void testNegative() {
        final String text1 = "-01:23";
        final String hex1 = "0xefadffffff";
        final QMinute t1 = parseExpr(text1);
        checkEquals(text1, hex1, t1);
        final String text2 = "-00:45";
        final String hex2 = "0xefd3ffffff";
        final QMinute t2 = parseExpr(text2);
        checkEquals(text2, hex2, t2);
    }

    @Test
    public void testMaximum() {
        final String text = "23:59";
        final String hex = "0xef9f050000";
        final QMinute t1 = parseExpr(text);
        checkEquals(text, hex, t1);
        final QMinute t2 = parseExpr(text + 'u');
        checkEquals(text, hex, t2);
        final QMinute t3 = parseExpr("23:59:59.999999999u");
        checkEquals(text, hex, t3);
    }

    @Test
    public void testMinimum() {
        final String text = "-23:59";
        final String hex = "0xef61faffff";
        final QMinute t1 = parseExpr(text);
        checkEquals(text, hex, t1);
        final QMinute t2 = parseExpr(text + 'u');
        checkEquals(text, hex, t2);
        final QMinute t3 = parseExpr("-23:59:59.999999999u");
        checkEquals(text, hex, t3);
    }

}
