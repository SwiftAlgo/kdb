package com.swiftalgo.kdb.q.parser;

/*-
 * #%L
 * KDB+ Parser
 * %%
 * Copyright (C) 2017 SwiftAlgo Limited
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.swiftalgo.kdb.q.QTimespan;
import org.junit.Test;

public class QTimespanParserTest extends QTypeParserTest<QTimespan> {

    @Test
    public void testNull() {
        final String lowerNullText = "0nn";
        final String upperNullText = "0Nn";
        final String nullHex = "0xf00000000000000080";
        final QTimespan t1 = parseExpr(upperNullText);
        checkEquals(upperNullText, nullHex, t1);
        final QTimespan t2 = parseExpr(lowerNullText);
        checkEquals(upperNullText, nullHex, t2);
    }

    @Test
    public void testPositiveInfinity() {
        final String lowerText = "0wn";
        final String upperText = "0Wn";
        final String hex = "0xf0ffffffffffffff7f";
        final QTimespan t1 = parseExpr(upperText);
        checkEquals(upperText, hex, t1);
        final QTimespan t2 = parseExpr(lowerText);
        checkEquals(upperText, hex, t2);
    }

    @Test
    public void testNegativeInfinity() {
        final String lowerText = "-0wn";
        final String upperText = "-0Wn";
        final String hex = "0xf00100000000000080";
        final QTimespan t1 = parseExpr(upperText);
        checkEquals(upperText, hex, t1);
        final QTimespan t2 = parseExpr(lowerText);
        checkEquals(upperText, hex, t2);
    }

    @Test
    public void testZero() {
        final String text = "0D00:00:00.000000000";
        final String hex = "0xf00000000000000000";
        final QTimespan t1 = parseExpr(text);
        checkEquals(text, hex, t1);
        final QTimespan t2 = parseExpr(text + 'n');
        checkEquals(text, hex, t2);
        final QTimespan t3 = parseExpr('-' + text);
        checkEquals(text, hex, t3);
        final QTimespan t4 = parseExpr("00:00:00.000000000n");
        checkEquals(text, hex, t4);
        final QTimespan t5 = parseExpr("00:00:00n");
        checkEquals(text, hex, t5);
        final QTimespan t6 = parseExpr("00:00:00.n");
        checkEquals(text, hex, t6);
        final QTimespan t7 = parseExpr("00:00:00.0n");
        checkEquals(text, hex, t7);
        final QTimespan t8 = parseExpr("00:00:00.00000");
        checkEquals(text, hex, t8);
    }

    @Test
    public void testMaximum() {
        final String text = "0D23:59:59.999999999";
        final String hex = "0xf0ffff4e91944e0000";
        final QTimespan t1 = parseExpr(text);
        checkEquals(text, hex, t1);
        final QTimespan t2 = parseExpr(text + 'n');
        checkEquals(text, hex, t2);
        final QTimespan t3 = parseExpr("23:59:59.999999999");
        checkEquals(text, hex, t3);
    }

    @Test
    public void testMillisRounding() {
        final String text = "0D12:30:29.123456789";
        final String hex = "0xf0157fe828f4280000";
        final QTimespan t1 = parseExpr(text);
        checkEquals(text, hex, t1);
        final QTimespan t2 = parseExpr(text + 'n');
        checkEquals(text, hex, t2);
    }

    @Test
    public void testMillis() {
        final String text = "0D12:30:29.123456789";
        final String hex = "0xf0157fe828f4280000";
        final QTimespan t1 = parseExpr(text);
        checkEquals(text, hex, t1);
        final QTimespan t2 = parseExpr(text + 'n');
        checkEquals(text, hex, t2);
    }

    @Test
    public void testPositiveDays() {
        final String text = "100D12:30:29.123456789";
        final String hex = "0xf0157fc4ebfcda1e00";
        final QTimespan t1 = parseExpr(text);
        checkEquals(text, hex, t1);
        final QTimespan t2 = parseExpr(text + 'n');
        checkEquals(text, hex, t2);
    }

    @Test
    public void testNegativeDays() {
        final String text = "-100D12:30:29.123456789";
        final String hex = "0xf0eb803b140325e1ff";
        final QTimespan t1 = parseExpr(text);
        checkEquals(text, hex, t1);
        final QTimespan t2 = parseExpr(text + 'n');
        checkEquals(text, hex, t2);
    }

    @Test
    public void testNegativeZeroDays() {
        final String text = "-0D12:30:29.123456789";
        final String hex = "0xf0eb8017d70bd7ffff";
        final QTimespan t1 = parseExpr(text);
        checkEquals(text, hex, t1);
        final QTimespan t2 = parseExpr(text + 'n');
        checkEquals(text, hex, t2);
    }


}
