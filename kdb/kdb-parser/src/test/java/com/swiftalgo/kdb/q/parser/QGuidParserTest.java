package com.swiftalgo.kdb.q.parser;

/*-
 * #%L
 * KDB+ Parser
 * %%
 * Copyright (C) 2017 SwiftAlgo Limited
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.swiftalgo.kdb.q.QGuid;
import org.junit.Test;

public class QGuidParserTest extends QTypeParserTest<QGuid> {

    @Test
    public void testGuid() {
        final QGuid t1 = new QGuid("977b6c8c-4224-a5ac-1c94-9ea27e4d3df7");
        checkEquals("977b6c8c-4224-a5ac-1c94-9ea27e4d3df7", "0xfe977b6c8c4224a5ac1c949ea27e4d3df7", t1);
        final QGuid t2 = parseExpr("0Ng");
        checkEquals("00000000-0000-0000-0000-000000000000", "0xfe00000000000000000000000000000000", t2);
        final QGuid t3 = parseExpr("0ng");
        checkEquals("00000000-0000-0000-0000-000000000000", "0xfe00000000000000000000000000000000", t3);
    }

}
