package com.swiftalgo.kdb.q.parser;

/*-
 * #%L
 * KDB+ Parser
 * %%
 * Copyright (C) 2017 SwiftAlgo Limited
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.swiftalgo.kdb.q.QTimespanV;
import org.junit.Test;

public class QTimespanVParserTest extends QTypeParserTest<QTimespanV> {

    @Test
    public void testCompactTimespanV() {
        final String inText1 = "-0W -5D12:13:14.123456789 -0D23:59:59.999999999 -23:59:59.999999999 -00:00:00.000000000 00:00:00.000000000 0N 23:59:59.999999999 5D12:13:14.123456789 0Wn";
        final String outText1 = "-0W -5D12:13:14.123456789 -0D23:59:59.999999999 -0D23:59:59.999999999 0D00:00:00.000000000 0D00:00:00.000000000 0N 0D23:59:59.999999999 5D12:13:14.123456789 0Wn";
        final String hex1 = "0x10000a0000000100000000000080eb2e5bfb154ffeff0100b16e6bb1ffff0100b16e6bb1ffff000000000000000000000000000000000000000000000080ffff4e91944e000015d1a404eab00100ffffffffffffff7f";

        final QTimespanV t1 = parseExpr(inText1);
        checkEquals(outText1, hex1, t1);

        final String inText2 = "-123D01:23:34.567000000 00:00:00.000000000 123D22:34:53.123456789";
        final String outText2 = "-123D01:23:34.567000000 0D00:00:00.000000000 123D22:34:53.123456789";
        final String hex2 = "0x10000300000040e88ea30e3adaff0000000000000000150fb458510b2600";
        final QTimespanV t2 = parseExpr(inText2);
        checkEquals(outText2, hex2, t2);
    }

    @Test
    public void testListTimespanV() {

        final String inText1 = "( -0wn; \t -5D12:13:14.123456789 ; \t -0D23:59:59.999999999 ; -23:59:59.999999999 ; -00:00:00.000000000 ; 00:00:00.000000000; 0Nn;23:59:59.999999999n \t ; 5D12:13:14.123456789  ;0Wn)";
        final String outText1 = "-0W -5D12:13:14.123456789 -0D23:59:59.999999999 -0D23:59:59.999999999 0D00:00:00.000000000 0D00:00:00.000000000 0N 0D23:59:59.999999999 5D12:13:14.123456789 0Wn";
        final QTimespanV t1 = parseExpr(inText1);
        final String hex = "0x10000a0000000100000000000080eb2e5bfb154ffeff0100b16e6bb1ffff0100b16e6bb1ffff000000000000000000000000000000000000000000000080ffff4e91944e000015d1a404eab00100ffffffffffffff7f";
        checkEquals(outText1, hex, t1);

        final String inText2 = "( \t -123D01:23:34.567000000 \t ; \t 00:00:00.000000000 \t ; \t 123D22:34:53.123456789 \t )";
        final String outText2 = "-123D01:23:34.567000000 0D00:00:00.000000000 123D22:34:53.123456789";
        final String hex2 = "0x10000300000040e88ea30e3adaff0000000000000000150fb458510b2600";
        final QTimespanV t2 = parseExpr(inText2);
        checkEquals(outText2, hex2, t2);
    }

    @Test
    public void testSingletonTimespanV() {
        final String x = "5D22:34:53.987654321";
        final String hex1 = "0x100001000000b1aacc91d6d20100";
        final QTimespanV t1 = parseExpr("enlist " + x);
        checkEquals("," + x, hex1, t1);
        final QTimespanV t2 = parseExpr(String.format("enlist (%s)", x));
        checkEquals("," + x, hex1, t2);
        final QTimespanV t3 = parseExpr("`s#enlist " + x);
        checkEquals("`s#," + x, "0x100101000000b1aacc91d6d20100", t3);
    }

    @Test
    public void testEnclosedTimespanV() {
        final String inText1 = "-123D01:23:34.567000000 00:00:00.000000000 123D22:34:53.123456789";
        final String outText1 = "-123D01:23:34.567000000 0D00:00:00.000000000 123D22:34:53.123456789";
        final QTimespanV t1 = parseExpr(String.format("( \t %s \t )", inText1));
        final String hex = "0x10000300000040e88ea30e3adaff0000000000000000150fb458510b2600";
        checkEquals(outText1, hex, t1);
        final QTimespanV t2 = parseExpr(String.format("( \t ( \t %s \t ) \t )", inText1));
        checkEquals(outText1, hex, t2);
    }

    @Test
    public void testAttrTimespanV() {
        final String inText1 = "`s#00:00:00.000000000 23:59:59.999999999 5D12:13:14.123456789";
        final String outText1 = "`s#0D00:00:00.000000000 0D23:59:59.999999999 5D12:13:14.123456789";
        final String hex1 = "0x1001030000000000000000000000ffff4e91944e000015d1a404eab00100";
        final QTimespanV t1 = parseExpr(inText1);
        checkEquals(outText1, hex1, t1);
    }

}
