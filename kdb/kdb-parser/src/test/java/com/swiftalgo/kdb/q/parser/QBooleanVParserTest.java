package com.swiftalgo.kdb.q.parser;

/*-
 * #%L
 * KDB+ Parser
 * %%
 * Copyright (C) 2017 SwiftAlgo Limited
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.swiftalgo.kdb.q.QBooleanV;
import org.junit.Test;

public class QBooleanVParserTest extends QTypeParserTest<QBooleanV> {

    @Test
    public void testCompactBooleanV() {
        final QBooleanV t1 = parseExpr("01010b");
        checkEquals("01010b", "0x0100050000000001000100", t1);

        final QBooleanV t2 = parseExpr("`s#00011b");
        checkEquals("`s#00011b", "0x0101050000000000000101", t2);
    }

    @Test
    public void testListBooleanV() {
        final QBooleanV t1 = parseExpr("(0b;1b;0b)");
        checkEquals("010b", "0x010003000000000100", t1);
        final QBooleanV t2 = parseExpr("( \t 0b \t ; \t 1b \t; \t 0b \t )");
        checkEquals("010b", "0x010003000000000100", t2);
        final QBooleanV t3 = parseExpr("( \t (0b) \t ;( \t 1b \t); \t ((0b)) \t )");
        checkEquals("010b", "0x010003000000000100", t3);
    }

    @Test
    public void testEnclosedBooleanV() {
        final QBooleanV t1 = parseExpr("(01010b)");
        checkEquals("01010b", "0x0100050000000001000100", t1);
        final QBooleanV t2 = parseExpr("( ( 01010b ) )");
        checkEquals("01010b", "0x0100050000000001000100", t2);
        final QBooleanV t3 = parseExpr("( `s# ( 00011b ) )");
        checkEquals("`s#00011b", "0x0101050000000000000101", t3);
        final QBooleanV t4 = parseExpr("`g#( `p#(`s#00011b ) )");
        checkEquals("`g#00011b", "0x0104050000000000000101", t4);
    }

    @Test
    public void testSingletonBooleanV() {
        final QBooleanV t1 = parseExpr("enlist 0b");
        checkEquals(",0b", "0x01000100000000", t1);
        final QBooleanV t2 = parseExpr("enlist 1b");
        checkEquals(",1b", "0x01000100000001", t2);
        final QBooleanV t3 = parseExpr("enlist (1b)");
        checkEquals(",1b", "0x01000100000001", t3);
        final QBooleanV t4 = parseExpr("`s#enlist 1b");
        checkEquals("`s#,1b", "0x01010100000001", t4);
    }

}
