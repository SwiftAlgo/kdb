package com.swiftalgo.kdb.q.parser;

/*-
 * #%L
 * KDB+ Parser
 * %%
 * Copyright (C) 2017 SwiftAlgo Limited
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.swiftalgo.kdb.q.QDateV;
import org.junit.Test;

public class QDateVParserTest extends QTypeParserTest<QDateV> {

    @Test
    public void testCompactDateV() {
        final String text1 = "-0W 1709.01.01 0N 2290.12.31 0Wd";
        final String hex1 = "0x0e000500000001000080d360feff000000802d9f0100ffffff7f";

        final QDateV t1 = parseExpr(text1);
        checkEquals(text1, hex1, t1);

        final String text2 = "1970.01.01 2000.01.01 2015.05.28";
        final String hex2 = "0x0e000300000033d5ffff00000000fa150000";
        final QDateV t2b = parseExpr(text2);
        checkEquals(text2, hex2, t2b);
    }

    @Test
    public void testListDateV() {
        final String inText1 = "( -0wd; \t 1709.01.01d; 0Nd;2290.12.31d \t ;0Wd)";
        final String outText1 = "-0W 1709.01.01 0N 2290.12.31 0Wd";
        final String hex = "0x0e000500000001000080d360feff000000802d9f0100ffffff7f";
        final QDateV t1 = parseExpr(inText1);
        checkEquals(outText1, hex, t1);

        final String inText2 = "(1970.01.01;2000.01.01;2015.05.28)";
        final String outText2 = "1970.01.01 2000.01.01 2015.05.28";
        final String hex2 = "0x0e000300000033d5ffff00000000fa150000";
        final QDateV t2 = parseExpr(inText2);
        checkEquals(outText2, hex2, t2);
    }

    @Test
    public void testSingletonDateV() {
        final String x = "2015.05.28";
        final String hex1 = "0x0e0001000000fa150000";
        final QDateV t1 = parseExpr("enlist " + x);
        checkEquals("," + x, hex1, t1);
        final QDateV t2 = parseExpr("enlist " + x + 'd');
        checkEquals("," + x, hex1, t2);
        final QDateV t3 = parseExpr(String.format("enlist (%s)", x));
        checkEquals("," + x, hex1, t3);
        final QDateV t4 = parseExpr("`s#enlist " + x);
        checkEquals("`s#," + x, "0x0e0101000000fa150000", t4);
    }

    @Test
    public void testEnclosedDateV() {
        final String text1 = "1970.01.01 2000.01.01 2015.05.28";
        final String hex = "0x0e000300000033d5ffff00000000fa150000";
        final QDateV t1 = parseExpr(String.format("( \t %s \t )", text1));
        checkEquals(text1, hex, t1);
        final QDateV t2 = parseExpr(String.format("( \t ( \t %s \t ) \t )", text1));
        checkEquals(text1, hex, t2);
    }

    @Test
    public void testAttrDateV() {
        final String text1 = "`s#1970.01.01 2000.01.01 2015.05.28";
        final String hex1 = "0x0e010300000033d5ffff00000000fa150000";
        final QDateV t1 = parseExpr(text1);
        checkEquals(text1, hex1, t1);
    }

    @Test
    public void testNegativeDateV() {
        final String inText1 = "-1970.01.01 -2000.01.01 2000.01.01 -2015.05.28d";
        final String outText1 = "2029.12.31 2000.01.01 2000.01.01 1984.08.06";
        final QDateV t1 = parseExpr(inText1);
        final String hex = "0x0e0004000000cd2a0000000000000000000006eaffff";
        checkEquals(outText1, hex, t1);
    }

}
