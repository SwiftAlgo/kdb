package com.swiftalgo.kdb.q.parser;

/*-
 * #%L
 * KDB+ Parser
 * %%
 * Copyright (C) 2017 SwiftAlgo Limited
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.swiftalgo.kdb.q.QChar;
import org.junit.Test;

public class QCharParserTest extends QTypeParserTest<QChar> {

    @Test
    public void testCharA() {
        final QChar t1 = parseExpr("\"a\"");
        checkEquals("\"a\"", "0xf661", t1);
        final QChar t2 = parseExpr("\" \"");
        checkEquals("\" \"", "0xf620", t2);
        final QChar t3 = parseExpr("( \t ( \t \"a\" \t ) \t )");
        checkEquals("\"a\"", "0xf661", t3);

    }

}
