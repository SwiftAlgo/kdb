package com.swiftalgo.kdb.q.parser;

/*-
 * #%L
 * KDB+ Parser
 * %%
 * Copyright (C) 2017 SwiftAlgo Limited
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.swiftalgo.kdb.q.QTable;
import org.junit.Test;

public class QTableParserTest extends QTypeParserTest<QTable> {

    @Test
    public void testTable() {
        final QTable t1 = parseExpr("flip `ticker`shares!(`IBM`MSFT;1 2h)");
        checkEquals("+`ticker`shares!(`IBM`MSFT;1 2h)", "0x6200630b00020000007469636b657200736861726573000000020000000b000200000049424d004d5346540005000200000001000200", t1);
    }

}
