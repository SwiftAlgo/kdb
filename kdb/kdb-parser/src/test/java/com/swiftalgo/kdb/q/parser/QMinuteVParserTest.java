package com.swiftalgo.kdb.q.parser;

/*-
 * #%L
 * KDB+ Parser
 * %%
 * Copyright (C) 2017 SwiftAlgo Limited
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.swiftalgo.kdb.q.QMinuteV;
import org.junit.Test;

public class QMinuteVParserTest extends QTypeParserTest<QMinuteV> {

    @Test
    public void testCompactMinuteV() {
        final String inText1 = "-0W -23:59:59.999999999 -23:59 -00:00:00 00:00 0N 23:59 23:59:59.999999999 0Wu";
        final String outText1 = "-0W -23:59 -23:59 00:00 00:00 0N 23:59 23:59 0Wu";
        final String hex1 = "0x1100090000000100008061faffff61faffff0000000000000000000000809f0500009f050000ffffff7f";

        final QMinuteV t1 = parseExpr(inText1);
        checkEquals(outText1, hex1, t1);

        final String text2 = "-01:23 -00:45 00:00 22:34";
        final String hex2 = "0x110004000000adffffffd3ffffff000000004a050000";
        final QMinuteV t2 = parseExpr(text2);
        checkEquals(text2, hex2, t2);
    }

    @Test
    public void testListMinuteV() {

        final String inText1 = "( -0wu; \t -23:59:59.999999999u ; \t -23:59 ; \t -00:00 \t ; \t 00:00 \t ; \t 0Nu \t ; \t 23:59; \t 23:59:59.999999999u \t ;0Wu)";
        final String outText1 = "-0W -23:59 -23:59 00:00 00:00 0N 23:59 23:59 0Wu";
        final QMinuteV t1 = parseExpr(inText1);
        final String hex = "0x1100090000000100008061faffff61faffff0000000000000000000000809f0500009f050000ffffff7f";
        checkEquals(outText1, hex, t1);

        final String inText2 = "( \t -01:23 \t ; \t -00:45 \t ; 00:00 \t ; \t 22:34 \t )";
        final String outText2 = "-01:23 -00:45 00:00 22:34";
        final String hex2 = "0x110004000000adffffffd3ffffff000000004a050000";
        final QMinuteV t2 = parseExpr(inText2);
        checkEquals(outText2, hex2, t2);
    }

    @Test
    public void testSingletonMinuteV() {
        final String x = "22:34";
        final String hex1 = "0x1100010000004a050000";
        final QMinuteV t1 = parseExpr("enlist " + x);
        checkEquals("," + x, hex1, t1);
        final QMinuteV t2 = parseExpr(String.format("enlist (%s)", x));
        checkEquals("," + x, hex1, t2);
        final QMinuteV t3 = parseExpr("`s#enlist " + x);
        checkEquals("`s#," + x, "0x1101010000004a050000", t3);
    }

    @Test
    public void testEnclosedMinuteV() {
        final String text = "01:23 00:00 22:34";
        final String hex = "0x11000300000053000000000000004a050000";
        final QMinuteV t1 = parseExpr(String.format("( \t %s \t )", text));
        checkEquals(text, hex, t1);
        final QMinuteV t2 = parseExpr(String.format("( \t ( \t %s \t ) \t )", text));
        checkEquals(text, hex, t2);
    }

    @Test
    public void testAttrMinuteV() {
        final String text = "`s#00:00 12:13 23:59";
        final String hex1 = "0x11010300000000000000dd0200009f050000";
        final QMinuteV t1 = parseExpr(text);
        checkEquals(text, hex1, t1);
    }

}
