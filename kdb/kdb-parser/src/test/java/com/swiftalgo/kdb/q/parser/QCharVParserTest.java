package com.swiftalgo.kdb.q.parser;

/*-
 * #%L
 * KDB+ Parser
 * %%
 * Copyright (C) 2017 SwiftAlgo Limited
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.swiftalgo.kdb.q.QCharV;
import org.junit.Test;

public class QCharVParserTest extends QTypeParserTest<QCharV> {

    @Test
    public void testCompactCharV() {
        final QCharV t1 = parseExpr("\"abc\"");
        checkEquals("\"abc\"", "0x0a0003000000616263", t1);
    }

    @Test
    public void testListCharV() {
        final QCharV t1 = parseExpr("( \"a\" ; \"b\" ; \"c\" )");
        checkEquals("\"abc\"", "0x0a0003000000616263", t1);
        final QCharV t2 = parseExpr("( (\"a\") ;( \t \"b\" \t ); ((\"c\")) )");
        checkEquals("\"abc\"", "0x0a0003000000616263", t1);

    }

    @Test
    public void testSingletonCharV() {
        final QCharV t1 = parseExpr("enlist \"a\"");
        checkEquals(",\"a\"", "0x0a000100000061", t1);
    }

    @Test
    public void testAttrCharV() {
        final QCharV t1 = parseExpr("`s#enlist \"a\"");
        checkEquals("`s#,\"a\"", "0x0a010100000061", t1);
        final QCharV t2 = parseExpr("`s# \t ( \t ( \t \"abc\" \t ) \t )");
        checkEquals("`s#\"abc\"", "0x0a0103000000616263", t2);
    }

}
