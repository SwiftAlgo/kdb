package com.swiftalgo.kdb.q.parser;

/*-
 * #%L
 * KDB+ Parser
 * %%
 * Copyright (C) 2017 SwiftAlgo Limited
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.swiftalgo.kdb.q.QInt;
import org.junit.Test;

public class QIntParserTest extends QTypeParserTest<QInt> {

    @Test
    public void testIntA() {

        final QInt t1 = parseExpr("0i");
        checkEquals("0i", "0xfa00000000", t1);

        final QInt t2 = parseExpr("1i");
        checkEquals("1i", "0xfa01000000", t2);

        final QInt t3 = parseExpr("-1i");
        checkEquals("-1i", "0xfaffffffff", t3);

        //null
        final QInt t8 = parseExpr("0Ni");
        checkEquals("0Ni", "0xfa00000080", t8);

        //+/- infinity
        final QInt t9 = parseExpr("-0Wi");
        checkEquals("-0Wi", "0xfa01000080", t9);

        final QInt t10 = parseExpr("-0wi");
        checkEquals("-0Wi", "0xfa01000080", t10);

        final QInt t11 = parseExpr("-2147483647i"); //Integer.MIN_VALUE + 1
        checkEquals("-0Wi", "0xfa01000080", t11);

        final QInt t12 = parseExpr("0Wi");
        checkEquals("0Wi", "0xfaffffff7f", t12);

        final QInt t13 = parseExpr("0wi");
        checkEquals("0Wi", "0xfaffffff7f", t13);

        final QInt t14 = parseExpr("2147483647i"); //Integer.MAX_VALUE
        checkEquals("0Wi", "0xfaffffff7f", t14);
    }

    @Test(expected = NumberFormatException.class)
    public void testIntALowerBoundException() {
        parseExpr(Integer.MIN_VALUE + "i");
    }

    @Test(expected = NumberFormatException.class)
    public void testIntAUpperBoundException() {
        parseExpr((((long) Integer.MAX_VALUE) + 1) + "i");
    }

}
