package com.swiftalgo.kdb.q.parser;

/*-
 * #%L
 * KDB+ Parser
 * %%
 * Copyright (C) 2017 SwiftAlgo Limited
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.swiftalgo.kdb.q.QDate;
import org.junit.Test;

public class QDateParserTest extends QTypeParserTest<QDate> {

    @Test
    public void testJavaEpoch() {
        final String text = "1970.01.01";
        final String hex = "0xf233d5ffff";
        final QDate t1 = parseExpr(text);
        checkEquals(text, hex, t1);
        final QDate t2 = parseExpr("1970.01.01d");
        checkEquals(text, hex, t2);
    }

    @Test
    public void testQEpoch() {
        final String qEpochText = "2000.01.01";
        final String qEpochHex = "0xf200000000";
        final QDate t1 = parseExpr(qEpochText);
        checkEquals(qEpochText, qEpochHex, t1);
        final QDate t2 = parseExpr("2000.01.01d");
        checkEquals(qEpochText, qEpochHex, t2);
    }

    @Test
    public void testNull() {
        final String nullText = "0Nd";
        final String nullHex = "0xf200000080";
        final QDate t1 = parseExpr(nullText);
        checkEquals(nullText, nullHex, t1);
        final String lowerNullText = "0nd";
        final QDate t2 = parseExpr(lowerNullText);
        checkEquals(nullText, nullHex, t2);
    }

    @Test
    public void testPositiveInfinity() {
        final String posInfText = "0Wd";
        final String posInfHex = "0xf2ffffff7f";
        final QDate t1 = parseExpr(posInfText);
        checkEquals(posInfText, posInfHex, t1);
        final String lowerInfText = "0wd";
        final QDate t2 = parseExpr(lowerInfText);
        checkEquals(posInfText, posInfHex, t2);
    }

    @Test
    public void testNegativeInfinity() {
        final String negInfText = "-0Wd";
        final String negInfHex = "0xf201000080";
        final QDate t1 = parseExpr(negInfText);
        checkEquals(negInfText, negInfHex, t1);
        final String lowerNegInfText = "-0wd";
        final QDate t2 = parseExpr(lowerNegInfText);
        checkEquals(negInfText, negInfHex, t2);
    }

    @Test
    public void testMinimum() {
        final String text = "1709.01.01";
        final String hex = "0xf2d360feff";
        final QDate t4 = parseExpr(text);
        checkEquals(text, hex, t4);
        final QDate t5 = parseExpr("1709.01.01d");
        checkEquals(text, hex, t5);
    }

    @Test
    public void testMaximum() {
        final String text = "2290.12.31";
        final String hex = "0xf22d9f0100";
        final QDate t1 = parseExpr(text);
        checkEquals(text, hex, t1);
        final QDate t7 = parseExpr("2290.12.31d");
        checkEquals(text, hex, t7);
    }

    @Test
    public void testNegative() {
        final String date1 = "2015.05.27";
        final String date2 = "1984.08.07";
        final String hex1 = "0xf207eaffff";
        final String hex2 = "0xf2f9150000";
        final QDate t1a = parseExpr('-' + date1);
        checkEquals(date2, hex1, t1a);
        final QDate t1b = parseExpr('-' + date1 + 'd');
        checkEquals(date2, hex1, t1b);
        final QDate t2a = parseExpr('-' + date2);
        checkEquals(date1, hex2, t2a);
        final QDate t2b = parseExpr('-' + date2 + 'd');
        checkEquals(date1, hex2, t2b);
    }

}
