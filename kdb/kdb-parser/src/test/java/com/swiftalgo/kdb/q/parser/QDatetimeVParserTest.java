package com.swiftalgo.kdb.q.parser;

/*-
 * #%L
 * KDB+ Parser
 * %%
 * Copyright (C) 2017 SwiftAlgo Limited
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.swiftalgo.kdb.q.QDatetimeV;
import org.junit.Test;

public class QDatetimeVParserTest extends QTypeParserTest<QDatetimeV> {

    @Test
    public void testCompactDatetimeV() {
        final String inText1 = "-0W 1709.01.01T00:00:00.000000000 0N 2290.12.31T23:59:59.999999999 0Wz";
        final String outText1 = "-0w 1709.01.01T00:00:00.000 0N 2290.12.31T23:59:59.999 0wz"; //TODO Trailing 'z' is superflous.
        final String hex1 = "0x0f0005000000000000000000f0ff00000000d0f2f9c0000000000000f8ffe5fcffffdff2f940000000000000f07f";

        final QDatetimeV t1 = parseExpr(inText1);
        checkEquals(outText1, hex1, t1);

        final String inText2a = "1970.01.01T01:23:34.567000000 2000.01.01T00:00:00.000000000 2015.05.28T22:34:53.123456789";
        final String outText2 = "1970.01.01T01:23:34.567 2000.01.01T00:00:00.000 2015.05.28T22:34:53.123";
        final String hex2 = "0x0f00030000002dd52d927866c5c000000000000000005dd756def0fab540";
        final QDatetimeV t2a = parseExpr(inText2a);
        checkEquals(outText2, hex2, t2a);

        final String text2b = "1970.01.01T01:23:34.567 2000.01.01T00:00:00 2015.05.28T22:34:53.123456789z";
        final QDatetimeV t2b = parseExpr(text2b);
        checkEquals(outText2, hex2, t2b);
    }

    @Test
    public void testListDatetimeV() {
        final String inText1 = "( -0wz; \t 1709.01.01T00:00:00.000000000; 0Nz;2290.12.31T23:59:59.999999999z \t ;0Wz)";
        final String outText1 = "-0w 1709.01.01T00:00:00.000 0N 2290.12.31T23:59:59.999 0wz"; //TODO Trailing 'z' is superflous.
        final QDatetimeV t1 = parseExpr(inText1);
        final String hex = "0x0f0005000000000000000000f0ff00000000d0f2f9c0000000000000f8ffe5fcffffdff2f940000000000000f07f";
        checkEquals(outText1, hex, t1);

        final String inText2a = "(1970.01.01T01:23:34.567;2000.01.01T00:00:00;2015.05.28T22:34:53.123456789)";
        final String outText2 = "1970.01.01T01:23:34.567 2000.01.01T00:00:00.000 2015.05.28T22:34:53.123";
        final String hex2 = "0x0f00030000002dd52d927866c5c000000000000000005dd756def0fab540";
        final QDatetimeV t2a = parseExpr(inText2a);
        checkEquals(outText2, hex2, t2a);

        final String inText2b = "(1970.01.01T01:23:34.567;2000.01.01T00:00:00;2015.05.28T22:34:53.123456789)";
        final QDatetimeV t2b = parseExpr(inText2b);
        checkEquals(outText2, hex2, t2b);
    }

    @Test
    public void testSingletonDatetimeV() {
        final String x = "2015.05.28T22:34:53.987";
        final String hex1 = "0x0f00010000000a9dfedef0fab540";
        final QDatetimeV t1 = parseExpr("enlist " + x);
        checkEquals("," + x, hex1, t1);
        final QDatetimeV t2 = parseExpr(String.format("enlist (%s)", x));
        checkEquals("," + x, hex1, t2);
        final QDatetimeV t3 = parseExpr("`s#enlist " + x);
        checkEquals("`s#," + x, "0x0f01010000000a9dfedef0fab540", t3);
    }

    @Test
    public void testEnclosedDatetimeV() {
        final String inText1 = "1970.01.01T01:23:34.567 2000.01.01T00:00:00 2015.05.28T22:34:53.123456789";
        final String outText1 = "1970.01.01T01:23:34.567 2000.01.01T00:00:00.000 2015.05.28T22:34:53.123";
        final QDatetimeV t1 = parseExpr(String.format("( \t %s \t )", inText1));
        final String hex = "0x0f00030000002dd52d927866c5c000000000000000005dd756def0fab540";
        checkEquals(outText1, hex, t1);
        final QDatetimeV t2 = parseExpr(String.format("( \t ( \t %s \t ) \t )", inText1));
        checkEquals(outText1, hex, t2);
    }

    @Test
    public void testAttrDatetimeV() {
        final String inText1 = "`s#1970.01.01T01:23:34.567899999 2000.01.01T00:00:00.000000000 2015.05.28T22:34:53.123456789";
        final String outText1 = "`s#1970.01.01T01:23:34.567 2000.01.01T00:00:00.000 2015.05.28T22:34:53.123";
        final String hex1 = "0x0f01030000002dd52d927866c5c000000000000000005dd756def0fab540";
        final QDatetimeV t1 = parseExpr(inText1);
        checkEquals(outText1, hex1, t1);
    }

    @Test
    public void testNegativeDatetimeV() {
        final String inText1 = "-1970.01.01T01:23:34.567 -2000.01.01T12:34:56.987654321 2000.01.01T12:34:56.987654321 -2015.05.28T22:34:53.123456789";
        final String outText1 = "2029.12.30T22:36:25.433 1999.12.31T11:25:03.013 2000.01.01T12:34:56.987 1984.08.05T01:25:06.877";
        final QDatetimeV t1 = parseExpr(inText1);
        final String hex = "0x0f00040000002dd52d927866c54027c1a54fd3c6e0bf27c1a54fd3c6e03f5dd756def0fab5c0";
        checkEquals(outText1, hex, t1);
    }

}
