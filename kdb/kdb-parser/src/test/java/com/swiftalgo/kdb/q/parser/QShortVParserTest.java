package com.swiftalgo.kdb.q.parser;

/*-
 * #%L
 * KDB+ Parser
 * %%
 * Copyright (C) 2017 SwiftAlgo Limited
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.swiftalgo.kdb.q.QShortV;
import org.junit.Test;

public class QShortVParserTest extends QTypeParserTest<QShortV> {

    @Test
    public void testCompactShortV() {
        final QShortV t1 = parseExpr("-3 -2 -1 0 0N 1 2 3h");
        checkEquals("-3 -2 -1 0 0N 1 2 3h", "0x050008000000fdfffeffffff00000080010002000300", t1);
        final QShortV t2 = parseExpr("-32768 -32767 32767 32768h"); //massaging Integer values to Short range.
        checkEquals("-32767 -32767 32767 32767h", "0x05000400000001800180ff7fff7f", t2);
        final QShortV t3 = parseExpr("1 \t 2 \t 3h");
        checkEquals("1 2 3h", "0x050003000000010002000300", t3);
    }

    @Test
    public void testListShortV() {
        final QShortV t1 = parseExpr("(-3h;-2h;-1h;0h;0Nh;1h;2h;3h)");
        checkEquals("-3 -2 -1 0 0N 1 2 3h", "0x050008000000fdfffeffffff00000080010002000300", t1);
        final QShortV t2 = parseExpr("( \t 1h \t ; \t 2h \t; \t 3h \t )");
        checkEquals("1 2 3h", "0x050003000000010002000300", t2);
        final QShortV t3 = parseExpr("( \t (1h) \t ;( \t 2h \t); (\t (3h) \t) )");
        checkEquals("1 2 3h", "0x050003000000010002000300", t3);
    }

    @Test(expected = NumberFormatException.class)
    public void testListShortVLowerRange() {
        parseExpr(Integer.MIN_VALUE + " 0h");
    }

    @Test(expected = NumberFormatException.class)
    public void testListShortVUpperRange() {
        parseExpr(((long) Integer.MAX_VALUE + 1) + " 0h");
    }

    @Test
    public void testEnclosedShortV() {
        final QShortV t1 = parseExpr("( \t 1 2 3h \t )");
        checkEquals("1 2 3h", "0x050003000000010002000300", t1);
        final QShortV t2 = parseExpr("( \t ( \t 1 2 3h \t ) \t )");
        checkEquals("1 2 3h", "0x050003000000010002000300", t2);
    }

    @Test
    public void testSingletonShortV() {
        final QShortV t1 = parseExpr("enlist 1h");
        checkEquals(",1h", "0x0500010000000100", t1);
        final QShortV t2 = parseExpr("enlist (1h)");
        checkEquals(",1h", "0x0500010000000100", t2);
        final QShortV t3 = parseExpr("`s#enlist 1h");
        checkEquals("`s#,1h", "0x0501010000000100", t3);
    }

}
