package com.swiftalgo.kdb.q.parser;

/*-
 * #%L
 * KDB+ Parser
 * %%
 * Copyright (C) 2017 SwiftAlgo Limited
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.swiftalgo.kdb.q.QFloat;
import org.junit.Test;

public class QFloatParserTest extends QTypeParserTest<QFloat> {

    @Test
    public void testFloatWithSuffix() {
        final QFloat t1 = parseExpr("-3.141592653589793f");
        checkEquals("-3.141592653589793", "0xf7182d4454fb2109c0", t1);
        final QFloat t2 = parseExpr("-1f");
        checkEquals("-1f", "0xf7000000000000f0bf", t2);
        final QFloat t3 = parseExpr("-0.f");
        checkEquals("0f", "0xf70000000000000080", t3);
        final QFloat t4 = parseExpr("0.f");
        checkEquals("0f", "0xf70000000000000000", t4);
        final QFloat t5 = parseExpr("0.0f");
        checkEquals("0f", "0xf70000000000000000", t5);
        final QFloat t6 = parseExpr(".0f");
        checkEquals("0f", "0xf70000000000000000", t6);
        final QFloat t7 = parseExpr("2f");
        checkEquals("2f", "0xf70000000000000040", t7);
        final QFloat t8 = parseExpr("2.1f");
        checkEquals("2.1", "0xf7cdcccccccccc0040", t8);
        final QFloat t9 = parseExpr("3.141592653589793f");
        checkEquals("3.141592653589793", "0xf7182d4454fb210940", t9);
    }

    @Test
    public void testFloatNoSuffix() {
        final QFloat t1 = parseExpr("-3.141592653589793");
        checkEquals("-3.141592653589793", "0xf7182d4454fb2109c0", t1);
        final QFloat t2 = parseExpr("-1.");
        checkEquals("-1f", "0xf7000000000000f0bf", t2);
        final QFloat t3 = parseExpr("-0.");
        checkEquals("0f", "0xf70000000000000080", t3);
        final QFloat t4 = parseExpr("0.");
        checkEquals("0f", "0xf70000000000000000", t4);
        final QFloat t5 = parseExpr("0.0");
        checkEquals("0f", "0xf70000000000000000", t5);
        final QFloat t6 = parseExpr(".0");
        checkEquals("0f", "0xf70000000000000000", t6);
        final QFloat t7 = parseExpr("2.");
        checkEquals("2f", "0xf70000000000000040", t7);
        final QFloat t8 = parseExpr("2.1");
        checkEquals("2.1", "0xf7cdcccccccccc0040", t8);
        final QFloat t9 = parseExpr("3.141592653589793");
        checkEquals("3.141592653589793", "0xf7182d4454fb210940", t9);
    }

    @Test
    public void testFloatSpecialValues() {
        final QFloat t1 = parseExpr("-0wf");
        checkEquals("-0w", "0xf7000000000000f0ff", t1);
        final QFloat t2 = parseExpr("-0Wf");
        checkEquals("-0w", "0xf7000000000000f0ff", t2);
        final QFloat t3 = parseExpr("-0w");
        checkEquals("-0w", "0xf7000000000000f0ff", t3);

        final QFloat t4 = parseExpr("0nf");
        checkEquals("0n", "0xf7000000000000f8ff", t4);
        final QFloat t5 = parseExpr("0Nf");
        checkEquals("0n", "0xf7000000000000f8ff", t5);
        final QFloat t6 = parseExpr("0n");
        checkEquals("0n", "0xf7000000000000f8ff", t6);

        final QFloat t7 = parseExpr("0wf");
        checkEquals("0w", "0xf7000000000000f07f", t7);
        final QFloat t8 = parseExpr("0Wf");
        checkEquals("0w", "0xf7000000000000f07f", t8);
        final QFloat t9 = parseExpr("0w");
        checkEquals("0w", "0xf7000000000000f07f", t9);
    }

    @Test
    public void testFloatExponentialIn() {
        final QFloat t1 = parseExpr("-3141592653589793e-15f");
        checkEquals("-3.141592653589793", "0xf7182d4454fb2109c0", t1);
        final QFloat t2 = parseExpr("3141592653589793e-15f");
        checkEquals("3.141592653589793", "0xf7182d4454fb210940", t2);
        final QFloat t3 = parseExpr("-0.03141592653589793e2f");
        checkEquals("-3.141592653589793", "0xf7182d4454fb2109c0", t3);
        final QFloat t4 = parseExpr("0.03141592653589793e2f");
        checkEquals("3.141592653589793", "0xf7182d4454fb210940", t4);
        final QFloat t5 = parseExpr("-0.03141592653589793e+2f");
        checkEquals("-3.141592653589793", "0xf7182d4454fb2109c0", t5);
        final QFloat t6 = parseExpr("0.03141592653589793e+2f");
        checkEquals("3.141592653589793", "0xf7182d4454fb210940", t6);
    }

    @Test
    public void testFloatExponentialInOut() {
        //TODO same double rounding.
        final QFloat t1 = parseExpr("-3.1415926535897936e+64"); //not p
        checkEquals("-3.141592653589794e+64", "0xf73e670c768b1753cd", t1);
        final QFloat t2 = parseExpr("-3.1415926535897932e+64");
        checkEquals("-3.141592653589793e+64", "0xf73d670c768b1753cd", t2);
        final QFloat t3 = parseExpr("-3.1415926535897932e-64");
        checkEquals("-3.141592653589793e-64", "0xf751acefc7db8ac0b2", t3);
        final QFloat t4 = parseExpr("3.1415926535897932e-64");
        checkEquals("3.141592653589793e-64", "0xf751acefc7db8ac032", t4);
        final QFloat t5 = parseExpr("3.1415926535897932e+64");
        checkEquals("3.141592653589793e+64", "0xf73d670c768b17534d", t5);
        final QFloat t6 = parseExpr("3.1415926535897936e+64"); //not pi
        checkEquals("3.141592653589794e+64", "0xf73e670c768b17534d", t6);
    }

    @Test
    public void testFloatIntegerBoundaries() {
        final String minText = Long.toString(Long.MIN_VALUE) + 'f';
        final QFloat t1 = parseExpr(minText);
        checkEquals(minText, "0xf7000000000000e0c3", t1);
        final String maxText = Long.toString(Long.MAX_VALUE) + 'f';
        final QFloat t2 = parseExpr(maxText);
        checkEquals(maxText, "0xf7000000000000e043", t2);
    }

}
