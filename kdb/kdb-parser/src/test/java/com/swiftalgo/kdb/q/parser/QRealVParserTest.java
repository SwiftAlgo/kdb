package com.swiftalgo.kdb.q.parser;

/*-
 * #%L
 * KDB+ Parser
 * %%
 * Copyright (C) 2017 SwiftAlgo Limited
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.swiftalgo.kdb.q.QRealV;
import org.junit.Test;

public class QRealVParserTest extends QTypeParserTest<QRealV> {

    @Test
    public void testCompactRealV() {
        final QRealV t1 = parseExpr("-31415926.5e-7 -3.14159265 -1\t-1e0 -0. -0.e0  0. 0.e0 0.0 0.0e0 .0 .0e0 .1 .1e0 2 2e0 2.1 2.1e0 3.14159265 31415926.5e-7e");
        checkEquals("-3.1415927 -3.1415927 -1 -1 0 0 0 0 0 0 0 0 0.1 0.1 2 2 2.1 2.1 3.1415927 3.1415927e", "0x080014000000db0f49c0db0f49c0000080bf000080bf0000008000000080000000000000000000000000000000000000000000000000cdcccc3dcdcccc3d00000040000000406666064066660640db0f4940db0f4940", t1);
    }

    @Test
    public void testListRealV() {
        final QRealV t1 = parseExpr("(-31415926.5e-7e; \t -3.14159265e \t ;  -1e  ;\t\t-0.e\t\t;0.e;0.0e;.0e;.1e;2e;2.1e;3.14159265e;31415926.5e-7e  )");
        checkEquals("-3.1415927 -3.1415927 -1 0 0 0 0 0.1 2 2.1 3.1415927 3.1415927e", "0x08000c000000db0f49c0db0f49c0000080bf00000080000000000000000000000000cdcccc3d0000004066660640db0f4940db0f4940", t1);
        final QRealV t2 = parseExpr("((-31415926.5e-7e); (-3.14159265e); \t (-1e) \t ;( \t -0.e \t );((0.e));(0.0e);(.0e);(.1e);(2e);(2.1e);(3.14159265e);(31415926.5e-7e))");
        checkEquals("-3.1415927 -3.1415927 -1 0 0 0 0 0.1 2 2.1 3.1415927 3.1415927e", "0x08000c000000db0f49c0db0f49c0000080bf00000080000000000000000000000000cdcccc3d0000004066660640db0f4940db0f4940", t2);
    }

}
