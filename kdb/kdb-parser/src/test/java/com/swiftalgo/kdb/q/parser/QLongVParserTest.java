package com.swiftalgo.kdb.q.parser;

/*-
 * #%L
 * KDB+ Parser
 * %%
 * Copyright (C) 2017 SwiftAlgo Limited
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.swiftalgo.kdb.q.QLongV;
import org.junit.Test;

public class QLongVParserTest extends QTypeParserTest<QLongV> {

    @Test
    public void testCompactLongV() {
        final String hex1 = "0x070008000000fdfffffffffffffffeffffffffffffffffffffffffffffff00000000000000000000000000000080010000000000000002000000000000000300000000000000";
        final QLongV t1 = parseExpr("-3 -2 -1 0 0N 1 2 3j");
        checkEquals("-3 -2 -1 0 0N 1 2 3", hex1, t1);
        final QLongV t2 = parseExpr("-3 -2 -1 0 0N 1 2 3");
        checkEquals("-3 -2 -1 0 0N 1 2 3", hex1, t2);

        final String longRangeNoSuffix = (Long.MIN_VALUE + 1) + " 0N " + Long.MAX_VALUE;
        final String longRange = longRangeNoSuffix + "j";
        final String hex2 = "0x07000300000001000000000000800000000000000080ffffffffffffff7f";
        final QLongV t3 = parseExpr(longRange);
        checkEquals("-0W 0N 0W", hex2, t3);
        final QLongV t4 = parseExpr(longRangeNoSuffix);
        checkEquals("-0W 0N 0W", hex2, t4);
    }

    @Test
    public void testListLongV() {
        final QLongV t1 = parseExpr("(-3j;-2j;-1j;0j;0Nj;1j;2j;3j)");
        final String hex = "0x070008000000fdfffffffffffffffeffffffffffffffffffffffffffffff00000000000000000000000000000080010000000000000002000000000000000300000000000000";
        checkEquals("-3 -2 -1 0 0N 1 2 3", hex, t1);
        final QLongV t2 = parseExpr("(-3;-2;-1;0;0N;1;2;3)");
        checkEquals("-3 -2 -1 0 0N 1 2 3", hex, t2);
        final QLongV t3 = parseExpr("((-3);(-2); \t (-1) \t ;( \t 0 \t );((0N));(1);(2);(3))");
        checkEquals("-3 -2 -1 0 0N 1 2 3", hex, t3);
    }

    @Test
    public void testSingletonLongV() {
        final QLongV t1 = parseExpr("enlist 1j");
        checkEquals(",1", "0x0700010000000100000000000000", t1);
        final QLongV t2 = parseExpr("enlist (1j)");
        checkEquals(",1", "0x0700010000000100000000000000", t2);
        final QLongV t3 = parseExpr("`s#enlist 1j");
        checkEquals("`s#,1", "0x0701010000000100000000000000", t3);
    }

    @Test
    public void testEnclosedLongV() {
        final QLongV t1 = parseExpr("( \t 1 2 3j \t )");
        final String hex = "0x070003000000010000000000000002000000000000000300000000000000";
        checkEquals("1 2 3", hex, t1);
        final QLongV t2 = parseExpr("( \t ( \t 1 2 3j \t ) \t )");
        checkEquals("1 2 3", hex, t2);
    }

    @Test
    public void testAttrLongV() {
        final String hex1 = "0x070107000000fdfffffffffffffffeffffffffffffffffffffffffffffff0000000000000000010000000000000002000000000000000300000000000000";
        final QLongV t1 = parseExpr("`s#-3 -2 -1 0 1 2 3j");
        checkEquals("`s#-3 -2 -1 0 1 2 3", hex1, t1);
        final QLongV t2 = parseExpr("`s#-3 -2 -1 0 1 2 3");
        checkEquals("`s#-3 -2 -1 0 1 2 3", hex1, t2);
        final String hex2 = "0x070103000000010000000000000002000000000000000300000000000000";
        final QLongV t3 = parseExpr("`s# \t ( \t ( \t 1 2 3j \t ) \t )");
        checkEquals("`s#1 2 3", hex2, t3);
        final QLongV t4 = parseExpr("`s# \t ( \t ( \t 1 2 3 \t ) \t )");
        checkEquals("`s#1 2 3", hex2, t4);
    }

}
