package com.swiftalgo.kdb.q.parser;

/*-
 * #%L
 * KDB+ Parser
 * %%
 * Copyright (C) 2017 SwiftAlgo Limited
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.swiftalgo.kdb.q.QIntV;
import org.junit.Test;

public class QIntVParserTest extends QTypeParserTest<QIntV> {

    @Test
    public void testCompactIntV() {
        final QIntV t1 = parseExpr("-3 -2 -1 0 0N 1 2 3i");
        checkEquals("-3 -2 -1 0 0N 1 2 3i", "0x060008000000fdfffffffeffffffffffffff0000000000000080010000000200000003000000", t1);
        final QIntV t2 = parseExpr(Integer.toString(Integer.MIN_VALUE + 1) + " 0 " + Integer.MAX_VALUE + "i");
        checkEquals("-0W 0 0Wi", "0x0600030000000100008000000000ffffff7f", t2);
    }

    @Test
    public void testListIntV() {
        final QIntV t1 = parseExpr("(-3i;-2i;-1i;0i;0Ni;1i;2i;3i)");
        checkEquals("-3 -2 -1 0 0N 1 2 3i", "0x060008000000fdfffffffeffffffffffffff0000000000000080010000000200000003000000", t1);
        final QIntV t2 = parseExpr("((-3i);(-2i); \t (-1i) \t ;( \t 0i \t );((0Ni));(1i);(2i);(3i))");
        checkEquals("-3 -2 -1 0 0N 1 2 3i", "0x060008000000fdfffffffeffffffffffffff0000000000000080010000000200000003000000", t2);
    }

    @Test
    public void testSingletonIntV() {
        final QIntV t1 = parseExpr("enlist 1i");
        checkEquals(",1i", "0x06000100000001000000", t1);
        final QIntV t2 = parseExpr("enlist (1i)");
        checkEquals(",1i", "0x06000100000001000000", t2);
        final QIntV t3 = parseExpr("`s#enlist 1i");
        checkEquals("`s#,1i", "0x06010100000001000000", t3);
    }

    @Test
    public void testEnclosedIntV() {
        final QIntV t1 = parseExpr("( \t 1 2 3i \t )");
        checkEquals("1 2 3i", "0x060003000000010000000200000003000000", t1);
        final QIntV t2 = parseExpr("( \t ( \t 1 2 3i \t ) \t )");
        checkEquals("1 2 3i", "0x060003000000010000000200000003000000", t2);
    }

    @Test
    public void testAttrIntV() {
        final QIntV t1 = parseExpr("`s#-3 -2 -1 0 1 2 3i");
        checkEquals("`s#-3 -2 -1 0 1 2 3i", "0x060107000000fdfffffffeffffffffffffff00000000010000000200000003000000", t1);
        final QIntV t2 = parseExpr("`s# \t ( \t ( \t 1 2 3i \t ) \t )");
        checkEquals("`s#1 2 3i", "0x060103000000010000000200000003000000", t2);
    }

}
