package com.swiftalgo.kdb.q.parser;

/*-
 * #%L
 * KDB+ Parser
 * %%
 * Copyright (C) 2017 SwiftAlgo Limited
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.swiftalgo.kdb.q.QByte;
import org.junit.Test;

public class QByteParserTest extends QTypeParserTest<QByte> {

    @Test
    public void testByteA() {
        final QByte t1 = parseExpr("0xff");
        checkEquals("0xff", "0xfcff", t1);

        final QByte t2 = parseExpr("0xFF");
        checkEquals("0xff", "0xfcff", t2);

        final QByte t3 = parseExpr("0x00");
        checkEquals("0x00", "0xfc00", t3);

        final QByte t4 = parseExpr("0x12");
        checkEquals("0x12", "0xfc12", t4);
    }

}
