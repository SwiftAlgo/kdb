package com.swiftalgo.kdb.q.parser;

/*-
 * #%L
 * KDB+ Parser
 * %%
 * Copyright (C) 2017 SwiftAlgo Limited
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.swiftalgo.kdb.q.QTimestampV;
import org.junit.Test;

public class QTimestampVParserTest extends QTypeParserTest<QTimestampV> {

    @Test
    public void testCompactTimestampV() {
        final String text1 = "-0W 1709.01.01D00:00:00.000000000 0N 2290.12.31D23:59:59.999999999 0Wp";
        final String hex1 = "0x0c0005000000010000000000008000001d6453588f800000000000000080ffff312d41f6707fffffffffffffff7f";

        final QTimestampV t1 = parseExpr(text1);
        checkEquals(text1, hex1, t1);

        final String outText2 = "1970.01.01D01:23:34.567000000 2000.01.01D00:00:00.000000000 2015.05.28D22:34:53.123456789";
        final String hex2 = "0x0c0003000000c0173939c0b7dcf20000000000000000150fe5e9f036bf06";
        final QTimestampV t2a = parseExpr(outText2);
        checkEquals(outText2, hex2, t2a);

        final String text2b = "1970.01.01D01:23:34.567 2000.01.01D00:00:00 2015.05.28D22:34:53.123456789p";
        final QTimestampV t2b = parseExpr(text2b);
        checkEquals(outText2, hex2, t2b);
    }

    @Test
    public void testListTimestampV() {
        final String inText1 = "( -0wp; \t 1709.01.01D00:00:00.000000000; 0Np;2290.12.31D23:59:59.999999999p \t ;0Wp)";
        final String outText1 = "-0W 1709.01.01D00:00:00.000000000 0N 2290.12.31D23:59:59.999999999 0Wp";
        final QTimestampV t1 = parseExpr(inText1);
        final String hex = "0x0c0005000000010000000000008000001d6453588f800000000000000080ffff312d41f6707fffffffffffffff7f";
        checkEquals(outText1, hex, t1);

        final String outText2 = "1970.01.01D01:23:34.567000000 2000.01.01D00:00:00.000000000 2015.05.28D22:34:53.123456789";
        final String inText2a = "(1970.01.01D01:23:34.567;2000.01.01D00:00:00;2015.05.28D22:34:53.123456789)";
        final String hex2 = "0x0c0003000000c0173939c0b7dcf20000000000000000150fe5e9f036bf06";
        final QTimestampV t2a = parseExpr(inText2a);
        checkEquals(outText2, hex2, t2a);

        final String inText2b = "(1970.01.01D01:23:34.567;2000.01.01D00:00:00;2015.05.28D22:34:53.123456789)";
        final QTimestampV t2b = parseExpr(inText2b);
        checkEquals(outText2, hex2, t2b);
    }

    @Test
    public void testSingletonTimestampV() {
        final String x = "2015.05.28D22:34:53.987654321";
        final String hex1 = "0x0c0001000000b1aa671df136bf06";
        final QTimestampV t1 = parseExpr("enlist " + x);
        checkEquals("," + x, hex1, t1);
        final QTimestampV t2 = parseExpr(String.format("enlist (%s)", x));
        checkEquals("," + x, hex1, t2);
        final QTimestampV t3 = parseExpr("`s#enlist " + x);
        checkEquals("`s#," + x, "0x0c0101000000b1aa671df136bf06", t3);
    }

    @Test
    public void testEnclosedTimestampV() {
        final String inText1 = "1970.01.01D01:23:34.567 2000.01.01D00:00:00 2015.05.28D22:34:53.123456789";
        final String outText1 = "1970.01.01D01:23:34.567000000 2000.01.01D00:00:00.000000000 2015.05.28D22:34:53.123456789";
        final QTimestampV t1 = parseExpr(String.format("( \t %s \t )", inText1));
        final String hex = "0x0c0003000000c0173939c0b7dcf20000000000000000150fe5e9f036bf06";
        checkEquals(outText1, hex, t1);
        final QTimestampV t2 = parseExpr(String.format("( \t ( \t %s \t ) \t )", inText1));
        checkEquals(outText1, hex, t2);
    }

    @Test
    public void testAttrTimestampV() {
        final String text1 = "`s#1970.01.01D01:23:34.567000000 2000.01.01D00:00:00.000000000 2015.05.28D22:34:53.123456789";
        final String hex1 = "0x0c0103000000c0173939c0b7dcf20000000000000000150fe5e9f036bf06";
        final QTimestampV t1 = parseExpr(text1);
        checkEquals(text1, hex1, t1);
    }

    @Test
    public void testNegativeTimestampV() {
        final String inText1 = "-1970.01.01D01:23:34.567 -2000.01.01D12:34:56.987654321 2000.01.01D12:34:56.987654321 -2015.05.28D22:34:53.123456789";
        final String outText1 = "2029.12.30D22:36:25.433000000 1999.12.31D11:25:03.012345679 2000.01.01D12:34:56.987654321 1984.08.05D01:25:06.876543211";
        final QTimestampV t1 = parseExpr(inText1);
        final String hex = "0x0c000400000040e8c6c63f48230d4f372479cdd6ffffb1c8db8632290000ebf01a160fc940f9";
        checkEquals(outText1, hex, t1);
    }

}
