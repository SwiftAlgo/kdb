package com.swiftalgo.kdb.q.parser;

/*-
 * #%L
 * KDB+ Parser
 * %%
 * Copyright (C) 2017 SwiftAlgo Limited
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.swiftalgo.kdb.q.QSecondV;
import org.junit.Test;

public class QSecondVParserTest extends QTypeParserTest<QSecondV> {

    @Test
    public void testCompactSecondV() {
        final String inText1 = "-0W -23:59:59.123456 -23:59:59 -23:59 -00:00:00 00:00:00 0N 23:59 23:59:59 23:59:59.123456 0Wv";
        final String outText1 = "-0W -23:59:59 -23:59:59 -23:59:00 00:00:00 00:00:00 0N 23:59:00 23:59:59 23:59:59 0Wv";
        final String hex1 = "0x12000b0000000100008081aefeff81aefeffbcaefeff000000000000000000000080445101007f5101007f510100ffffff7f";

        final QSecondV t1 = parseExpr(inText1);
        checkEquals(outText1, hex1, t1);

        final String text2 = "-01:23:45 -00:45:09 00:00:00 22:34:01";
        final String hex2 = "0x1200040000005fecffff6bf5ffff00000000593d0100";
        final QSecondV t2 = parseExpr(text2);
        checkEquals(text2, hex2, t2);
    }

    @Test
    public void testListSecondV() {

        final String inText1 = "( -0wv; \t -23:59:59.123456v ; \t -23:59:59v \t ; \t -23:59v ; \t -00:00:00 \t ; \t 00:00:00 \t ; \t 0Nv \t ; \t 23:59v; \t 23:59:59 \t ; \t 23:59:59.123456v \t ;0Wv)";
        final String outText1 = "-0W -23:59:59 -23:59:59 -23:59:00 00:00:00 00:00:00 0N 23:59:00 23:59:59 23:59:59 0Wv";
        final QSecondV t1 = parseExpr(inText1);
        final String hex = "0x12000b0000000100008081aefeff81aefeffbcaefeff000000000000000000000080445101007f5101007f510100ffffff7f";
        checkEquals(outText1, hex, t1);

        final String inText2 = "( \t -01:23:45 \t ; \t -00:45:09 \t ; 00:00:00 \t ; \t 22:34:01 \t )";
        final String outText2 = "-01:23:45 -00:45:09 00:00:00 22:34:01";
        final String hex2 = "0x1200040000005fecffff6bf5ffff00000000593d0100";
        final QSecondV t2 = parseExpr(inText2);
        checkEquals(outText2, hex2, t2);
    }

    @Test
    public void testSingletonSecondV() {
        final String x = "22:34:56";
        final String hex1 = "0x120001000000903d0100";
        final QSecondV t1 = parseExpr("enlist " + x);
        checkEquals("," + x, hex1, t1);
        final QSecondV t2 = parseExpr(String.format("enlist (%s)", x));
        checkEquals("," + x, hex1, t2);
        final QSecondV t3 = parseExpr("`s#enlist " + x);
        checkEquals("`s#," + x, "0x120101000000903d0100", t3);
    }

    @Test
    public void testEnclosedSecondV() {
        final String text = "01:23:45 00:00:00 22:34:08";
        final String hex = "0x120003000000a113000000000000603d0100";
        final QSecondV t1 = parseExpr(String.format("( \t %s \t )", text));
        checkEquals(text, hex, t1);
        final QSecondV t2 = parseExpr(String.format("( \t ( \t %s \t ) \t )", text));
        checkEquals(text, hex, t2);
    }

    @Test
    public void testAttrSecondV() {
        final String text = "`s#00:00:00 12:13:14 23:59:59";
        final String hex1 = "0x12010300000000000000daab00007f510100";
        final QSecondV t1 = parseExpr(text);
        checkEquals(text, hex1, t1);
    }

}
