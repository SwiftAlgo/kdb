package com.swiftalgo.kdb.q.parser;

/*-
 * #%L
 * KDB+ Parser
 * %%
 * Copyright (C) 2017 SwiftAlgo Limited
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.swiftalgo.kdb.q.QMonth;
import org.junit.Test;

public class QMonthParserTest extends QTypeParserTest<QMonth> {

    @Test
    public void testJavaEpoch() {
        final String text = "1970.01m";
        final String hex = "0xf398feffff";
        final QMonth t1 = parseExpr(text);
        checkEquals(text, hex, t1);
    }

    @Test
    public void testQEpoch() {
        final String qEpochText = "2000.01m";
        final String qEpochHex = "0xf300000000";
        final QMonth t1 = parseExpr(qEpochText);
        checkEquals(qEpochText, qEpochHex, t1);
        final QMonth t2 = parseExpr('-' + qEpochText);
        checkEquals(qEpochText, qEpochHex, t2);
    }

    @Test
    public void testNull() {
        final String nullText = "0Nm";
        final String nullHex = "0xf300000080";
        final QMonth t1 = parseExpr(nullText);
        checkEquals(nullText, nullHex, t1);
        final String lowerNullText = "0nm";
        final QMonth t2 = parseExpr(lowerNullText);
        checkEquals(nullText, nullHex, t2);
    }

    @Test
    public void testPositiveInfinity() {
        final String posInfText = "0Wm";
        final String posInfHex = "0xf3ffffff7f";
        final QMonth t1 = parseExpr(posInfText);
        checkEquals(posInfText, posInfHex, t1);
        final String lowerInfText = "0wm";
        final QMonth t2 = parseExpr(lowerInfText);
        checkEquals(posInfText, posInfHex, t2);
    }

    @Test
    public void testNegativeInfinity() {
        final String negInfText = "-0Wm";
        final String negInfHex = "0xf301000080";
        final QMonth t1 = parseExpr(negInfText);
        checkEquals(negInfText, negInfHex, t1);
        final String lowerNegInfText = "-0wm";
        final QMonth t2 = parseExpr(lowerNegInfText);
        checkEquals(negInfText, negInfHex, t2);
    }

    @Test
    public void testMinimum() {
        final String text = "1709.01m";
        final String hex = "0xf35cf2ffff";
        final QMonth t4 = parseExpr(text);
        checkEquals(text, hex, t4);
    }

    @Test
    public void testMaximum() {
        final String text = "2290.12m";
        final String hex = "0xf3a30d0000";
        final QMonth t1 = parseExpr(text);
        checkEquals(text, hex, t1);
    }

    @Test
    public void testNegative() {
        final QMonth t1 = parseExpr("-2000.02m");
        checkEquals("1999.12m", "0xf3ffffffff", t1);
        final QMonth t2 = parseExpr("-2001.01m");
        checkEquals("1999.01m", "0xf3f4ffffff", t2);
        final QMonth t3 = parseExpr("-2015.07m");
        checkEquals("1984.07m", "0xf346ffffff", t3);
        final QMonth t4 = parseExpr("-2015.05m");
        checkEquals("1984.09m", "0xf348ffffff", t4);
    }

}
