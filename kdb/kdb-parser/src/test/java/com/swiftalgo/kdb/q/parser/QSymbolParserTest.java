package com.swiftalgo.kdb.q.parser;

/*-
 * #%L
 * KDB+ Parser
 * %%
 * Copyright (C) 2017 SwiftAlgo Limited
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.swiftalgo.kdb.q.QSymbol;
import org.junit.Test;

public class QSymbolParserTest extends QTypeParserTest<QSymbol> {

    @Test
    public void testSymbolA() {
        final QSymbol t1 = parseExpr("`a");
        checkEquals("`a", "0xf56100", t1);
        final QSymbol t2 = parseExpr("`");
        checkEquals("`", "0xf500", t2);
        final QSymbol t3 = parseExpr("( \t ( \t `a \t ) \t )");
        checkEquals("`a", "0xf56100", t3);
        final QSymbol t4 = parseExpr("( \t ( \t ` \t ) \t )");
        checkEquals("`", "0xf500", t4);
    }

    @Test
    public void testCastToSymbol() {
        final QSymbol t1 = parseExpr("`$\"a\"");
        checkEquals("`a", "0xf56100", t1);
        final QSymbol t2 = parseExpr("`$\"€\"");
        checkEquals("`€", "0xf5e282ac00", t2);
        final QSymbol t3 = parseExpr("`$\"IBM\"");
        checkEquals("`IBM", "0xf549424d00", t3);
        final QSymbol t4 = parseExpr("`$\"The teddy bear cost €350!\"");
        checkEquals("`The teddy bear cost €350!", "0xf5546865207465646479206265617220636f737420e282ac3335302100", t4);
    }

}
