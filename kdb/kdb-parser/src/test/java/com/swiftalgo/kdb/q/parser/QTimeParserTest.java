package com.swiftalgo.kdb.q.parser;

/*-
 * #%L
 * KDB+ Parser
 * %%
 * Copyright (C) 2017 SwiftAlgo Limited
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.swiftalgo.kdb.q.QTime;
import org.junit.Test;

public class QTimeParserTest extends QTypeParserTest<QTime> {

    @Test
    public void testNull() {
        final String lowerNullText = "0nt";
        final String upperNullText = "0Nt";
        final String nullHex = "0xed00000080";
        final QTime t1 = parseExpr(upperNullText);
        checkEquals(upperNullText, nullHex, t1);
        final QTime t2 = parseExpr(lowerNullText);
        checkEquals(upperNullText, nullHex, t2);
    }

    @Test
    public void testPositiveInfinity() {
        final String lowerText = "0wt";
        final String upperText = "0Wt";
        final String hex = "0xedffffff7f";
        final QTime t1 = parseExpr(upperText);
        checkEquals(upperText, hex, t1);
        final QTime t2 = parseExpr(lowerText);
        checkEquals(upperText, hex, t2);
    }

    @Test
    public void testNegativeInfinity() {
        final String lowerText = "-0wt";
        final String upperText = "-0Wt";
        final String hex = "0xed01000080";
        final QTime t1 = parseExpr(upperText);
        checkEquals(upperText, hex, t1);
        final QTime t2 = parseExpr(lowerText);
        checkEquals(upperText, hex, t2);
    }

    @Test
    public void testNegative() {
        final String text1 = "-01:23:45.678";
        final String hex1 = "0xed7250b3ff";
        final QTime t1 = parseExpr(text1);
        checkEquals(text1, hex1, t1);
        final String text2 = "-00:45:15.123";
        final String hex2 = "0xed0d92d6ff";
        final QTime t2 = parseExpr(text2);
        checkEquals(text2, hex2, t2);
    }


    @Test
    public void testZero() {
        final String text = "00:00:00.000";
        final String hex = "0xed00000000";
        final QTime t1 = parseExpr(text);
        checkEquals(text, hex, t1);
        final QTime t2 = parseExpr(text + 't');
        checkEquals(text, hex, t2);
        final QTime t3 = parseExpr("00:00:00.000000000t");
        checkEquals(text, hex, t3);
        final QTime t4 = parseExpr("00:00:00t");
        checkEquals(text, hex, t4);
        final QTime t5 = parseExpr("00:00:00.t");
        checkEquals(text, hex, t5);
        final QTime t6 = parseExpr("00:00:00.0t");
        checkEquals(text, hex, t6);
        final QTime t7 = parseExpr("00:00:00.0");
        checkEquals(text, hex, t7);
        final QTime t8 = parseExpr("00:00:00.00");
        checkEquals(text, hex, t8);
        final QTime t9 = parseExpr("00:00:00.0000");
        checkEquals(text, hex, t9);
    }

    @Test
    public void testNegativeZero() {
        final String text = "00:00:00.000";
        final String hex = "0xed00000000";
        final QTime t1 = parseExpr('-' + text);
        checkEquals(text, hex, t1);
        final QTime t2 = parseExpr('-' + text + 't');
        checkEquals(text, hex, t2);
        final QTime t3 = parseExpr("-00:00:00.000000000t");
        checkEquals(text, hex, t3);
        final QTime t4 = parseExpr("-00:00:00t");
        checkEquals(text, hex, t4);
        final QTime t5 = parseExpr("-00:00:00.t");
        checkEquals(text, hex, t5);
        final QTime t6 = parseExpr("-00:00:00.0t");
        checkEquals(text, hex, t6);
        final QTime t7 = parseExpr("-00:00:00.0");
        checkEquals(text, hex, t7);
        final QTime t8 = parseExpr("-00:00:00.00");
        checkEquals(text, hex, t8);
        final QTime t9 = parseExpr("-00:00:00.0000");
        checkEquals(text, hex, t9);
    }


    @Test
    public void testMaximum() {
        final String text = "23:59:59.999";
        final String hex = "0xedff5b2605";
        final QTime t1 = parseExpr(text);
        checkEquals(text, hex, t1);
        final QTime t2 = parseExpr(text + 't');
        checkEquals(text, hex, t2);
        final QTime t3 = parseExpr("23:59:59.999999999t");
        checkEquals(text, hex, t3);
    }

    @Test
    public void testMinimum() {
        final String text = "-23:59:59.999";
        final String hex = "0xed01a4d9fa";
        final QTime t1 = parseExpr(text);
        checkEquals(text, hex, t1);
        final QTime t2 = parseExpr(text + 't');
        checkEquals(text, hex, t2);
        final QTime t3 = parseExpr("-23:59:59.999999999t");
        checkEquals(text, hex, t3);
    }

}
