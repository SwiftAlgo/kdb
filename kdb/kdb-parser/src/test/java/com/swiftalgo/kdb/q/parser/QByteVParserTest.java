package com.swiftalgo.kdb.q.parser;

/*-
 * #%L
 * KDB+ Parser
 * %%
 * Copyright (C) 2017 SwiftAlgo Limited
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.swiftalgo.kdb.q.QByteV;
import org.junit.Test;

public class QByteVParserTest extends QTypeParserTest<QByteV> {

    @Test
    public void testCompactByteV() {
        final QByteV t1 = parseExpr("0x0a0b0c");
        checkEquals("0x0a0b0c", "0x0400030000000a0b0c", t1);
        final QByteV t2 = parseExpr("0xfafbfc");
        checkEquals("0xfafbfc", "0x040003000000fafbfc", t2);
        final QByteV t3 = parseExpr("0x1a2b3c");
        checkEquals("0x1a2b3c", "0x0400030000001a2b3c", t3);
        final QByteV t4 = parseExpr("0x123456");
        checkEquals("0x123456", "0x040003000000123456", t4);
    }

    @Test
    public void testListByteV() {
        final QByteV t1 = parseExpr("(0xfa;0x00;0x1f)");
        checkEquals("0xfa001f", "0x040003000000fa001f", t1);
        final QByteV t2 = parseExpr("( \t 0xfa \t ; \t 0x00 \t; \t 0x1f \t )");
        checkEquals("0xfa001f", "0x040003000000fa001f", t2);
        final QByteV t3 = parseExpr("( \t (0xfa) \t ;( \t 0x00 \t); \t ((0x1f)) \t )");
        checkEquals("0xfa001f", "0x040003000000fa001f", t3);

    }


    @Test
    public void testEnclosedByteV() {
        final QByteV t1 = parseExpr("( 0x0a0b0c )");
        checkEquals("0x0a0b0c", "0x0400030000000a0b0c", t1);
        final QByteV t2 = parseExpr("( ( 0x0a0b0c ) )");
        checkEquals("0x0a0b0c", "0x0400030000000a0b0c", t2);

        final QByteV t3 = parseExpr("`s# ( 0x0a0b0c )");
        checkEquals("`s#0x0a0b0c", "0x0401030000000a0b0c", t3);
        final QByteV t4 = parseExpr("`g#( `p#(`s#0x0a0b0c ) )");
        checkEquals("`g#0x0a0b0c", "0x0404030000000a0b0c", t4);
    }

    @Test
    public void testSingletonByteV() {
        final QByteV t1 = parseExpr("enlist 0xfa");
        checkEquals(",0xfa", "0x040001000000fa", t1);
        final QByteV t2 = parseExpr("enlist (0xfa)");
        checkEquals(",0xfa", "0x040001000000fa", t2);
        final QByteV t3 = parseExpr("`s#enlist 0xfa");
        checkEquals("`s#,0xfa", "0x040101000000fa", t3);
    }

}
