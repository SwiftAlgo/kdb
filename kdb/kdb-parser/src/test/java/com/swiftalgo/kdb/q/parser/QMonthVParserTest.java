package com.swiftalgo.kdb.q.parser;

/*-
 * #%L
 * KDB+ Parser
 * %%
 * Copyright (C) 2017 SwiftAlgo Limited
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.swiftalgo.kdb.q.QMonthV;
import org.junit.Test;

public class QMonthVParserTest extends QTypeParserTest<QMonthV> {

    @Test
    public void testCompactMonthV() {
        final String text1 = "-0W 1709.01 0N 2290.12 0Wm";
        final String hex1 = "0x0d0005000000010000805cf2ffff00000080a30d0000ffffff7f";

        final QMonthV t1 = parseExpr(text1);
        checkEquals(text1, hex1, t1);

        final String text2 = "1970.01 2000.01 2015.05m";
        final String hex2 = "0x0d000300000098feffff00000000b8000000";
        final QMonthV t2b = parseExpr(text2);
        checkEquals(text2, hex2, t2b);
    }

    @Test
    public void testListMonthV() {
        final String inText1 = "( -0wm; \t 1709.01m; 0Nm;2290.12m \t ;0Wm)";
        final String outText1 = "-0W 1709.01 0N 2290.12 0Wm";
        final String hex = "0x0d0005000000010000805cf2ffff00000080a30d0000ffffff7f";
        final QMonthV t1 = parseExpr(inText1);
        checkEquals(outText1, hex, t1);

        final String outText2 = "1970.01 2000.01 2015.05m";
        final String inText2a = "(1970.01m;2000.01m;2015.05m)";
        final String hex2 = "0x0d000300000098feffff00000000b8000000";
        final QMonthV t2a = parseExpr(inText2a);
        checkEquals(outText2, hex2, t2a);
    }

    @Test
    public void testSingletonMonthV() {
        final String x = "2015.05m";
        final String hex1 = "0x0d0001000000b8000000";
        final QMonthV t1 = parseExpr("enlist " + x);
        checkEquals("," + x, hex1, t1);
        final QMonthV t2 = parseExpr(String.format("enlist (%s)", x));
        checkEquals("," + x, hex1, t2);
        final QMonthV t3 = parseExpr("`s#enlist " + x);
        checkEquals("`s#," + x, "0x0d0101000000b8000000", t3);
    }

    @Test
    public void testEnclosedMonthV() {
        final String text1 = "1970.01 2000.01 2015.05m";
        final QMonthV t1 = parseExpr(String.format("( \t %s \t )", text1));
        final String hex = "0x0d000300000098feffff00000000b8000000";
        checkEquals(text1, hex, t1);
        final QMonthV t2 = parseExpr(String.format("( \t ( \t %s \t ) \t )", text1));
        checkEquals(text1, hex, t2);
    }

    @Test
    public void testAttrMonthV() {
        final String text1 = "`s#1970.01 2000.06 2015.12m";
        final String hex1 = "0x0d010300000098feffff05000000bf000000";
        final QMonthV t1 = parseExpr(text1);
        checkEquals(text1, hex1, t1);
    }

    @Test
    public void testNegativeMonthV() {
        final String inText1 = "-1970.01 -2000.01 2000.01 -2015.05m";
        final String outText1 = "2030.01 2000.01 2000.01 1984.09m";
        final QMonthV t1 = parseExpr(inText1);
        final String hex = "0x0d000400000068010000000000000000000048ffffff";
        checkEquals(outText1, hex, t1);
    }

}
