package com.swiftalgo.kdb.q.parser;

/*-
 * #%L
 * KDB+ Parser
 * %%
 * Copyright (C) 2017 SwiftAlgo Limited
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.swiftalgo.kdb.q.QMixedV;
import org.junit.Test;

public class QMixedVParserTest extends QTypeParserTest<QMixedV> {

    @Test
    public void testListMixedV() {
        final QMixedV t1 = parseExpr("(`IBM`MSFT;1 2h)");
        checkEquals("(`IBM`MSFT;1 2h)", "0x0000020000000b000200000049424d004d5346540005000200000001000200", t1);
    }

}
