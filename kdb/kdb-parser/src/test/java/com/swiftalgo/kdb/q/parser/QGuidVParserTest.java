package com.swiftalgo.kdb.q.parser;

/*-
 * #%L
 * KDB+ Parser
 * %%
 * Copyright (C) 2017 SwiftAlgo Limited
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.swiftalgo.kdb.q.QAttr;
import com.swiftalgo.kdb.q.QGuid;
import com.swiftalgo.kdb.q.QGuidV;
import com.swiftalgo.util.DataUtil;
import org.junit.Test;

public class QGuidVParserTest extends QTypeParserTest<QGuidV> {

    @Test
    public void testGuidV() {
        final byte[] q1 = QGuid.fromString("344298f0-bcc5-88f5-be5f-bbd2af9a685d");
        final byte[] q2 = QGuid.fromString("78f5c6fa-240c-e1dc-e32d-53e75e8872e1");
        final byte[] q3 = QGuid.fromString("977b6c8c-4224-a5ac-1c94-9ea27e4d3df7");
        final QGuidV t1 = new QGuidV(QAttr.NONE, DataUtil.concat(q3, q2, q1));
        checkEquals("977b6c8c-4224-a5ac-1c94-9ea27e4d3df7 78f5c6fa-240c-e1dc-e32d-53e75e8872e1 344298f0-bcc5-88f5-be5f-bbd2af9a685d", "0x020003000000977b6c8c4224a5ac1c949ea27e4d3df778f5c6fa240ce1dce32d53e75e8872e1344298f0bcc588f5be5fbbd2af9a685d", t1);

        final QGuidV t2 = new QGuidV(QAttr.SORTED, DataUtil.concat(q1, q2, q3));
        checkEquals("`s#344298f0-bcc5-88f5-be5f-bbd2af9a685d 78f5c6fa-240c-e1dc-e32d-53e75e8872e1 977b6c8c-4224-a5ac-1c94-9ea27e4d3df7", "0x020103000000344298f0bcc588f5be5fbbd2af9a685d78f5c6fa240ce1dce32d53e75e8872e1977b6c8c4224a5ac1c949ea27e4d3df7", t2);

    }

}
