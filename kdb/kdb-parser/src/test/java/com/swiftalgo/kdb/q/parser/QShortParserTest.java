package com.swiftalgo.kdb.q.parser;

/*-
 * #%L
 * KDB+ Parser
 * %%
 * Copyright (C) 2017 SwiftAlgo Limited
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.swiftalgo.kdb.q.QShort;
import org.junit.Test;

public class QShortParserTest extends QTypeParserTest<QShort> {

    @Test
    public void testShortA() {

        final QShort t1 = parseExpr("0h");
        checkEquals("0h", "0xfb0000", t1);

        final QShort t2 = parseExpr("1h");
        checkEquals("1h", "0xfb0100", t2);

        final QShort t3 = parseExpr("-1h");
        checkEquals("-1h", "0xfbffff", t3);

        //KDB+ massages out-of-range non-null values between [Integer.MIN_VALUE+1,Integer.MAX_VALUE] to lie in [Short.MIN_VALUE,Short.MAX_VALUE].
        final QShort t4 = parseExpr(Short.MIN_VALUE + "h");
        checkEquals(Short.toString((short) (Short.MIN_VALUE + 1)) + "h", "0xfb0180", t4);

        final QShort t5 = parseExpr(Short.MAX_VALUE + "h");
        checkEquals(Short.toString(Short.MAX_VALUE) + "h", "0xfbff7f", t5);

        final QShort t6 = parseExpr((Integer.MIN_VALUE + 1) + "h");
        checkEquals(Short.toString((short) (Short.MIN_VALUE + 1)) + "h", "0xfb0180", t6);

        final QShort t7 = parseExpr(Integer.MAX_VALUE + "h");
        checkEquals(Short.toString(Short.MAX_VALUE) + "h", "0xfbff7f", t7);

        //null
        final QShort t8 = parseExpr("0Nh");
        checkEquals("0Nh", "0xfb0080", t8);

        //+/- infinity
        final QShort t9 = parseExpr("-0Wh");
        checkEquals("-32767h", "0xfb0180", t9);

        final QShort t10 = parseExpr("-0wh");
        checkEquals("-32767h", "0xfb0180", t10);

        final QShort t11 = parseExpr("0Wh");
        checkEquals("32767h", "0xfbff7f", t11);

        final QShort t12 = parseExpr("0wh");
        checkEquals("32767h", "0xfbff7f", t12);
    }

    @Test(expected = NumberFormatException.class)
    public void testShortALowerBoundException() {
        parseExpr(Integer.MIN_VALUE + "h");
    }

    @Test(expected = NumberFormatException.class)
    public void testShortAUpperBoundException() {
        parseExpr(((long) Integer.MAX_VALUE + 1) + "h");
    }

}
