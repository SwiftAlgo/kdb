package com.swiftalgo.kdb.q.parser;

/*-
 * #%L
 * KDB+ Parser
 * %%
 * Copyright (C) 2017 SwiftAlgo Limited
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.swiftalgo.kdb.q.QFloatV;
import org.junit.Test;

public class QFloatVParserTest extends QTypeParserTest<QFloatV> {

    @Test
    public void testCompactFloatV() {
        final QFloatV t1 = parseExpr("-31415926535.89793e-10 -3.141592653589793 -1\t-1e0 -0. -0.e0 0. 0.e0 0.0 0.0e0 .0 .0e0 .1 .1e0 2 2e0 2.1 2.1e0 3.141592653589793 31415926535.89793e-10f");
        checkEquals("-3.141592653589793 -3.141592653589793 -1 -1 0 0 0 0 0 0 0 0 0.1 0.1 2 2 2.1 2.1 3.141592653589793 3.141592653589793", "0x090014000000182d4454fb2109c0182d4454fb2109c0000000000000f0bf000000000000f0bf000000000000008000000000000000800000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000009a9999999999b93f9a9999999999b93f00000000000000400000000000000040cdcccccccccc0040cdcccccccccc0040182d4454fb210940182d4454fb210940", t1);
        final QFloatV t2 = parseExpr("-0w -0W 0n 0N 0W 0w");
        checkEquals("-0w -0w 0n 0n 0w 0w", "0x090006000000000000000000f0ff000000000000f0ff000000000000f8ff000000000000f8ff000000000000f07f000000000000f07f", t2);
        final QFloatV t3 = parseExpr("-0w -0W 0n 0N 0w 0Wf");
        checkEquals("-0w -0w 0n 0n 0w 0w", "0x090006000000000000000000f0ff000000000000f0ff000000000000f8ff000000000000f8ff000000000000f07f000000000000f07f", t3);
    }

    @Test
    public void testListFloatV() {
        final QFloatV t1 = parseExpr("(-31415926535.89793e-10f; -3.141592653589793f \t ;\t -1f  ;\t\t-0.f\t\t;0.f;0.0f;.0f;.1f;2f;2.1f;3.141592653589793f;31415926535.89793e-10f)");
        checkEquals("-3.141592653589793 -3.141592653589793 -1 0 0 0 0 0.1 2 2.1 3.141592653589793 3.141592653589793", "0x09000c000000182d4454fb2109c0182d4454fb2109c0000000000000f0bf00000000000000800000000000000000000000000000000000000000000000009a9999999999b93f0000000000000040cdcccccccccc0040182d4454fb210940182d4454fb210940", t1);
        final QFloatV t2 = parseExpr("((-31415926535.89793e-10f); (-3.141592653589793f);(-1f);(-0.f);(0.f);(0.0f);(.0f);(.1f);(2f);(2.1f);(3.141592653589793f);(31415926535.89793e-10f))");
        checkEquals("-3.141592653589793 -3.141592653589793 -1 0 0 0 0 0.1 2 2.1 3.141592653589793 3.141592653589793", "0x09000c000000182d4454fb2109c0182d4454fb2109c0000000000000f0bf00000000000000800000000000000000000000000000000000000000000000009a9999999999b93f0000000000000040cdcccccccccc0040182d4454fb210940182d4454fb210940", t2);
        /* TODO - interpret 0N, 0W in mixed list as float, not long.
        final QFloatV t3 = parseExpr("(-0w; 0n; 0N; 0W; 0w)");
        checkEquals("-0w -0w 0n 0n 0w 0w", "0x", t3);
        */
        final QFloatV t3 = parseExpr("(-0w; -0w; 0n; 0n; 0w; 0w)");
        checkEquals("-0w -0w 0n 0n 0w 0w", "0x090006000000000000000000f0ff000000000000f0ff000000000000f8ff000000000000f8ff000000000000f07f000000000000f07f", t3);
        final QFloatV t4 = parseExpr("(-0wf; -0Wf; 0nf; 0Nf; 0wf; 0Wf)");
        checkEquals("-0w -0w 0n 0n 0w 0w", "0x090006000000000000000000f0ff000000000000f0ff000000000000f8ff000000000000f8ff000000000000f07f000000000000f07f", t4);
    }


}
