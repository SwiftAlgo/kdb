package com.swiftalgo.kdb.q.parser;

/*-
 * #%L
 * KDB+ Parser
 * %%
 * Copyright (C) 2017 SwiftAlgo Limited
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.swiftalgo.kdb.q.QTimeV;
import org.junit.Test;

public class QTimeVParserTest extends QTypeParserTest<QTimeV> {

    @Test
    public void testCompactTimeV() {
        final String inText1 = "-0W -23:59:59.999 -23:59:59 -23:59 -00:00:00 00:00:00 0N 23:59 23:59:59 23:59:59.999 0Wt";
        final String outText1 = "-0W -23:59:59.999 -23:59:59.000 -23:59:00.000 00:00:00.000 00:00:00.000 0N 23:59:00.000 23:59:59.000 23:59:59.999 0Wt"; //TODO No trailing 't' - determined by context.  Cf 0W 0Wt => 0W 0Wt
        final String hex1 = "0x13000b0000000100008001a4d9fae8a7d9fa608edafa000000000000000000000080a071250518582605ff5b2605ffffff7f";

        final QTimeV t1 = parseExpr(inText1);
        checkEquals(outText1, hex1, t1);

        final String inText2 = "-01:23:45.6789012345 -00:45:09.123 00:00:00.000 22:34:01.999";
        final String outText2 = "-01:23:45.678 -00:45:09.123 00:00:00.000 22:34:01.999";
        final String hex2 = "0x1300040000007250b3ff7da9d6ff000000008fa7d704";
        final QTimeV t2 = parseExpr(inText2);
        checkEquals(outText2, hex2, t2);
    }

    @Test
    public void testListTimeV() {

        final String inText1 = "( -0wt; \t -23:59:59.999 ; \t -23:59:59t \t ; \t -23:59t ; \t -00:00:00.000 \t ; \t 00:00:00t \t ; \t 0Nt \t ; \t 23:59t; \t 23:59:59t \t ; \t 23:59:59.999 \t ;0Wt)";
        final String outText1 = "-0W -23:59:59.999 -23:59:59.000 -23:59:00.000 00:00:00.000 00:00:00.000 0N 23:59:00.000 23:59:59.000 23:59:59.999 0Wt"; //TODO No trailing 't' - determined by context.  Cf 0W 0Wt => 0W 0Wt
        final QTimeV t1 = parseExpr(inText1);
        final String hex = "0x13000b0000000100008001a4d9fae8a7d9fa608edafa000000000000000000000080a071250518582605ff5b2605ffffff7f";
        checkEquals(outText1, hex, t1);

        final String inText2 = "( \t -01:23:45.6789012345t \t ; \t -00:45:09.123 \t ; 00:00:00.000 \t ; \t 22:34:01.999 \t )";
        final String outText2 = "-01:23:45.678 -00:45:09.123 00:00:00.000 22:34:01.999";
        final String hex2 = "0x1300040000007250b3ff7da9d6ff000000008fa7d704";
        final QTimeV t2 = parseExpr(inText2);
        checkEquals(outText2, hex2, t2);
    }

    @Test
    public void testSingletonTimeV() {
        final String x = "22:34:56.789";
        final String hex1 = "0x130001000000957dd804";
        final QTimeV t1 = parseExpr("enlist " + x);
        checkEquals("," + x, hex1, t1);
        final QTimeV t2 = parseExpr(String.format("enlist (%s)", x));
        checkEquals("," + x, hex1, t2);
        final QTimeV t3 = parseExpr("`s# enlist " + x);
        checkEquals("`s#," + x, "0x130101000000957dd804", t3);
    }

    @Test
    public void testEnclosedTimeV() {
        final String inText = "01:23:45.6789012345 00:00:00 22:34:08.753t";
        final String outText = "01:23:45.678 00:00:00.000 22:34:08.753";
        final String hex = "0x1300030000008eaf4c0000000000f1c1d704";
        final QTimeV t1 = parseExpr(String.format("( \t %s \t )", inText));
        checkEquals(outText, hex, t1);
        final QTimeV t2 = parseExpr(String.format("( \t ( \t %s \t ) \t )", inText));
        checkEquals(outText, hex, t2);
    }

    @Test
    public void testAttrTimeV() {
        final String inText = "`s#00:00:00 12:13:14.123 23:59:59.999";
        final String outText = "`s#00:00:00.000 12:13:14.123 23:59:59.999";
        final String hex1 = "0x130103000000000000000b4c9f02ff5b2605";
        final QTimeV t1 = parseExpr(inText);
        checkEquals(outText, hex1, t1);
    }

}
