package com.swiftalgo.kdb.q.parser;

/*-
 * #%L
 * KDB+ Parser
 * %%
 * Copyright (C) 2017 SwiftAlgo Limited
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.swiftalgo.kdb.q.QDatetime;
import org.junit.Test;

public class QDatetimeParserTest extends QTypeParserTest<QDatetime> {

    @Test
    public void testNearJavaEpoch() {
        final String text = "1970.01.01T11:11:11.111";
        final String hex = "0xf1f4eace564466c5c0";
        final QDatetime t1 = parseExpr(text);
        checkEquals(text, hex, t1);
        final QDatetime t2 = parseExpr("1970.01.01T11:11:11.111z");
        checkEquals(text, hex, t2);
    }


    @Test
    public void testJavaEpoch() {
        final String text = "1970.01.01T00:00:00.000";
        final String hex = "0xf1000000008066c5c0";
        final QDatetime t1 = parseExpr(text);
        checkEquals(text, hex, t1);
        final QDatetime t2 = parseExpr("1970.01.01T00:00:00.000z");
        checkEquals(text, hex, t2);
    }

    @Test
    public void testNearQEpoch() {
        final String text = "2000.01.01T12:34:56.789";
        final String hex = "0xf1737c5181cec6e03f";
        final QDatetime t1 = parseExpr(text);
        checkEquals(text, hex, t1);
        final QDatetime t2 = parseExpr("2000.01.01T12:34:56.789z");
        checkEquals(text, hex, t2);
    }


    @Test
    public void testQEpoch() {
        final String text = "2000.01.01T00:00:00.000";
        final String hex = "0xf10000000000000000";
        final QDatetime t1 = parseExpr(text);
        checkEquals(text, hex, t1);
        final QDatetime t2 = parseExpr("2000.01.01T00:00:00.000z");
        checkEquals(text, hex, t2);
    }

    @Test
    public void testNull() {
        final String lowerNullText = "0nz";
        final String nullText = "0Nz";
        final String nullHex = "0xf1000000000000f8ff";
        final QDatetime t1 = parseExpr(nullText);
        checkEquals(nullText, nullHex, t1);
        final QDatetime t2 = parseExpr(lowerNullText);
        checkEquals(nullText, nullHex, t2);
    }

    @Test
    public void testPositiveInfinity() {
        final String lowerText = "0wz";
        final String upperText = "0Wz";
        final String hex = "0xf1000000000000f07f";
        final QDatetime t1 = parseExpr(upperText);
        checkEquals(lowerText, hex, t1);
        final QDatetime t2 = parseExpr(lowerText);
        checkEquals(lowerText, hex, t2);
    }

    @Test
    public void testNegativeInfinity() {
        final String lowerText = "-0wz";
        final String upperText = "-0Wz";
        final String hex = "0xf1000000000000f0ff";
        final QDatetime t1 = parseExpr(upperText);
        checkEquals(lowerText, hex, t1);
        final QDatetime t2 = parseExpr(lowerText);
        checkEquals(lowerText, hex, t2);
    }

    @Test
    public void testNearMinimum() {
        final String text = "1709.01.01T23:59:59.999";
        final String hex = "0xf11b030000c0f2f9c0";
        final QDatetime t1 = parseExpr(text);
        checkEquals(text, hex, t1);
        final QDatetime t2 = parseExpr(text + 'z');
        checkEquals(text, hex, t2);
    }

    @Test
    public void testMinimum() {
        final String text = "1709.01.01T00:00:00.000";
        final String hex = "0xf100000000d0f2f9c0";
        final QDatetime t1 = parseExpr(text);
        checkEquals(text, hex, t1);
        final QDatetime t2 = parseExpr("1709.01.01T00:00:00.000z");
        checkEquals(text, hex, t2);
    }

    @Test
    public void testMaximum() {
        final String text = "2290.12.31T23:59:59.999";
        final String hex = "0xf1e5fcffffdff2f940";
        final QDatetime t1 = parseExpr(text);
        checkEquals(text, hex, t1);
        final QDatetime t2 = parseExpr("2290.12.31T23:59:59.999z");
        checkEquals(text, hex, t2);
    }

}
