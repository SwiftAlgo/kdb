package com.swiftalgo.kdb.q.parser;

/*-
 * #%L
 * KDB+ Parser
 * %%
 * Copyright (C) 2017 SwiftAlgo Limited
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.swiftalgo.kdb.q.QReal;
import org.junit.Test;

public class QRealParserTest extends QTypeParserTest<QReal> {

    @Test
    public void testReal() {
        final QReal t1 = parseExpr("-3.14159265e");
        checkEquals("-3.1415927e", "0xf8db0f49c0", t1);
        final QReal t2 = parseExpr("-1e");
        checkEquals("-1e", "0xf8000080bf", t2);
        final QReal t3 = parseExpr("-0.e");
        checkEquals("0e", "0xf800000080", t3);
        final QReal t4 = parseExpr("0.e");
        checkEquals("0e", "0xf800000000", t4);
        final QReal t5 = parseExpr("0.0e");
        checkEquals("0e", "0xf800000000", t5);
        final QReal t6 = parseExpr(".0e");
        checkEquals("0e", "0xf800000000", t6);
        final QReal t7 = parseExpr("2e");
        checkEquals("2e", "0xf800000040", t7);
        final QReal t8 = parseExpr("2.1e");
        checkEquals("2.1e", "0xf866660640", t8);
        final QReal t9 = parseExpr("3.14159265e");
        checkEquals("3.1415927e", "0xf8db0f4940", t9);
    }

    @Test
    public void testRealSpecialValues() {
        final QReal t1 = parseExpr("-0we");
        checkEquals("-0we", "0xf8000080ff", t1);
        final QReal t2 = parseExpr("-0We");
        checkEquals("-0we", "0xf8000080ff", t2);

        final QReal t3 = parseExpr("0ne");
        checkEquals("0ne", "0xf80000c0ff", t3);
        final QReal t4 = parseExpr("0Ne");
        checkEquals("0ne", "0xf80000c0ff", t4);

        final QReal t5 = parseExpr("0we");
        checkEquals("0we", "0xf80000807f", t5);
        final QReal t6 = parseExpr("0We");
        checkEquals("0we", "0xf80000807f", t6);
    }

    @Test
    public void testRealExponentialIn() {
        final QReal t1 = parseExpr("-31415926.5e-7e");
        checkEquals("-3.1415927e", "0xf8db0f49c0", t1);
        final QReal t2 = parseExpr("31415926.5e-7e");
        checkEquals("3.1415927e", "0xf8db0f4940", t2);
        final QReal t3 = parseExpr("-0.0314159265e2e");
        checkEquals("-3.1415927e", "0xf8db0f49c0", t3);
        final QReal t4 = parseExpr("0.0314159265e2e");
        checkEquals("3.1415927e", "0xf8db0f4940", t4);
        final QReal t5 = parseExpr("-0.0314159265e+2e");
        checkEquals("-3.1415927e", "0xf8db0f49c0", t5);
        final QReal t6 = parseExpr("0.0314159265e+2e");
        checkEquals("3.1415927e", "0xf8db0f4940", t6);
    }

    @Test
    public void testRealExponentialInOut() {
        final QReal t1 = parseExpr("-3.14159269e32e");
        checkEquals("-3.1415928e+32e", "0xf8f6d377f5", t1);
        final QReal t2 = parseExpr("-3.14159265e32e");
        checkEquals("-3.1415926e+32e", "0xf8f5d377f5", t2);
        final QReal t3 = parseExpr("-3.14159265e-32e");
        checkEquals("-3.1415927e-32e", "0xf8ee1e238b", t3);
        final QReal t4 = parseExpr("3.14159265e-32e");
        checkEquals("3.1415927e-32e", "0xf8ee1e230b", t4);
        final QReal t5 = parseExpr("3.14159265e32e");
        checkEquals("3.1415926e+32e", "0xf8f5d37775", t5);
        final QReal t6 = parseExpr("3.14159269e32e");
        checkEquals("3.1415928e+32e", "0xf8f6d37775", t6);
    }

    @Test
    public void testRealIntegerBoundaries() {
        final String minText = Integer.toString(Integer.MIN_VALUE) + 'e';
        final QReal t1 = parseExpr(minText);
        checkEquals(minText, "0xf8000000cf", t1);
        final String maxText = Integer.toString(Integer.MAX_VALUE) + 'e';
        final QReal t2 = parseExpr(maxText);
        checkEquals(maxText, "0xf80000004f", t2);
    }


}
