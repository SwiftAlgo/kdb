package com.swiftalgo.kdb.q.parser;

/*-
 * #%L
 * KDB+ Parser
 * %%
 * Copyright (C) 2017 SwiftAlgo Limited
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.swiftalgo.kdb.q.*;
import com.swiftalgo.kdb.q.util.Util;
import com.swiftalgo.util.DataUtil;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.RuleNode;
import org.antlr.v4.runtime.tree.TerminalNode;
import org.apache.commons.lang3.StringUtils;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class QTypeParser extends QBaseVisitor<QType> {

    @Override
    public QType visitExpr(@NotNull QParser.ExprContext ctx) {
        //System.out.println("visitExpr");
        final QType result = super.visitExpr(ctx);
        //System.out.println(String.format("visitExpr->%s", result));
        return result;
    }

    @Override
    public QList visitList(@NotNull QParser.ListContext ctx) {
        //System.out.println("visitList");
        final QList result = (QList) super.visitList(ctx);
        //System.out.println(String.format("visitList->%s", result));
        return result;
    }

    @Override
    public QBooleanV visitCompactBooleanV(@NotNull QParser.CompactBooleanVContext ctx) {
        final String valueText = ctx.BOOLEAN_V().getText();
        final boolean[] values = new boolean[valueText.length() - 1];
        for (int i = 0; i < values.length; ++i) {
            switch (valueText.charAt(i)) {
                case '0':
                    values[i] = false;
                    break;
                case '1':
                    values[i] = true;
                    break;
                default:
                    throw new IllegalStateException();
            }
        }
        final QBooleanV result = new QBooleanV(QAttr.NONE, values);
        return result;
    }

    @Override
    public QBooleanV visitListBooleanV(@NotNull QParser.ListBooleanVContext ctx) {
        final List<QParser.BooleanAContext> children = ctx.booleanA();
        final boolean[] values = new boolean[children.size()];
        for (int i = 0; i < values.length; ++i) {
            values[i] = ((QBoolean) visit(children.get(i))).getValue();
        }
        return new QBooleanV(QAttr.NONE, values);
    }

    @Override
    public QBooleanV visitSingletonBooleanV(@NotNull QParser.SingletonBooleanVContext ctx) {
        final QBoolean child = (QBoolean) visit(ctx.booleanA());
        return new QBooleanV(QAttr.NONE, new boolean[]{child.getValue()});
    }

    @Override
    public QBooleanV visitAttrBooleanV(@NotNull QParser.AttrBooleanVContext ctx) {
        final QAttr attr = QAttr.get(ctx.ATTR().getText());
        final QBooleanV child = (QBooleanV) visit(ctx.booleanV());
        return new QBooleanV(attr, child);
    }


    @Override
    public QGuidV visitCompactGuidV(@NotNull QParser.CompactGuidVContext ctx) {
        return new QGuidV(QAttr.NONE, new byte[ctx.anyNull().size() * 16]);
    }

    @Override
    public QGuidV visitListGuidV(@NotNull QParser.ListGuidVContext ctx) {
        final List<QParser.GuidAContext> children = ctx.guidA();
        byte[][] childBytes = new byte[children.size()][];
        for (int i = 0; i < children.size(); ++i) {
            childBytes[i] = ((QGuid) visit(children.get(i))).getValue();
        }
        return new QGuidV(QAttr.NONE, DataUtil.concat(childBytes));
    }

    @Override
    public QGuidV visitSingletonGuidV(@NotNull QParser.SingletonGuidVContext ctx) {
        final QGuid child = (QGuid) visit(ctx.guidA());
        return new QGuidV(QAttr.NONE, child.getValue());
    }

    @Override
    public QGuidV visitAttrGuidV(@NotNull QParser.AttrGuidVContext ctx) {
        final QAttr attr = QAttr.get(ctx.ATTR().getText());
        final QGuidV child = (QGuidV) visit(ctx.guidV());
        return attr == child.attr() ? child : new QGuidV(attr, child);
    }

    @Override
    public QByteV visitCompactByteV(@NotNull QParser.CompactByteVContext ctx) {
        final String valueText = ctx.BYTE_V().getText().substring(2);
        final byte[] bytes = DataUtil.hexStringToByteArray(valueText);
        //System.out.println(String.format("Parsed %s to %d bytes %s", valueText, bytes.length, Arrays.toString(bytes)));
        return new QByteV(QAttr.NONE, bytes);
    }

    @Override
    public QByteV visitListByteV(@NotNull QParser.ListByteVContext ctx) {
        final List<QParser.ByteAContext> children = ctx.byteA();
        final byte[] values = new byte[children.size()];
        for (int i = 0; i < values.length; ++i) {
            values[i] = ((QByte) visit(children.get(i))).getValue();
        }
        return new QByteV(QAttr.NONE, values);
    }

    @Override
    public QByteV visitSingletonByteV(@NotNull QParser.SingletonByteVContext ctx) {
        final QByte child = (QByte) visit(ctx.byteA());
        return new QByteV(QAttr.NONE, new byte[]{child.getValue()});
    }

    @Override
    public QByteV visitAttrByteV(@NotNull QParser.AttrByteVContext ctx) {
        final QAttr attr = QAttr.get(ctx.ATTR().getText());
        final QByteV child = (QByteV) visit(ctx.byteV());
        return attr == child.attr() ? child : new QByteV(attr, child);
    }

    @Override
    public QShortV visitCompactShortV(@NotNull QParser.CompactShortVContext ctx) {
        final List<QParser.IntBaseContext> intBases = ctx.intBase();
        final short[] values = new short[intBases.size()];
        for (int i = 0; i < intBases.size(); ++i) {
            final short value = parseShortFromIntBase(intBases.get(i));
            values[i] = value;
        }
        return new QShortV(QAttr.NONE, values);
    }

    @Override
    public QShortV visitListShortV(@NotNull QParser.ListShortVContext ctx) {
        final List<QParser.ShortAContext> children = ctx.shortA();
        final short[] values = new short[children.size()];
        for (int i = 0; i < values.length; ++i) {
            values[i] = ((QShort) visit(children.get(i))).getValue();
        }
        return new QShortV(QAttr.NONE, values);
    }

    @Override
    public QShortV visitSingletonShortV(@NotNull QParser.SingletonShortVContext ctx) {
        final QShort child = (QShort) visit(ctx.shortA());
        return new QShortV(QAttr.NONE, new short[]{child.getValue()});
    }

    @Override
    public QShortV visitAttrShortV(@NotNull QParser.AttrShortVContext ctx) {
        final QAttr attr = QAttr.get(ctx.ATTR().getText());
        final QShortV child = (QShortV) visit(ctx.shortV());
        return attr == child.attr() ? child : new QShortV(attr, child);
    }

    @Override
    public QIntV visitCompactIntV(@NotNull QParser.CompactIntVContext ctx) {
        final List<QParser.IntBaseContext> intBases = ctx.intBase();
        final int[] values = new int[intBases.size()];
        for (int i = 0; i < intBases.size(); ++i) {
            final int value = parseIntFromIntBase(intBases.get(i));
            values[i] = value;
        }
        return new QIntV(QAttr.NONE, values);
    }

    @Override
    public QIntV visitListIntV(@NotNull QParser.ListIntVContext ctx) {
        final List<QParser.IntAContext> children = ctx.intA();
        final int[] values = new int[children.size()];
        for (int i = 0; i < values.length; ++i) {
            values[i] = ((QInt) visit(children.get(i))).getValue();
        }
        return new QIntV(QAttr.NONE, values);
    }

    @Override
    public QIntV visitSingletonIntV(@NotNull QParser.SingletonIntVContext ctx) {
        final QInt child = (QInt) visit(ctx.intA());
        return new QIntV(QAttr.NONE, new int[]{child.getValue()});
    }

    @Override
    public QIntV visitAttrIntV(@NotNull QParser.AttrIntVContext ctx) {
        final QAttr attr = QAttr.get(ctx.ATTR().getText());
        final QIntV child = (QIntV) visit(ctx.intV());
        return attr == child.attr() ? child : new QIntV(attr, child);
    }

    @Override
    public QLongV visitCompactLongV(@NotNull QParser.CompactLongVContext ctx) {
        final List<QParser.IntBaseContext> intTokens = ctx.intBase();
        final long[] values = new long[intTokens.size() + 1];
        int i;
        for (i = 0; i < intTokens.size(); ++i) {
            values[i] = parseLongFromIntBase(intTokens.get(i));
        }
        values[i] = parseLongFromLongType(ctx.longType());
        return new QLongV(QAttr.NONE, values);
    }

    @Override
    public QLongV visitListLongV(@NotNull QParser.ListLongVContext ctx) {
        final List<QParser.LongAContext> children = ctx.longA();
        final long[] values = new long[children.size()];
        for (int i = 0; i < values.length; ++i) {
            values[i] = ((QLong) visit(children.get(i))).getValue();
        }
        return new QLongV(QAttr.NONE, values);
    }

    @Override
    public QLongV visitSingletonLongV(@NotNull QParser.SingletonLongVContext ctx) {
        final QLong child = (QLong) visit(ctx.longA());
        return new QLongV(QAttr.NONE, new long[]{child.getValue()});
    }

    @Override
    public QLongV visitAttrLongV(@NotNull QParser.AttrLongVContext ctx) {
        final QAttr attr = QAttr.get(ctx.ATTR().getText());
        final QLongV child = (QLongV) visit(ctx.longV());
        return attr == child.attr() ? child : new QLongV(attr, child);
    }

    @Override
    public QRealV visitCompactRealV(@NotNull QParser.CompactRealVContext ctx) {
        final List<QParser.FloatBaseContext> floatBases = ctx.floatBase();
        final float[] values = new float[floatBases.size()];
        for (int i = 0; i < floatBases.size(); ++i) {
            values[i] = parseFloatFromFloatBase(floatBases.get(i));
        }
        return new QRealV(QAttr.NONE, values);
    }

    @Override
    public QType visitListRealV(@NotNull QParser.ListRealVContext ctx) {
        final List<QParser.RealAContext> children = ctx.realA();
        final float[] values = new float[children.size()];
        for (int i = 0; i < children.size(); ++i) {
            values[i] = ((QReal) visit(children.get(i))).getValue();
        }
        return new QRealV(QAttr.NONE, values);
    }

    @Override
    public QRealV visitSingletonRealV(@NotNull QParser.SingletonRealVContext ctx) {
        final QReal child = (QReal) visit(ctx.realA());
        return new QRealV(QAttr.NONE, new float[]{child.getValue()});
    }

    @Override
    public QRealV visitAttrRealV(@NotNull QParser.AttrRealVContext ctx) {
        final QAttr attr = QAttr.get(ctx.ATTR().getText());
        final QRealV child = (QRealV) visit(ctx.realV());
        return attr == child.attr() ? child : new QRealV(attr, child);
    }

    @Override
    public QFloatV visitCompactFloatV(@NotNull QParser.CompactFloatVContext ctx) {
        final List<QParser.FloatBaseContext> floatBases = ctx.floatBase();
        final double[] values = new double[floatBases.size() + 1];
        int i;
        for (i = 0; i < floatBases.size(); ++i) {
            values[i] = parseDoubleFromFloatBase(floatBases.get(i));
        }
        values[i] = parseDoubleFromFloatType(ctx.floatType());
        return new QFloatV(QAttr.NONE, values);
    }

    @Override
    public QFloatV visitListFloatV(@NotNull QParser.ListFloatVContext ctx) {
        final List<QParser.FloatAContext> children = ctx.floatA();
        final double[] values = new double[children.size()];
        for (int i = 0; i < children.size(); ++i) {
            values[i] = ((QFloat) visit(children.get(i))).getValue();
        }
        return new QFloatV(QAttr.NONE, values);
    }

    @Override
    public QFloatV visitSingletonFloatV(@NotNull QParser.SingletonFloatVContext ctx) {
        final QFloat child = (QFloat) visit(ctx.floatA());
        return new QFloatV(QAttr.NONE, new double[]{child.getValue()});
    }

    @Override
    public QFloatV visitAttrFloatV(@NotNull QParser.AttrFloatVContext ctx) {
        final QAttr attr = QAttr.get(ctx.ATTR().getText());
        final QFloatV child = (QFloatV) visit(ctx.floatV());
        return attr == child.attr() ? child : new QFloatV(attr, child);
    }

    @Override
    public QCharV visitCompactCharV(@NotNull QParser.CompactCharVContext ctx) {
        final String text = ctx.CHAR_V().getText();
        return new QCharV(QAttr.NONE, text.substring(1, text.length() - 1));
    }

    @Override
    public QCharV visitListCharV(@NotNull QParser.ListCharVContext ctx) {
        final List<QParser.CharAContext> children = ctx.charA();
        final StringBuilder sb = new StringBuilder(children.size());
        for (int i = 0; i < children.size(); ++i) {
            sb.append(((QChar) visit(children.get(i))).getValue());
        }
        return new QCharV(QAttr.NONE, sb.toString());
    }

    @Override
    public QCharV visitSingletonCharV(@NotNull QParser.SingletonCharVContext ctx) {
        final QChar child = (QChar) visit(ctx.charA());
        return new QCharV(QAttr.NONE, new String(new char[]{child.getValue()}));
    }

    @Override
    public QCharV visitAttrCharV(@NotNull QParser.AttrCharVContext ctx) {
        final QAttr attr = QAttr.get(ctx.ATTR().getText());
        final QCharV child = (QCharV) visit(ctx.charV());
        return attr == child.attr() ? child : new QCharV(attr, child);
    }

    @Override
    public QSymbolV visitCompactSymbolV(@NotNull QParser.CompactSymbolVContext ctx) {
        final List<TerminalNode> sTokens = ctx.SYMBOL();
        final String[] values = new String[sTokens.size()];
        for (int i = 0; i < sTokens.size(); ++i) {
            values[i] = sTokens.get(i).getText().substring(1);
        }
        return new QSymbolV(QAttr.NONE, values);
    }

    @Override
    public QSymbolV visitListSymbolV(@NotNull QParser.ListSymbolVContext ctx) {
        final List<QParser.SymbolAContext> children = ctx.symbolA();
        final String[] values = new String[children.size()];
        for (int i = 0; i < children.size(); ++i) {
            values[i] = ((QSymbol) visit(children.get(i))).getValue();
        }
        return new QSymbolV(QAttr.NONE, values);
    }

    @Override
    public QSymbolV visitSingletonSymbolV(@NotNull QParser.SingletonSymbolVContext ctx) {
        final QSymbol child = (QSymbol) visit(ctx.symbolA());
        return new QSymbolV(QAttr.NONE, new String[]{child.getValue()});
    }

    @Override
    public QSymbolV visitAttrSymbolV(@NotNull QParser.AttrSymbolVContext ctx) {
        final QAttr attr = QAttr.get(ctx.ATTR().getText());
        final QSymbolV child = (QSymbolV) visit(ctx.symbolV());
        return attr == child.attr() ? child : new QSymbolV(attr, child);
    }

    @Override
    public QTimestampV visitCompactTimestampV(@NotNull QParser.CompactTimestampVContext ctx) {
        final List<QParser.TimestampBaseContext> bases = ctx.timestampBase();
        final long[] values = new long[bases.size() + 1];
        int i = 0;
        for (; i < bases.size(); ++i) {
            QParser.TimestampBaseContext base = bases.get(i);
            values[i] = parseTimestamp(base.DATE(), base.t, base.anyNull(), base.anyInf());
        }
        final QParser.TimestampTypeContext last = ctx.timestampType();
        values[i] = parseTimestamp(last.DATE(), last.t, last.anyNull(), last.anyInf());
        return new QTimestampV(QAttr.NONE, values);
    }

    @Override
    public QTimestampV visitListTimestampV(@NotNull QParser.ListTimestampVContext ctx) {
        final List<QParser.TimestampAContext> xs = ctx.timestampA();
        final long[] values = new long[xs.size()];
        for (int i = 0; i < xs.size(); ++i) {
            values[i] = ((QTimestamp) visit(xs.get(i))).getValue();
        }
        return new QTimestampV(QAttr.NONE, values);
    }

    @Override
    public QTimestampV visitSingletonTimestampV(@NotNull QParser.SingletonTimestampVContext ctx) {
        final long[] values = new long[]{((QTimestamp) visit(ctx.timestampA())).getValue()};
        return new QTimestampV(QAttr.NONE, values);
    }

    @Override
    public QTimestampV visitAttrTimestampV(@NotNull QParser.AttrTimestampVContext ctx) {
        final QAttr attr = QAttr.get(ctx.ATTR().getText());
        final QTimestampV child = (QTimestampV) visit(ctx.timestampV());
        return attr == child.attr() ? child : new QTimestampV(attr, child);
    }

    @Override
    public QMonthV visitCompactMonthV(@NotNull QParser.CompactMonthVContext ctx) {
        final List<QParser.MonthBaseContext> bases = ctx.monthBase();
        final int[] values = new int[bases.size() + 1];
        int i = 0;
        for (; i < bases.size(); ++i) {
            QParser.MonthBaseContext base = bases.get(i);
            values[i] = parseMonth(base.FLOAT(), base.anyNull(), base.anyInf());
        }
        final QParser.MonthBaseContext last = ctx.monthType().monthBase();
        values[i] = parseMonth(last.FLOAT(), last.anyNull(), last.anyInf());
        return new QMonthV(QAttr.NONE, values);
    }

    @Override
    public QMonthV visitListMonthV(@NotNull QParser.ListMonthVContext ctx) {
        final List<QParser.MonthAContext> types = ctx.monthA();
        final int[] values = new int[types.size()];
        for (int i = 0; i < types.size(); ++i) {
            values[i] = ((QMonth) visit(types.get(i))).getValue();
        }
        return new QMonthV(QAttr.NONE, values);
    }

    @Override
    public QMonthV visitSingletonMonthV(@NotNull QParser.SingletonMonthVContext ctx) {
        final int[] values = new int[]{((QMonth) visit(ctx.monthA())).getValue()};
        return new QMonthV(QAttr.NONE, values);
    }

    @Override
    public QMonthV visitAttrMonthV(@NotNull QParser.AttrMonthVContext ctx) {
        final QAttr attr = QAttr.get(ctx.ATTR().getText());
        final QMonthV child = (QMonthV) visit(ctx.monthV());
        return attr == child.attr() ? child : new QMonthV(attr, child);
    }

    @Override
    public QDateV visitCompactDateV(@NotNull QParser.CompactDateVContext ctx) {
        final List<QParser.DateBaseContext> x = ctx.dateBase();
        final int[] values = new int[x.size() + 1];
        int i = 0;
        for (; i < x.size(); ++i) {
            QParser.DateBaseContext base = x.get(i);
            values[i] = parseDate(base.DATE(), base.anyNull(), base.anyInf());
        }
        final QParser.DateTypeContext last = ctx.dateType();
        values[i] = parseDate(last.DATE(), last.anyNull(), last.anyInf());
        return new QDateV(QAttr.NONE, values);
    }

    @Override
    public QDateV visitListDateV(@NotNull QParser.ListDateVContext ctx) {
        final List<QParser.DateAContext> types = ctx.dateA();
        final int[] values = new int[types.size()];
        for (int i = 0; i < types.size(); ++i) {
            values[i] = ((QDate) visit(types.get(i))).getValue();
        }
        return new QDateV(QAttr.NONE, values);

    }

    @Override
    public QDateV visitSingletonDateV(@NotNull QParser.SingletonDateVContext ctx) {
        final int[] values = new int[]{((QDate) visit(ctx.dateA())).getValue()};
        return new QDateV(QAttr.NONE, values);
    }

    @Override
    public QDateV visitAttrDateV(@NotNull QParser.AttrDateVContext ctx) {
        final QAttr attr = QAttr.get(ctx.ATTR().getText());
        final QDateV child = (QDateV) visit(ctx.dateV());
        return attr == child.attr() ? child : new QDateV(attr, child);
    }

    @Override
    public QDatetimeV visitCompactDatetimeV(@NotNull QParser.CompactDatetimeVContext ctx) {
        final List<QParser.DatetimeBaseContext> x = ctx.datetimeBase();
        final double[] values = new double[x.size() + 1];
        int i = 0;
        for (; i < x.size(); ++i) {
            QParser.DatetimeBaseContext base = x.get(i);
            values[i] = parseDatetime(base.DATE(), base.t, base.anyNull(), base.anyInf());
        }
        final QParser.DatetimeTypeContext last = ctx.datetimeType();
        values[i] = parseDatetime(last.DATE(), last.t, last.anyNull(), last.anyInf());
        return new QDatetimeV(QAttr.NONE, values);
    }

    @Override
    public QDatetimeV visitListDatetimeV(@NotNull QParser.ListDatetimeVContext ctx) {
        final List<QParser.DatetimeAContext> types = ctx.datetimeA();
        final double[] values = new double[types.size()];
        for (int i = 0; i < types.size(); ++i) {
            values[i] = ((QDatetime) visit(types.get(i))).getValue();
        }
        return new QDatetimeV(QAttr.NONE, values);

    }

    @Override
    public QDatetimeV visitSingletonDatetimeV(@NotNull QParser.SingletonDatetimeVContext ctx) {
        final double[] values = new double[]{((QDatetime) visit(ctx.datetimeA())).getValue()};
        return new QDatetimeV(QAttr.NONE, values);
    }

    @Override
    public QDatetimeV visitAttrDatetimeV(@NotNull QParser.AttrDatetimeVContext ctx) {
        final QAttr attr = QAttr.get(ctx.ATTR().getText());
        final QDatetimeV child = (QDatetimeV) visit(ctx.datetimeV());
        return attr == child.attr() ? child : new QDatetimeV(attr, child);
    }

    @Override
    public QTimespanV visitCompactTimespanV(@NotNull QParser.CompactTimespanVContext ctx) {
        final List<QParser.TimespanBaseContext> bases = ctx.timespanBase();
        final long[] values = new long[bases.size() + 1];
        int i = 0;
        for (; i < bases.size(); ++i) {
            QParser.TimespanBaseContext base = bases.get(i);
            values[i] = parseTimespan(base.INTEGER(), base.t, base.anyNull(), base.anyInf());
        }
        final QParser.TimespanTypeContext last = ctx.timespanType();
        values[i] = parseTimespan(last.INTEGER(), last.t, last.anyNull(), last.anyInf());
        return new QTimespanV(QAttr.NONE, values);
    }

    @Override
    public QTimespanV visitListTimespanV(@NotNull QParser.ListTimespanVContext ctx) {
        final List<QParser.TimespanAContext> xs = ctx.timespanA();
        final long[] values = new long[xs.size()];
        for (int i = 0; i < xs.size(); ++i) {
            values[i] = ((QTimespan) visit(xs.get(i))).getValue();
        }
        return new QTimespanV(QAttr.NONE, values);
    }

    @Override
    public QTimespanV visitSingletonTimespanV(@NotNull QParser.SingletonTimespanVContext ctx) {
        final long[] values = new long[]{((QTimespan) visit(ctx.timespanA())).getValue()};
        return new QTimespanV(QAttr.NONE, values);
    }

    @Override
    public QTimespanV visitAttrTimespanV(@NotNull QParser.AttrTimespanVContext ctx) {
        final QAttr attr = QAttr.get(ctx.ATTR().getText());
        final QTimespanV child = (QTimespanV) visit(ctx.timespanV());
        return attr == child.attr() ? child : new QTimespanV(attr, child);
    }

    @Override
    public QMinuteV visitCompactMinuteV(@NotNull QParser.CompactMinuteVContext ctx) {
        final List<QParser.TimeBaseContext> x = ctx.timeBase();
        final int[] values = new int[x.size() + 1];
        int i = 0;
        for (; i < x.size(); ++i) {
            QParser.TimeBaseContext base = x.get(i);
            values[i] = parseMinute(base.t, base.anyNull(), base.anyInf());
        }
        final QParser.MinuteTypeContext last = ctx.minuteType();
        values[i] = parseMinute(last.t, last.anyNull(), last.anyInf());
        return new QMinuteV(QAttr.NONE, values);
    }

    @Override
    public QMinuteV visitListMinuteV(@NotNull QParser.ListMinuteVContext ctx) {
        final List<QParser.MinuteAContext> types = ctx.minuteA();
        final int[] values = new int[types.size()];
        int i = 0;
        for (; i < types.size(); ++i) {
            values[i] = ((QMinute) visit(types.get(i))).getValue();
        }
        return new QMinuteV(QAttr.NONE, values);
    }

    @Override
    public QMinuteV visitSingletonMinuteV(@NotNull QParser.SingletonMinuteVContext ctx) {
        final int[] values = new int[]{((QMinute) visit(ctx.minuteA())).getValue()};
        return new QMinuteV(QAttr.NONE, values);
    }

    @Override
    public QMinuteV visitAttrMinuteV(@NotNull QParser.AttrMinuteVContext ctx) {
        final QAttr attr = QAttr.get(ctx.ATTR().getText());
        final QMinuteV child = (QMinuteV) visit(ctx.minuteV());
        return attr == child.attr() ? child : new QMinuteV(attr, child);
    }

    @Override
    public QSecondV visitCompactSecondV(@NotNull QParser.CompactSecondVContext ctx) {
        final List<QParser.TimeBaseContext> x = ctx.timeBase();
        final int[] values = new int[x.size() + 1];
        int i = 0;
        for (; i < x.size(); ++i) {
            QParser.TimeBaseContext base = x.get(i);
            values[i] = parseSecond(base.t, base.anyNull(), base.anyInf());
        }
        final QParser.SecondTypeContext last = ctx.secondType();
        values[i] = parseSecond(last.t, last.anyNull(), last.anyInf());
        return new QSecondV(QAttr.NONE, values);
    }

    @Override
    public QSecondV visitListSecondV(@NotNull QParser.ListSecondVContext ctx) {
        final List<QParser.SecondAContext> types = ctx.secondA();
        final int[] values = new int[types.size()];
        for (int i = 0; i < types.size(); ++i) {
            values[i] = ((QSecond) visit(types.get(i))).getValue();
        }
        return new QSecondV(QAttr.NONE, values);
    }

    @Override
    public QSecondV visitSingletonSecondV(@NotNull QParser.SingletonSecondVContext ctx) {
        final int[] values = new int[]{((QSecond) visit(ctx.secondA())).getValue()};
        return new QSecondV(QAttr.NONE, values);
    }

    @Override
    public QSecondV visitAttrSecondV(@NotNull QParser.AttrSecondVContext ctx) {
        final QAttr attr = QAttr.get(ctx.ATTR().getText());
        final QSecondV child = (QSecondV) visit(ctx.secondV());
        return attr == child.attr() ? child : new QSecondV(attr, child);
    }

    @Override
    public QTimeV visitCompactTimeV(@NotNull QParser.CompactTimeVContext ctx) {
        final List<QParser.TimeBaseContext> x = ctx.timeBase();
        final int[] values = new int[x.size() + 1];
        int i = 0;
        for (; i < x.size(); ++i) {
            QParser.TimeBaseContext base = x.get(i);
            values[i] = parseTime(base.t, base.anyNull(), base.anyInf());
        }
        final QParser.TimeTypeContext last = ctx.timeType();
        values[i] = parseTime(last.t, last.anyNull(), last.anyInf());
        return new QTimeV(QAttr.NONE, values);
    }

    @Override
    public QTimeV visitListTimeV(@NotNull QParser.ListTimeVContext ctx) {
        final List<QParser.TimeAContext> types = ctx.timeA();
        final int[] values = new int[types.size()];
        for (int i = 0; i < types.size(); ++i) {
            values[i] = ((QTime) visit(types.get(i))).getValue();
        }
        return new QTimeV(QAttr.NONE, values);
    }

    @Override
    public QTimeV visitSingletonTimeV(@NotNull QParser.SingletonTimeVContext ctx) {
        final int[] values = new int[]{((QTime) visit(ctx.timeA())).getValue()};
        return new QTimeV(QAttr.NONE, values);
    }

    @Override
    public QTimeV visitAttrTimeV(@NotNull QParser.AttrTimeVContext ctx) {
        final QAttr attr = QAttr.get(ctx.ATTR().getText());
        final QTimeV child = (QTimeV) visit(ctx.timeV());
        return attr == child.attr() ? child : new QTimeV(attr, child);
    }

    @Override
    public QMixedV visitListMixedV(@NotNull QParser.ListMixedVContext ctx) {
        final List<QParser.ExprContext> exprContexts = ctx.expr();
        final QType[] items = new QType[exprContexts.size()];
        for (int i = 0; i < items.length; ++i) {
            items[i] = visit(exprContexts.get(i));
        }
        return new QMixedV(QAttr.NONE, items);
    }

    @Override
    public QBoolean visitBasicBooleanA(@NotNull QParser.BasicBooleanAContext ctx) {
        return parseBOOLEAN_A(ctx.BOOLEAN_A()) ? QBoolean.TRUE : QBoolean.FALSE;
    }

    @Override
    public QGuid visitGuidA(@NotNull QParser.GuidAContext ctx) {
        if (ctx.anyNull() == null) {
            throw new IllegalArgumentException("'" + ctx.getText());
        }
        return new QGuid(new byte[16]);
    }

    @Override
    public QByte visitBasicByteA(@NotNull QParser.BasicByteAContext ctx) {
        final QByte result = new QByte(parseBYTE_A(ctx.BYTE_A()));
        return result;
    }

    @Override
    public QShort visitBasicShortA(@NotNull QParser.BasicShortAContext ctx) {
        final short value = parseShortFromIntBase(ctx.intBase());
        final QShort result = new QShort(value);
        return result;
    }

    @Override
    public QInt visitBasicIntA(@NotNull QParser.BasicIntAContext ctx) {
        final int value = parseIntFromIntBase(ctx.intBase());
        final QInt result = new QInt(value);
        return result;
    }

    @Override
    public QLong visitBasicLongA(@NotNull QParser.BasicLongAContext ctx) {
        final long value = parseLongFromLongType(ctx.longType());
        final QLong result = new QLong(value);
        return result;
    }

    @Override
    public QReal visitBasicRealA(@NotNull QParser.BasicRealAContext ctx) {
        final float value = parseFloatFromFloatBase(ctx.floatBase());
        return new QReal(value);
    }

    @Override
    public QFloat visitBasicFloatA(@NotNull QParser.BasicFloatAContext ctx) {
        final double value = parseDoubleFromFloatType(ctx.floatType());
        return new QFloat(value);
    }

    @Override
    public QChar visitBasicCharA(@NotNull QParser.BasicCharAContext ctx) {
        final char c = parseCHAR_A(ctx.CHAR_A());
        return new QChar(c);
    }

    @Override
    public QSymbol visitBasicSymbolA(@NotNull QParser.BasicSymbolAContext ctx) {
        final String value = ctx.SYMBOL().getText().substring(1);
        return new QSymbol(value);
    }

    @Override
    public QSymbol visitCastCharToSymbol(@NotNull QParser.CastCharToSymbolContext ctx) {
        final QChar child = (QChar) visit(ctx.charA());
        return new QSymbol(new String(new char[]{child.getValue()}));
    }

    @Override
    public QSymbol visitCastCharVToSymbol(@NotNull QParser.CastCharVToSymbolContext ctx) {
        final QCharV child = (QCharV) visit(ctx.charV());
        return new QSymbol(child.getValue());
    }

    @Override
    public QTimestamp visitBasicTimestampA(@NotNull QParser.BasicTimestampAContext ctx) {
        final QParser.TimestampTypeContext tstCtx = ctx.timestampType();
        final long value = parseTimestamp(tstCtx.DATE(), tstCtx.t, tstCtx.anyNull(), tstCtx.anyInf());
        return QTimestamp.create(value);
    }

    @Override
    public QMonth visitBasicMonthA(@NotNull QParser.BasicMonthAContext ctx) {
        QParser.MonthBaseContext mbCtx = ctx.monthType().monthBase();
        final int value = parseMonth(mbCtx.FLOAT(), mbCtx.anyNull(), mbCtx.anyInf());
        return QMonth.create(value);
    }

    @Override
    public QDate visitBasicDateA(@NotNull QParser.BasicDateAContext ctx) {
        QParser.DateTypeContext dtCtx = ctx.dateType();
        final int value = parseDate(dtCtx.DATE(), dtCtx.anyNull(), dtCtx.anyInf());
        return QDate.create(value);
    }

    @Override
    public QDatetime visitBasicDatetimeA(@NotNull QParser.BasicDatetimeAContext ctx) {
        final QParser.DatetimeTypeContext dttCtx = ctx.datetimeType();
        final double value = parseDatetime(dttCtx.DATE(), dttCtx.t, dttCtx.anyNull(), dttCtx.anyInf());
        return QDatetime.create(value);
    }

    @Override
    public QTimespan visitBasicTimespanA(@NotNull QParser.BasicTimespanAContext ctx) {
        final QParser.TimespanTypeContext tstCtx = ctx.timespanType();
        final long value = parseTimespan(tstCtx.INTEGER(), tstCtx.t, tstCtx.anyNull(), tstCtx.anyInf());
        return QTimespan.create(value);
    }

    @Override
    public QMinute visitBasicMinuteA(@NotNull QParser.BasicMinuteAContext ctx) {
        final QParser.MinuteTypeContext mtCtx = ctx.minuteType();
        final int value = parseMinute(mtCtx.t, mtCtx.anyNull(), mtCtx.anyInf());
        return QMinute.create(value);
    }

    @Override
    public QSecond visitBasicSecondA(@NotNull QParser.BasicSecondAContext ctx) {
        final QParser.SecondTypeContext stCtx = ctx.secondType();
        final int value = parseSecond(stCtx.t, stCtx.anyNull(), stCtx.anyInf());
        return QSecond.create(value);

    }

    @Override
    public QTime visitBasicTimeA(@NotNull QParser.BasicTimeAContext ctx) {
        final QParser.TimeTypeContext ttCtx = ctx.timeType();
        final int value = parseTime(ttCtx.t, ttCtx.anyNull(), ttCtx.anyInf());
        return QTime.create(value);
    }

    @Override
    public QDict visitCompactDict(@NotNull QParser.CompactDictContext ctx) {
        final QList keys = (QList) visit(ctx.list(0));
        final QList values = (QList) visit(ctx.list(1));
        return (QAttr.SORTED == keys.attr()) ? new QSortedDict(keys, values) : new QDict(keys, values);
    }

    @Override
    public QDict visitAttrDict(@NotNull QParser.AttrDictContext ctx) {
        final QAttr attr = QAttr.get(ctx.ATTR().getText());
        final QDict child = (QDict) visit(ctx.dict());
        if (attr == child.key().attr()) {
            return child;
        } else {
            final QList newKey = child.key().clone(attr);
            return QAttr.SORTED == attr ? new QSortedDict(newKey, child.value()) : new QDict(newKey, child.value());
        }
    }

    @Override
    public QTable visitFlipDict(@NotNull QParser.FlipDictContext ctx) {
        final QDict child = (QDict) visit(ctx.dict());
        return new QTable(QAttr.NONE, child);
    }

    @Override
    protected QType aggregateResult(QType aggregate, QType nextResult) {
        QType result;
        if (aggregate == null) {
            result = nextResult;
        } else {
            if (nextResult == null) {
                result = aggregate;
            } else {
                throw new IllegalStateException(String.format("Cannot aggregate %s onto %s", nextResult, aggregate));
            }
        }
        //System.out.println(String.format("aggregate(aggregate=%s, nextResult=%s)->%s", aggregate, nextResult, result));
        return result;
    }

    @Override
    public QType visitChildren(@NotNull RuleNode node) {
        //System.out.println(String.format("visitChildren(%s)",node));
        final QType result = super.visitChildren(node);
        //System.out.println(String.format("visitChildren(%s)->%s", node, String.valueOf(result)));
        return result;
    }

    private boolean parseBOOLEAN_A(TerminalNode node) {
        final String text = node.getText();
        switch (text.charAt(0)) {
            case '0':
                return false;
            case '1':
                return true;
            default:
                throw new IllegalStateException();
        }
    }

    private byte parseBYTE_A(TerminalNode node) {
        final String text = node.getText();
        final int higher = Character.digit(text.charAt(2), 16);
        final int lower = Character.digit(text.charAt(3), 16);
        final byte b = (byte) ((higher << 4) + lower);
        return b;
    }

    private char parseCHAR_A(TerminalNode node) {
        final String text = node.getText();
        if (text.length() == 3) {
            return text.charAt(1);
        } else {
            throw new IllegalStateException(String.format("Unexpected character text '%s'", text));
        }
    }

    private short parseShortFromIntBase(QParser.IntBaseContext ctx) {
        if (ctx.anyNull() != null) {
            return Short.MIN_VALUE;
        } else if (ctx.anyInf() != null) {
            return ctx.anyInf().getText().charAt(0) == '-' ? Short.MIN_VALUE + 1 : Short.MAX_VALUE;
        } else {
            final int intValue = Integer.parseInt(ctx.INTEGER().getText());
            if (intValue == Integer.MIN_VALUE) {
                throw new NumberFormatException("Value out of range. Value:'" + intValue + "'");
            }
            return (short) Math.min(Short.MAX_VALUE, Math.max(intValue, Short.MIN_VALUE + 1));
        }
    }

    private int parseIntFromIntBase(QParser.IntBaseContext ctx) {
        if (ctx.anyNull() != null) {
            return Integer.MIN_VALUE;
        } else if (ctx.anyInf() != null) {
            return ctx.anyInf().getText().charAt(0) == '-' ? Integer.MIN_VALUE + 1 : Integer.MAX_VALUE;
        } else {
            final int value = Integer.parseInt(ctx.INTEGER().getText());
            if (value == Integer.MIN_VALUE) {
                throw new NumberFormatException("Value out of range. Value:'" + value + "'");
            }
            return value;
        }
    }

    private long parseLongFromIntBase(QParser.IntBaseContext ctx) {
        if (ctx.anyNull() != null) {
            return Long.MIN_VALUE;
        } else if (ctx.anyInf() != null) {
            return ctx.anyInf().getText().charAt(0) == '-' ? Long.MIN_VALUE + 1 : Long.MAX_VALUE;
        } else {
            return parseLongFromINTEGER(ctx.INTEGER());
        }
    }

    private long parseLongFromLongType(QParser.LongTypeContext ctx) {
        if (ctx.U_NULL() != null) {
            return Long.MIN_VALUE;
        } else if (ctx.U_INF() != null) {
            if (ctx.U_INF().getText().charAt(0) == '-') {
                return Long.MIN_VALUE;
            } else {
                return Long.MAX_VALUE;
            }
        } else if (ctx.INTEGER() != null) {
            return parseLongFromINTEGER(ctx.INTEGER());
        } else {
            return parseLongFromIntBase(ctx.intBase());
        }
    }

    private long parseLongFromINTEGER(TerminalNode integer) {
        final long value = Long.parseLong(integer.getText());
        if (value == Long.MIN_VALUE) {
            throw new NumberFormatException("Value out of range. Value:'" + value + "'");
        }
        return value;
    }

    private float parseFloatFromFloatBase(QParser.FloatBaseContext ctx) {
        if (ctx.intBase() != null) {
            if (ctx.intBase().anyInf() != null) {
                if (ctx.intBase().anyInf().getText().charAt(0) == '-') {
                    return Float.NEGATIVE_INFINITY;
                } else {
                    return Float.POSITIVE_INFINITY;
                }
            } else if (ctx.intBase().anyNull() != null) {
                return QReal.NULL_VALUE;
            } else return Float.parseFloat(ctx.intBase().INTEGER().getText());
        } else if (ctx.FLOAT() != null) {
            return Float.parseFloat(ctx.FLOAT().getText());
        } else throw new IllegalStateException();
    }

    private double parseDoubleFromFloatBase(QParser.FloatBaseContext ctx) {
        if (ctx.intBase() != null) {
            if (ctx.intBase().anyInf() != null) {
                if (ctx.intBase().anyInf().getText().charAt(0) == '-') {
                    return Double.NEGATIVE_INFINITY;
                } else {
                    return Double.POSITIVE_INFINITY;
                }
            } else if (ctx.intBase().anyNull() != null) {
                return QFloat.NULL_VALUE;
            } else return Double.parseDouble(ctx.intBase().INTEGER().getText());
        } else if (ctx.FLOAT() != null) {
            return Double.parseDouble(ctx.FLOAT().getText());
        } else throw new IllegalStateException();
    }

    private double parseDoubleFromFloatType(QParser.FloatTypeContext ctx) {
        if (ctx.L_NULL() != null) {
            return QFloat.NULL_VALUE;
        } else if (ctx.L_INF() != null) {
            if (ctx.L_INF().getText().charAt(0) == '-') {
                return Double.NEGATIVE_INFINITY;
            } else {
                return Double.POSITIVE_INFINITY;
            }
        } else if (ctx.FLOAT() != null) {
            return Double.parseDouble(ctx.FLOAT().getText());
        } else {
            return parseDoubleFromFloatBase(ctx.floatBase());
        }
    }

    private long parseNanosFromTime(Token time) {
        String text = time.getText();
        long factor = 1;
        if (text.charAt(0) == '-') {
            factor = -1L;
            text = text.substring(1);
        }
        final int hour = Integer.parseInt(text.substring(0, 2));
        final int minute = Integer.parseInt(text.substring(3, 5));
        int seconds = 0;
        int msOrNs = 0;
        if (text.length() > 6) {
            seconds = Integer.parseInt(text.substring(6, 8));
            if (text.length() > 9) {
                msOrNs = Integer.parseInt(StringUtils.rightPad(Util.truncate(text.substring(9), Util.NANOS_DIGITS), Util.NANOS_DIGITS, '0'));
            }
        }
        return factor * Util.toNanos(hour, minute, seconds, msOrNs);
    }

    /**
     * Returns date value, ignoring possible '-' prefix.
     *
     * @param date
     * @return
     */
    private int parseQEpochDaysFromDate(TerminalNode date) {
        String text = date.getText();
        if (text.charAt(0) == '-') {
            text = text.substring(1);
        }
        final int year = Integer.parseInt(text.substring(0, 4));
        final int month = Integer.parseInt(text.substring(5, 7));
        final int day = Integer.parseInt(text.substring(8, 10));
        return Util.toQEpochDays(year, month, day);
    }

    private long parseTimestamp(TerminalNode date, Token t, QParser.AnyNullContext anyNullCtx, QParser.AnyInfContext anyInfCtx) {
        if (anyNullCtx != null) {
            return QTimestamp.NULL_VALUE;
        } else if (anyInfCtx != null) {
            return anyInfCtx.getText().charAt(0) == '-' ? QTimestamp.NEGATIVE_INFINITY_VALUE : QTimestamp.POSITIVE_INFINITY_VALUE;
        } else {
            final int qEpochDays = parseQEpochDaysFromDate(date);
            final long factor = date.getText().charAt(0) == '-' ? -1L : 1L;
            final long timeNanos = parseNanosFromTime(t);
            final long value = factor * (TimeUnit.DAYS.toNanos(qEpochDays) + timeNanos);
            return value;
        }
    }

    private int parseMonth(TerminalNode floatNode, QParser.AnyNullContext anyNullCtx, QParser.AnyInfContext anyInfCtx) {
        if (anyNullCtx != null) {
            return QMonth.NULL_VALUE;
        } else if (anyInfCtx != null) {
            return anyInfCtx.getText().charAt(0) == '-' ? QMonth.NEGATIVE_INFINITY_VALUE : QMonth.POSITIVE_INFINITY_VALUE;
        } else {
            final String text = floatNode.getText();
            final int dotIndex = text.indexOf('.');
            final int year = Integer.parseInt(text.substring(0, dotIndex));
            final int month = Integer.parseInt(text.substring(dotIndex + 1));
            final int factor = (year < 0) ? -1 : 1;
            final int value = factor * Util.toQEpochMonths(year * factor, month);
            return value;
        }
    }

    private int parseDate(TerminalNode date, QParser.AnyNullContext anyNullCtx, QParser.AnyInfContext anyInfCtx) {
        if (anyNullCtx != null) {
            return QDate.NULL_VALUE;
        } else if (anyInfCtx != null) {
            return anyInfCtx.getText().charAt(0) == '-' ? QDate.NEGATIVE_INFINITY_VALUE : QDate.POSITIVE_INFINITY_VALUE;
        } else {
            final int factor = date.getText().charAt(0) == '-' ? -1 : 1;
            return factor * parseQEpochDaysFromDate(date);
        }
    }

    private double parseDatetime(TerminalNode date, Token t, QParser.AnyNullContext anyNullCtx, QParser.AnyInfContext anyInfCtx) {
        if (anyNullCtx != null) {
            return QDatetime.NULL_VALUE;
        } else if (anyInfCtx != null) {
            return anyInfCtx.getText().charAt(0) == '-' ? QDatetime.NEGATIVE_INFINITY_VALUE : QDatetime.POSITIVE_INFINITY_VALUE;
        } else {
            final int factor = date.getText().charAt(0) == '-' ? -1 : 1;
            final long dayMillis = TimeUnit.NANOSECONDS.toMillis(parseNanosFromTime(t));
            if (dayMillis < 0) {
                throw new RuntimeException("millis < 0");
            }
            final double dayFraction = ((double) dayMillis) / Util.MILLIS_PER_DAY;
            final double value = factor * (parseQEpochDaysFromDate(date) + dayFraction);
            return value;
        }
    }

    private long parseTimespan(TerminalNode integer, Token t, QParser.AnyNullContext anyNullCtx, QParser.AnyInfContext anyInfCtx) {
        if (anyNullCtx != null) {
            return QTimespan.NULL_VALUE;
        } else if (anyInfCtx != null) {
            return anyInfCtx.getText().charAt(0) == '-' ? QTimespan.NEGATIVE_INFINITY_VALUE : QTimespan.POSITIVE_INFINITY_VALUE;
        } else {
            long nanos = 0L;
            boolean negative = false;
            if (integer != null) {
                final String iText = integer.getText();
                nanos = TimeUnit.DAYS.toNanos(Integer.parseInt(iText));
                if (iText.charAt(0) == '-') {
                    negative = true;
                }
            }
            final long timeNanos = parseNanosFromTime(t);
            nanos = negative ? nanos - timeNanos : nanos + timeNanos;
            return nanos;
        }
    }

    private int parseMinute(Token t, QParser.AnyNullContext anyNullCtx, QParser.AnyInfContext anyInfCtx) {
        if (anyNullCtx != null) {
            return QMinute.NULL_VALUE;
        } else if (anyInfCtx != null) {
            return anyInfCtx.getText().charAt(0) == '-' ? QMinute.NEGATIVE_INFINITY_VALUE : QMinute.POSITIVE_INFINITY_VALUE;
        } else {
            return (int) TimeUnit.NANOSECONDS.toMinutes(parseNanosFromTime(t));
        }
    }

    private int parseSecond(Token t, QParser.AnyNullContext anyNullCtx, QParser.AnyInfContext anyInfCtx) {
        if (anyNullCtx != null) {
            return QSecond.NULL_VALUE;
        } else if (anyInfCtx != null) {
            return anyInfCtx.getText().charAt(0) == '-' ? QSecond.NEGATIVE_INFINITY_VALUE : QSecond.POSITIVE_INFINITY_VALUE;
        } else {
            final int value = (int) TimeUnit.NANOSECONDS.toSeconds(parseNanosFromTime(t));
            return value;
        }
    }

    private int parseTime(Token t, QParser.AnyNullContext anyNullCtx, QParser.AnyInfContext anyInfCtx) {
        if (anyNullCtx != null) {
            return QTime.NULL_VALUE;
        } else if (anyInfCtx != null) {
            return anyInfCtx.getText().charAt(0) == '-' ? QTime.NEGATIVE_INFINITY_VALUE : QTime.POSITIVE_INFINITY_VALUE;
        } else {
            final int value = (int) TimeUnit.NANOSECONDS.toMillis(parseNanosFromTime(t));
            return value;
        }

    }
}
