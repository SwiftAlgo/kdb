#!/bin/bash --login

###
# #%L
# KDB+ Parser
# %%
# Copyright (C) 2017 SwiftAlgo Limited
# %%
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#      http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# #L%
###
shopt -s expand_aliases
if [ $1 ]; then
    echo "antlr4 $1.g4"
    antlr4 -visitor $1.g4 #-no-listener
    echo "javac $1*.java"
    javac $1*.java
    echo "grun $1 $2 $3 $4"
    grun $1 $2 $3 $4
else
    echo "Usage: $0 grammar_name target_rule [grun_opt1] [grun_opt2]"
    echo "Example: $0 Q prog -tokens -tree"
fi

