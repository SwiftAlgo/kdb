grammar Q;
/*-
 * #%L
 * KDB+ Parser
 * %%
 * Copyright (C) 2017 SwiftAlgo Limited
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

//Parser rules - will use first specified match.
prog           : expr_separator* expr (expr_separator+ expr)* expr_separator* EOF;
expr_separator : WS? (LIST_SEPARATOR | LINE_SEPARATOR) WS?;

singleExprEOF  : WS? expr WS? EOF;

expr           : list | atom | dict | table;

atom           : booleanA | guidA | byteA | shortA | intA | longA | realA | floatA | charA | symbolA | timestampA | monthA | dateA | datetimeA | timespanA | minuteA | secondA | timeA;
list           : booleanV | guidV | byteV | shortV | intV | longV | realV | floatV | charV | symbolV | timestampV | monthV | dateV | datetimeV | timespanV | minuteV | secondV | timeV | mixedV;
dict           : list WS? XKEY WS? list                                                                             #compactDict
               | '(' WS? dict WS? ')'                                                                               #enclosedDict
               | ATTR WS? dict                                                                                      #attrDict
               ;
table          : FLIP WS dict                                                                                       #flipDict
               | '(' WS? table WS? ')'                                                                              #enclosedTable
               ;
booleanV       : BOOLEAN_V                                                                                          #compactBooleanV
               | '(' WS? booleanA (WS? LIST_SEPARATOR WS? booleanA)+ WS? ')'                                        #listBooleanV
               | ENLIST WS booleanA                                                                                 #singletonBooleanV
               | '(' WS? booleanV WS? ')'                                                                           #enclosedBooleanV
               | ATTR WS? booleanV                                                                                  #attrBooleanV
               ;
guidV          : (anyNull WS)+ anyNull 'g'                                                                          #compactGuidV
               | '(' WS? guidA (WS? LIST_SEPARATOR WS? guidA)+ WS? ')'                                              #listGuidV
               | ENLIST WS guidA                                                                                    #singletonGuidV
               | ATTR WS? guidV                                                                                     #attrGuidV
               ;
byteV          : BYTE_V                                                                                             #compactByteV
               | '(' WS? byteA (WS? LIST_SEPARATOR WS? byteA)+ WS? ')'                                              #listByteV
               | ENLIST WS byteA                                                                                    #singletonByteV
               | '(' WS? byteV WS? ')'                                                                              #enclosedByteV
               | ATTR WS? byteV                                                                                     #attrByteV
               ;
shortV         : (intBase WS)+ intBase 'h'                                                                          #compactShortV
               | '(' WS? shortA (WS? LIST_SEPARATOR WS? shortA)+ WS? ')'                                            #listShortV
               | ENLIST WS shortA                                                                                   #singletonShortV
               | '(' WS? shortV WS? ')'                                                                             #enclosedShortV
               | ATTR WS? shortV                                                                                    #attrShortV
               ;
intV           : (intBase WS)+ intBase 'i'                                                                          #compactIntV
               | '(' WS? intA (WS? LIST_SEPARATOR WS? intA)+ WS? ')'                                                #listIntV
               | ENLIST WS intA                                                                                     #singletonIntV
               | '(' WS? intV WS? ')'                                                                               #enclosedIntV
               | ATTR WS? intV                                                                                      #attrIntV
               ;
longV          : (intBase WS)+ longType                                                                             #compactLongV
               | '(' WS? longA (WS? LIST_SEPARATOR WS? longA)+ WS? ')'                                              #listLongV
               | ENLIST WS longA                                                                                    #singletonLongV
               | '(' WS? longV WS? ')'                                                                              #enclosedLongV
               | ATTR WS? longV                                                                                     #attrLongV
               ;
realV          : (floatBase WS)+ floatBase 'e'                                                                      #compactRealV
               | '(' WS? realA (WS? LIST_SEPARATOR WS? realA)+ WS? ')'                                              #listRealV
               | ENLIST WS realA                                                                                    #singletonRealV
               | '(' WS? realV WS? ')'                                                                              #enclosedRealV
               | ATTR WS? realV                                                                                     #attrRealV
               ;

floatV         : (floatBase WS)+ floatType                                                                          #compactFloatV
               | '(' WS? floatA (WS? LIST_SEPARATOR WS? floatA)+ WS? ')'                                            #listFloatV
               | ENLIST WS floatA                                                                                   #singletonFloatV
               | '(' WS? floatV WS? ')'                                                                             #enclosedFloatV
               | ATTR WS? floatV                                                                                    #attrFloatV
               ;

charV          : CHAR_V                                                                                             #compactCharV
               | '(' WS? charA (WS? LIST_SEPARATOR WS? charA)+ WS? ')'                                              #listCharV
               | ENLIST WS charA                                                                                    #singletonCharV
               | '(' WS? charV WS? ')'                                                                              #enclosedCharV
               | ATTR WS? charV                                                                                     #attrCharV
               ;
symbolV        : SYMBOL SYMBOL+                                                                                     #compactSymbolV
                //No whitespace ie. ``IBM`MSFT` => (anyNull;`IBM;`MSFT)
               | '(' WS? symbolA (WS? LIST_SEPARATOR WS? symbolA)+ WS? ')'                                          #listSymbolV
               | '(' WS? symbolV WS? ')'                                                                            #enclosedSymbolV
               | ENLIST WS symbolA                                                                                  #singletonSymbolV
               | ATTR WS? symbolV                                                                                   #attrSymbolV
               ;
timestampV     : (timestampBase WS)+ timestampType                                                                  #compactTimestampV
               | '(' WS? timestampA (WS? LIST_SEPARATOR WS? timestampA)+ WS? ')'                                    #listTimestampV
               | ENLIST WS timestampA                                                                               #singletonTimestampV
               | '(' WS? timestampV WS? ')'                                                                         #enclosedTimestampV
               | ATTR WS? timestampV                                                                                #attrTimestampV
               ;
monthV         : (monthBase WS)+ monthType                                                                          #compactMonthV
               | '(' WS? monthA (WS? LIST_SEPARATOR WS? monthA)+ WS? ')'                                            #listMonthV
               | ENLIST WS monthA                                                                                   #singletonMonthV
               | '(' WS? monthV WS? ')'                                                                             #enclosedMonthV
               | ATTR WS? monthV                                                                                    #attrMonthV
               ;
dateV          : (dateBase WS)+ dateType                                                                            #compactDateV
               | '(' WS? dateA (WS? LIST_SEPARATOR WS? dateA)+ WS? ')'                                              #listDateV
               | ENLIST WS dateA                                                                                    #singletonDateV
               | '(' WS? dateV WS? ')'                                                                              #enclosedDateV
               | ATTR WS? dateV                                                                                     #attrDateV
               ;
datetimeV      : (datetimeBase WS)+ datetimeType                                                                    #compactDatetimeV
               | '(' WS? datetimeA (WS? LIST_SEPARATOR WS? datetimeA)+ WS? ')'                                      #listDatetimeV
               | ENLIST WS datetimeA                                                                                #singletonDatetimeV
               | '(' WS? datetimeV WS? ')'                                                                          #enclosedDatetimeV
               | ATTR WS? datetimeV                                                                                 #attrDatetimeV
               ;
timespanV      : (timespanBase WS)+ timespanType                                                                    #compactTimespanV
               | '(' WS? timespanA (WS? LIST_SEPARATOR WS? timespanA)+ WS? ')'                                      #listTimespanV
               | ENLIST WS timespanA                                                                                #singletonTimespanV
               | '(' WS? timespanV WS? ')'                                                                          #enclosedTimespanV
               | ATTR WS? timespanV                                                                                 #attrTimespanV
               ;
minuteV        : (timeBase WS)+ minuteType                                                                          #compactMinuteV
               | '(' WS? minuteA (WS? LIST_SEPARATOR WS? minuteA)+ WS? ')'                                          #listMinuteV
               | ENLIST WS minuteA                                                                                  #singletonMinuteV
               | '(' WS? minuteV WS? ')'                                                                            #enclosedMinuteV
               | ATTR WS? minuteV                                                                                   #attrMinuteV
               ;
secondV        : (timeBase WS)+ secondType                                                                          #compactSecondV
               | '(' WS? secondA (WS? LIST_SEPARATOR WS? secondA)+ WS? ')'                                          #listSecondV
               | ENLIST WS secondA                                                                                  #singletonSecondV
               | '(' WS? secondV WS? ')'                                                                            #enclosedSecondV
               | ATTR WS? secondV                                                                                   #attrSecondV
               ;
timeV          : (timeBase WS)+ timeType                                                                            #compactTimeV
               | '(' WS? timeA (WS? LIST_SEPARATOR WS? timeA)+ WS? ')'                                              #listTimeV
               | ENLIST WS timeA                                                                                    #singletonTimeV
               | '(' WS? timeV WS? ')'                                                                              #enclosedTimeV
               | ATTR WS? timeV                                                                                     #attrTimeV
               ;

mixedV         : '(' WS? expr (WS? LIST_SEPARATOR+ WS? expr)+ WS? ')'                                               #listMixedV
               | '(' WS? mixedV WS? ')'                                                                             #enclosedMixedV
               ;

booleanA       : BOOLEAN_A                                                                                          #basicBooleanA
               | '(' WS? booleanA WS? ')'                                                                           #enclosedBooleanA
               ;
guidA          : anyNull 'g';
byteA          : BYTE_A                                                                                             #basicByteA
               | '(' WS? byteA WS? ')'                                                                              #enclosedByteA
               ;
shortA         : intBase 'h'                                                                                        #basicShortA
               | '(' WS? shortA WS? ')'                                                                             #enclosedShortA
               ;
intA           : intBase 'i'                                                                                        #basicIntA
               | '(' WS? intA WS? ')'                                                                               #enclosedIntA
               ;
longA          : longType                                                                                           #basicLongA
               | '(' WS? longA WS? ')'                                                                              #enclosedLongA
               ;

realA          : floatBase 'e'                                                                                      #basicRealA
               | '(' WS? realA WS? ')'                                                                              #enclosedRealA
               ;
floatA         : floatType                                                                                          #basicFloatA
               | '(' WS? floatA WS? ')'                                                                             #enclosedFloatA
               ;
charA          : CHAR_A                                                                                             #basicCharA
               | '(' WS? charA WS? ')'                                                                              #enclosedCharA
               ;
symbolA        : SYMBOL                                                                                             #basicSymbolA
               | SYMBOL_CAST WS? charA                                                                              #castCharToSymbol
               | SYMBOL_CAST WS? charV                                                                              #castCharVToSymbol
               | '(' WS? symbolA WS? ')'                                                                            #enclosedSymbolA
               ;
timestampBase  : DATE 'D' t=(TIME_MINUTES | TIME_SECONDS | TIME_MILLIS | TIME_NANOS) | anyNull | anyInf;
timestampType  : DATE 'D' t=(TIME_MINUTES | TIME_SECONDS | TIME_MILLIS | TIME_NANOS) 'p'? | (anyNull | anyInf) 'p';
timestampA     : timestampType                                                                                      #basicTimestampA
               | '(' WS? timestampA WS? ')'                                                                         #enclosedTimestampA
               ;

monthBase      : FLOAT | anyNull | anyInf;
monthType      : monthBase 'm';
monthA         : monthType                                                                                          #basicMonthA
               | '(' WS? monthA WS? ')'                                                                             #enclosedMonthA
               ;

dateBase       : DATE | anyNull | anyInf;
dateType       : DATE 'd'? | (anyNull | anyInf) 'd';
dateA          : dateType                                                                                           #basicDateA
               | '(' WS? dateA WS? ')'                                                                              #enclosedDateA
               ;

datetimeBase   : DATE 'T' t=(TIME_MINUTES | TIME_SECONDS | TIME_MILLIS | TIME_NANOS) | anyNull | anyInf;
datetimeType   : DATE 'T' t=(TIME_MINUTES | TIME_SECONDS | TIME_MILLIS | TIME_NANOS) 'z'? | (anyNull | anyInf) 'z';
datetimeA      : datetimeType                                                                                       #basicDatetimeA
               | '(' WS? datetimeA WS? ')'                                                                          #enclosedDatetimeA
               ;

timespanBase   : (INTEGER 'D')? t=(TIME_NANOS | TIME_SECONDS | TIME_MINUTES | TIME_MILLIS) | anyNull | anyInf;
timespanType   : (INTEGER 'D')? (t=TIME_NANOS 'n'? | (t=(TIME_SECONDS | TIME_MINUTES | TIME_MILLIS) | anyNull | anyInf) 'n');
timespanA      : timespanType                                                                                       #basicTimespanA
               | '(' WS? timespanA WS? ')'                                                                          #enclosedTimespanA
               ;

minuteType     : t=TIME_MINUTES 'u'? | ( t=(TIME_SECONDS | TIME_MILLIS | TIME_NANOS) | anyNull | anyInf) 'u';
minuteA        : minuteType                                                                                         #basicMinuteA
               | '(' WS? minuteA WS? ')'                                                                            #enclosedMinuteA
               ;

secondType     : t=TIME_SECONDS 'v'? | ( t=(TIME_MINUTES | TIME_MILLIS | TIME_NANOS) | anyNull | anyInf) 'v';
secondA        : secondType                                                                                         #basicSecondA
               | '(' WS? secondA WS? ')'                                                                            #enclosedSecondA
               ;

timeBase       : t=(TIME_MINUTES | TIME_SECONDS | TIME_MILLIS | TIME_NANOS) | anyNull | anyInf;
timeType       : t=TIME_MILLIS 't'?  | ( t=(TIME_SECONDS | TIME_MINUTES | TIME_NANOS) | anyNull | anyInf) 't';
timeA          : timeType                                                                                           #basicTimeA
               | '(' WS? timeA WS? ')'                                                                              #enclosedTimeA
               ;

floatType      : L_NULL | L_INF | FLOAT | floatBase 'f';
floatBase      : intBase | FLOAT;
longType       : U_NULL | U_INF | INTEGER | intBase 'j';
intBase        : anyInf | anyNull | INTEGER;
anyNull        : L_NULL | U_NULL;
anyInf         : L_INF | U_INF;
//Lexer rules - tries to match as many characters as possible. If tied, will use first specified rule.
FLIP           : 'flip';
ENLIST         : 'enlist';
ATTR           : '`'[supg]'#';
SYMBOL_CAST    : '`$';
BOOLEAN_A      : [01] 'b';
BOOLEAN_V      : [01] [01]+ 'b';

BYTE_A         : '0x' HexByte;
BYTE_V         : '0x' HexByte HexByte+;

fragment
HexByte        : [0-9A-Fa-f][0-9A-Fa-f];
fragment
DIGIT          : [0-9];
L_NULL         : '0n';
U_NULL         : '0N';
L_INF          : NEGATIVE? '0w';
U_INF          : NEGATIVE? '0W';
POSITIVE       : '+';
NEGATIVE       : '-';
INTEGER        : NEGATIVE? DIGIT+;
FLOAT          : NEGATIVE? DIGIT+ '.' DIGIT* | NEGATIVE? DIGIT* '.' DIGIT+ | NEGATIVE? DIGIT+ '.'? DIGIT* 'e' (NEGATIVE | POSITIVE)? DIGIT+ | NEGATIVE? DIGIT* '.'? DIGIT+ 'e' (NEGATIVE | POSITIVE)? DIGIT+;

DATE           : NEGATIVE? DIGIT DIGIT DIGIT DIGIT '.' DIGIT DIGIT '.' DIGIT DIGIT; //eg. 2000.01.01
TIME_MINUTES   : NEGATIVE? DIGIT DIGIT ':' DIGIT DIGIT; //eg. 00:00.
TIME_SECONDS   : NEGATIVE? DIGIT DIGIT ':' DIGIT DIGIT ':' DIGIT DIGIT; //eg. 00:00:00
TIME_MILLIS    : NEGATIVE? DIGIT DIGIT ':' DIGIT DIGIT ':' DIGIT DIGIT '.' DIGIT? DIGIT? DIGIT? DIGIT? ; //eg. 00:00:00.000 - up to 4 decimal places.
TIME_NANOS     : NEGATIVE? DIGIT DIGIT ':' DIGIT DIGIT ':' DIGIT DIGIT '.' DIGIT DIGIT DIGIT DIGIT DIGIT+; //eg. 00:00:00.000000000 - unlimited decimal places (will be truncated).

CHAR_A         : '"'Character'"';
CHAR_V         : '"'Character*'"';

SYMBOL         : '`' ~[$#`;"\\!()[\]{} \r\n\t]*;
fragment
Character      : ESC | ~[\\"]; //ESC seq or not backslash or doublequote.  Allows \n etc.
ESC            : '\\"' | '\\\\'; // 2-char sequences \" and \\.

XKEY           : '!';
WS             : [ \t]+;

LIST_SEPARATOR : ';';
LINE_SEPARATOR : '\r'?'\n';
