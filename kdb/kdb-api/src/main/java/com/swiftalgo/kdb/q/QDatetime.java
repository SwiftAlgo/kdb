package com.swiftalgo.kdb.q;

/*-
 * #%L
 * KDB+ API
 * %%
 * Copyright (C) 2017 SwiftAlgo Limited
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.swiftalgo.kdb.q.util.Util;
import com.swiftalgo.util.DataUtil;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.format.SignStyle;

import static java.time.temporal.ChronoField.*;

public class QDatetime extends QAtom {

    public static final double NULL_VALUE = Double.longBitsToDouble(0xfff8000000000000L);
    public static final double NEGATIVE_INFINITY_VALUE = Double.longBitsToDouble(0xfff0000000000000L);
    public static final double POSITIVE_INFINITY_VALUE = Double.longBitsToDouble(0x7ff0000000000000L);

    public final static QDatetime NULL = new QDatetime(NULL_VALUE);
    public final static QDatetime NEGATIVE_INFINITY = new QDatetime(NEGATIVE_INFINITY_VALUE);
    public final static QDatetime POSITIVE_INFINITY = new QDatetime(POSITIVE_INFINITY_VALUE);

    public static final DateTimeFormatter Q_FORMATTER = new DateTimeFormatterBuilder()
            .appendValue(YEAR, 4, 10, SignStyle.EXCEEDS_PAD)
            .appendLiteral('.')
            .appendValue(MONTH_OF_YEAR, 2)
            .appendLiteral('.')
            .appendValue(DAY_OF_MONTH, 2)
            .appendLiteral('T')
            .appendValue(HOUR_OF_DAY, 2)
            .appendLiteral(':')
            .appendValue(MINUTE_OF_HOUR, 2)
            .appendLiteral(':')
            .appendValue(SECOND_OF_MINUTE, 2)
            .appendLiteral('.')
            .appendValue(MILLI_OF_SECOND, 3)
            .toFormatter();

    private final double value;

    public QDatetime(double value) {
        this.value = value;
        ByteBuffer b = (ByteBuffer) toBinary(ByteBuffer.allocate(256).order(ByteOrder.LITTLE_ENDIAN)).flip();
        System.out.println(DataUtil.byteBufferToHex(b, new StringBuilder()));
    }

    public QDatetime(ByteBuffer b) {
        super(b);
        this.value = b.getDouble();
    }

    public double getValue() {
        return value;
    }

    @Override
    public byte typeNum() {
        return DATETIME;
    }

    @Override
    public void toString(StringBuilder sb, ToStringContext ctx) {
        toString(sb, value, true);
    }

    @Override
    public int bytes() {
        return 9;
    }

    @Override
    public ByteBuffer toBinary(ByteBuffer b) {
        super.toBinary(b);
        b.putDouble(value);
        return b;
    }

    @Override
    public void accept(QVisitor v) {
        v.accept(this);
    }

    public static StringBuilder toString(StringBuilder sb, double value, boolean suffix) {
        if (Double.isNaN(value)) {
            sb.append("0N");
            if (suffix) sb.append('z');
        } else if (value == NEGATIVE_INFINITY_VALUE) {
            sb.append("-0w");
            if (suffix) sb.append('z');
        } else if (value == POSITIVE_INFINITY_VALUE) {
            sb.append("0w");
            if (suffix) sb.append('z');
        } else {
            final double jSecondsRaw = value * Util.SECONDS_PER_DAY + Util.Q_TO_JAVA_EPOCH_SECONDS;
            double jSeconds = Math.floor(jSecondsRaw);
            int nanoOfSecond = (int) Math.round((jSecondsRaw - jSeconds) * Util.MILLIS_PER_SECOND) * Util.NANOS_PER_MILLI;
            //System.out.println(String.format("value= %f, jSecondsRaw= %f, jSeconds= %f, nanoOfSecond= %d", value, jSecondsRaw, jSeconds, nanoOfSecond));
            final LocalDateTime ldt = LocalDateTime.ofEpochSecond((long) jSeconds, nanoOfSecond, ZoneOffset.UTC);
            Q_FORMATTER.formatTo(ldt, sb);
        }
        return sb;
    }

    public static QDatetime create(double value) {
        if (Double.isNaN(value)) {
            return NULL;
        } else if (value == NEGATIVE_INFINITY_VALUE) {
            return NEGATIVE_INFINITY;
        } else if (value == POSITIVE_INFINITY_VALUE) {
            return POSITIVE_INFINITY;
        } else {
            return new QDatetime(value);
        }
    }
}
