package com.swiftalgo.kdb.q;

/*-
 * #%L
 * KDB+ API
 * %%
 * Copyright (C) 2017 SwiftAlgo Limited
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.swiftalgo.kdb.q.factory.QFactory;

import java.nio.ByteBuffer;

public class QDict extends AbstractQType {

    private final QList key;
    private final QList values;

    public QDict(QList key, QList values) {
        if (key.count() != values.count()) {
            throw new IllegalStateException("'length");
        }
        this.key = key;
        this.values = values;
    }

    public QDict(QFactory factory, ByteBuffer buffer) {
        super(buffer);
        key = (QList) factory.newType(buffer);
        values = (QList) factory.newType(buffer);
    }

    public QList key() {
        return key;
    }

    public QList value() {
        return values;
    }

    @Override
    public boolean isAtom() {
        return false;
    }

    @Override
    public boolean isList() {
        return false;
    }

    public boolean isRectangular() {
        if (values.isAtom(0)) {
            return false;
        } else {
            final QMixedV mixedValues = (QMixedV) values;
            final int commonCount = ((QList) mixedValues.get(0)).count();
            for (int i = 1; i < values.count(); ++i) {
                if (values.isAtom(i)) {
                    return false;
                } else if (((QList) mixedValues.get(i)).count() != commonCount) {
                    return false;
                }
            }
            return true;
        }
    }

    @Override
    public void accept(QVisitor v) {
        v.accept(this);
    }

    @Override
    public byte typeNum() {
        return DICT;
    }

    @Override
    public byte wireTypeNum() {
        return DICT;
    }

    @Override
    public void toString(StringBuilder sb, ToStringContext ctx) {
        key.toString(sb, ctx);
        sb.append("!");
        values.toString(sb, ctx);
/*
        if (ctx.inner) {
            ctx.inner = true;
            key.toString(buffer, ctx);
            buffer.append("!");
            values.toString(buffer, ctx);
        } else {
            ctx.inner = true;
            for (int i = 0; i < key.count(); ++i) {
                key.toString(i, buffer, ctx);
                buffer.append("| ");
                values.toString(i, buffer, ctx);
                if (i < key.count() - 1) {
                    buffer.append("\n");
                }
            }
        }
*/
    }

    @Override
    public int bytes() {
        return 1 + key.bytes() + values.bytes();
    }

    @Override
    public ByteBuffer toBinary(ByteBuffer b) {
        super.toBinary(b);
        key.toBinary(b);
        values.toBinary(b);
        return b;
    }
}
