package com.swiftalgo.kdb.q;

/*-
 * #%L
 * KDB+ API
 * %%
 * Copyright (C) 2017 SwiftAlgo Limited
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

interface QVisitor {
    void accept(QBoolean b);

    void accept(QBooleanV b);

    void accept(QByte x);

    void accept(QByteV x);

    void accept(QChar c);

    void accept(QCharV c);

    void accept(QDatetime z);

    void accept(QDatetimeV z);

    void accept(QDate d);

    void accept(QDateV d);

    void accept(QDict d);

    void accept(QSortedDict d);

    void accept(QFloat f);

    void accept(QFloatV f);

    void accept(QGuid g);

    void accept(QGuidV g);

    void accept(QInt i);

    void accept(QIntV i);

    void accept(QLong j);

    void accept(QLongV j);

    void accept(QMinute u);

    void accept(QMinuteV u);

    void accept(QMixedV zero);

    void accept(QMonth m);

    void accept(QMonthV m);

    void accept(QReal e);

    void accept(QRealV e);

    void accept(QSecond v);

    void accept(QSecondV v);

    void accept(QShort h);

    void accept(QShortV h);

    void accept(QSymbol s);

    void accept(QSymbolV s);

    void accept(QTable t);

    void accept(QTimespan n);

    void accept(QTimespanV n);

    void accept(QTimestamp p);

    void accept(QTimestampV p);

    void accept(QTime t);

    void accept(QTimeV t);

}
