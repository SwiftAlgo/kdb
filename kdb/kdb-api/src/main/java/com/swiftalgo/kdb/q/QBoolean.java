package com.swiftalgo.kdb.q;

/*-
 * #%L
 * KDB+ API
 * %%
 * Copyright (C) 2017 SwiftAlgo Limited
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.swiftalgo.util.ByteBufferException;

import java.nio.ByteBuffer;

public class QBoolean extends QAtom {
    private final boolean value;

    public static final QBoolean FALSE = new QBoolean(false);

    public static final QBoolean TRUE = new QBoolean(true);

    public static QBoolean getInstance(boolean value) {
        return value ? TRUE : FALSE;
    }

    public QBoolean(boolean value) {
        this.value = value;
    }

    public QBoolean(ByteBuffer b) {
        super(b);
        final byte v = b.get();
        switch (v) {
            case ZERO:
                value = false;
                break;
            case ONE:
                value = true;
                break;
            default:
                throw new ByteBufferException(String.format("Expected boolean but was 0x%h.", v), (ByteBuffer) b.position(b.position() - 2));
        }
    }

    public boolean getValue() {
        return value;
    }

    @Override
    public byte typeNum() {
        return BOOLEAN;
    }

    @Override
    public void toString(StringBuilder sb, ToStringContext ctx) {
        sb.append(value ? "1b" : "0b");
    }

    @Override
    public int bytes() {
        return 2;
    }

    @Override
    public ByteBuffer toBinary(ByteBuffer b) {
        super.toBinary(b);
        b.put(value ? ONE : ZERO);
        return b;
    }

    @Override
    public void accept(QVisitor v) {
        v.accept(this);
    }

}
