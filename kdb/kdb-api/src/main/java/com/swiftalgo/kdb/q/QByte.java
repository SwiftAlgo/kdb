package com.swiftalgo.kdb.q;

/*-
 * #%L
 * KDB+ API
 * %%
 * Copyright (C) 2017 SwiftAlgo Limited
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.swiftalgo.util.DataUtil;

import java.nio.ByteBuffer;

public class QByte extends QAtom {

    private final byte value;

    public QByte(byte value) {
        this.value = value;
    }

    public QByte(ByteBuffer buffer) {
        super(buffer);
        value = buffer.get();
    }

    public byte getValue() {
        return value;
    }

    @Override
    public byte typeNum() {
        return BYTE;
    }

    @Override
    public void toString(StringBuilder sb, ToStringContext ctx) {
        sb.append("0x");
        DataUtil.byteToHex(value, sb);
    }

    @Override
    public int bytes() {
        return 2;
    }

    @Override
    public ByteBuffer toBinary(ByteBuffer b) {
        super.toBinary(b);
        b.put(value);
        return b;
    }

    @Override
    public void accept(QVisitor v) {
        v.accept(this);
    }

}
