package com.swiftalgo.kdb.q;

/*-
 * #%L
 * KDB+ API
 * %%
 * Copyright (C) 2017 SwiftAlgo Limited
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.swiftalgo.util.ByteBufferException;
import com.swiftalgo.util.DataUtil;

import java.nio.ByteBuffer;

public class QBooleanV extends AbstractQList {

    private final boolean[] values;

    public QBooleanV(QAttr attr, QBooleanV toCopy) {
        super(attr);
        values = toCopy.values;
    }

    public QBooleanV(QAttr attr, boolean[] values) {
        super(attr);
        if (values.length < 1) {
            throw new IllegalArgumentException();
        }
        this.values = values;
    }

    public QBooleanV(ByteBuffer b) {
        super(b);
        final int initialPos = b.position() - 2;
        int length = b.getInt();
        values = new boolean[length];
        for (int i = 0; i < length; ++i) {
            final byte booleanByte = b.get();
            if ((booleanByte & 0xFE) == ZERO) {
                values[i] = booleanByte == ONE ? true : false;
            } else {
                throw new ByteBufferException(String.format("Invalid boolean byte 0x%s.", DataUtil.byteToHex(booleanByte)), (ByteBuffer) b.position(initialPos));
            }
        }
    }

    @Override
    public QBooleanV clone(QAttr attr) {
        return attr == attr() ? this : new QBooleanV(attr, values);
    }

    @Override
    public boolean isAtom(int i) {
        return true;
    }

    @Override
    public byte typeNum() {
        return BOOLEAN_V;
    }

    @Override
    public void toString(StringBuilder sb, ToStringContext ctx) {
        sb.append(attr().toString());
        if (values.length == 1) {
            sb.append(","); //singleton eg. ,1b
        }
        for (int i = 0; i < values.length; ++i) {
            sb.append(values[i] ? "1" : "0");
        }
        sb.append("b");
    }

    @Override
    public void toString(int i, StringBuilder sb, ToStringContext ctx) {
        sb.append(values[i] ? "1b" : "0b");
    }

    @Override
    public int bytes() {
        return super.bytes() + values.length;
    }

    @Override
    public ByteBuffer toBinary(ByteBuffer b) {
        super.toBinary(b);
        for (int i = 0; i < values.length; ++i) {
            b.put(values[i] ? ONE : ZERO);
        }
        return b;
    }

    @Override
    public void accept(QVisitor v) {
        v.accept(this);
    }

    @Override
    public int count() {
        return values.length;
    }
}
