package com.swiftalgo.kdb.q;

/*-
 * #%L
 * KDB+ API
 * %%
 * Copyright (C) 2017 SwiftAlgo Limited
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.nio.ByteBuffer;

public class QFloatV extends AbstractQList {

    private final double[] values;

    public QFloatV(QAttr attr, double[] values) {
        super(attr);
        this.values = values;
    }

    public QFloatV(ByteBuffer b) {
        super(b);
        final int count = b.getInt();
        values = new double[count];
        for (int i = 0; i < count; ++i) {
            values[i] = b.getDouble();
        }
    }

    public QFloatV(QAttr attr, QFloatV toCopy) {
        super(attr);
        this.values = toCopy.values;
    }

    @Override
    public QFloatV clone(QAttr attr) {
        return attr == attr() ? this : new QFloatV(attr, values);
    }

    @Override
    public boolean isAtom(int i) {
        return true;
    }

    @Override
    public byte typeNum() {
        return FLOAT_V;
    }

    @Override
    public void toString(StringBuilder sb, ToStringContext ctx) {
        sb.append(attr().toString());
        if (values.length == 1) {
            sb.append(","); //singleton list eg. ,1f
        }
        int i = 0;
        for (; i < values.length - 1; ++i) {
            QFloat.toString(sb, values[i], false, ctx);
            sb.append(" ");
        }
        QFloat.toString(sb, values[i], true, ctx);
    }

    @Override
    public void toString(int index, StringBuilder sb, ToStringContext ctx) {
        QFloat.toString(sb, values[index], true, ctx);
    }

    @Override
    public int bytes() {
        return super.bytes() + (values.length << 3); //x8 bytes per value
    }

    @Override
    public ByteBuffer toBinary(ByteBuffer b) {
        super.toBinary(b);
        for (int i = 0; i < values.length; ++i) {
            b.putDouble(values[i]);
        }
        return b;
    }

    @Override
    public void accept(QVisitor v) {
        v.accept(this);
    }

    @Override
    public int count() {
        return values.length;
    }
}
