package com.swiftalgo.kdb.q;

/*-
 * #%L
 * KDB+ API
 * %%
 * Copyright (C) 2017 SwiftAlgo Limited
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.swiftalgo.util.DataUtil;

import java.nio.ByteBuffer;
import java.util.Arrays;

public class QByteV extends AbstractQList {

    private final byte[] bytes;

    public QByteV(QAttr attr, byte[] bytes) {
        super(attr);
        if (bytes.length < 1) {
            throw new IllegalArgumentException();
        }
        this.bytes = bytes;
    }

    public QByteV(ByteBuffer buffer) {
        super(buffer);
        final int length = buffer.getInt();
        bytes = Arrays.copyOfRange(buffer.array(), buffer.position(), buffer.position() + length);
    }

    public QByteV(QAttr attr, QByteV toCopy) {
        super(attr);
        this.bytes = toCopy.bytes;
    }

    @Override
    public QByteV clone(QAttr attr) {
        return attr == attr() ? this : new QByteV(attr, bytes);
    }

    @Override
    public boolean isAtom(int i) {
        return true;
    }

    @Override
    public byte typeNum() {
        return BYTE_V;
    }

    @Override
    public void toString(StringBuilder sb, ToStringContext ctx) {
        super.toString(sb, ctx);
        if (bytes.length == 1) {
            sb.append(","); //singleton list eg. ,0xFF
        }
        sb.append("0x");
        DataUtil.byteArrayToHex(bytes, sb);
    }

    @Override
    public void toString(int index, StringBuilder sb, ToStringContext ctx) {
        sb.append("0x");
        final char c1 = Character.forDigit(bytes[index] >>> 4 & 0x0F, 16);
        final char c2 = Character.forDigit(bytes[index] & 0x0F, 16);
        sb.append(c1).append(c2);
    }

    @Override
    public int bytes() {
        return super.bytes() + bytes.length;
    }

    @Override
    public ByteBuffer toBinary(ByteBuffer b) {
        super.toBinary(b);
        b.put(bytes);
        return b;
    }

    @Override
    public void accept(QVisitor v) {
        v.accept(this);
    }

    @Override
    public int count() {
        return bytes.length;
    }
}
