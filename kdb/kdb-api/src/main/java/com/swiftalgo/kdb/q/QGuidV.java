package com.swiftalgo.kdb.q;

/*-
 * #%L
 * KDB+ API
 * %%
 * Copyright (C) 2017 SwiftAlgo Limited
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.nio.ByteBuffer;
import java.util.Arrays;

public class QGuidV extends AbstractQList {

    private final byte[] bytes;

    public QGuidV(QAttr attr, byte[] bytes) {
        super(attr);
        if ((bytes.length % 16) != 0) {
            throw new IllegalArgumentException();
        }
        this.bytes = bytes;
    }

    public QGuidV(ByteBuffer b) {
        super(b);
        final int count = b.getInt();
        bytes = Arrays.copyOfRange(b.array(), b.position(), b.position() + count * 16);
    }

    public QGuidV(QAttr attr, QGuidV toCopy) {
        super(attr);
        this.bytes = toCopy.bytes;
    }

    @Override
    public QGuidV clone(QAttr attr) {
        return attr == attr() ? this : new QGuidV(attr, bytes);
    }

    @Override
    public boolean isAtom(int i) {
        return true;
    }

    @Override
    public byte typeNum() {
        return GUID_V;
    }

    @Override
    public void toString(StringBuilder sb, ToStringContext ctx) {
        sb.append(attr().toString());
        if (bytes.length == 16) {
            sb.append(","); //singleton list eg. ,0xFF
        }
        for (int i = 0; i < bytes.length; i += 16) {
            QGuid.toString(bytes, i, sb);
            if (i < bytes.length - 16) {
                sb.append(" ");
            }
        }
    }

    @Override
    public void toString(int index, StringBuilder b, ToStringContext ctx) {
        QGuid.toString(bytes, index << 4, b);
    }

    @Override
    public int bytes() {
        return super.bytes() + bytes.length;
    }

    @Override
    public ByteBuffer toBinary(ByteBuffer b) {
        super.toBinary(b);
        b.put(bytes);
        return b;
    }

    @Override
    public void accept(QVisitor v) {
        v.accept(this);
    }

    @Override
    public int count() {
        return bytes.length >> 4;
    }

}
