package com.swiftalgo.kdb.q;

/*-
 * #%L
 * KDB+ API
 * %%
 * Copyright (C) 2017 SwiftAlgo Limited
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.nio.ByteBuffer;

public class QInt extends QAtom {

    public static final int NULL_VALUE = Integer.MIN_VALUE;
    public static final int NEGATIVE_INFINITY_VALUE = Integer.MIN_VALUE + 1;
    public static final int POSITIVE_INFINITY_VALUE = Integer.MAX_VALUE;

    public final static QInt NULL = new QInt(NULL_VALUE);
    public final static QInt NEGATIVE_INFINITY = new QInt(NEGATIVE_INFINITY_VALUE);
    public final static QInt POSITIVE_INFINITY = new QInt(POSITIVE_INFINITY_VALUE);

    private final int value;

    public QInt(int value) {
        this.value = value;
    }

    public QInt(ByteBuffer buffer) {
        super(buffer);
        this.value = buffer.getInt();
    }

    public int getValue() {
        return value;
    }

    @Override
    public byte typeNum() {
        return INT;
    }

    @Override
    public void toString(StringBuilder sb, ToStringContext ctx) {
        toString(sb, value, true);
    }

    @Override
    public int bytes() {
        return 5;
    }

    @Override
    public ByteBuffer toBinary(ByteBuffer b) {
        super.toBinary(b);
        b.putInt(value);
        return b;
    }

    @Override
    public void accept(QVisitor v) {
        v.accept(this);
    }

    public static StringBuilder toString(StringBuilder sb, int value, boolean suffix) {
        switch (value) {
            case NULL_VALUE:
                sb.append("0N");
                break;
            case NEGATIVE_INFINITY_VALUE:
                sb.append("-");
            case POSITIVE_INFINITY_VALUE:
                sb.append("0W");
                break;
            default:
                sb.append(Integer.toString(value));
        }
        if (suffix) sb.append('i');
        return sb;
    }

}
