package com.swiftalgo.kdb.q;

/*-
 * #%L
 * KDB+ API
 * %%
 * Copyright (C) 2017 SwiftAlgo Limited
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.swiftalgo.kdb.q.util.Util;

import java.nio.ByteBuffer;
import java.util.concurrent.TimeUnit;

public class QTimespan extends QAtom {

    public static final long NULL_VALUE = Long.MIN_VALUE;
    public static final long NEGATIVE_INFINITY_VALUE = Long.MIN_VALUE + 1;
    public static final long POSITIVE_INFINITY_VALUE = Long.MAX_VALUE;

    public final static QTimespan NULL = new QTimespan(NULL_VALUE);
    public final static QTimespan NEGATIVE_INFINITY = new QTimespan(NEGATIVE_INFINITY_VALUE);
    public final static QTimespan POSITIVE_INFINITY = new QTimespan(POSITIVE_INFINITY_VALUE);


    private final long value;

    public QTimespan(long value) {
        this.value = value;
    }

    public QTimespan(ByteBuffer b) {
        super(b);
        this.value = b.getLong();
    }

    public long getValue() {
        return value;
    }

    @Override
    public byte typeNum() {
        return TIMESPAN;
    }

    @Override
    public void toString(StringBuilder sb, ToStringContext ctx) {
        toString(sb, value, true);
    }

    @Override
    public int bytes() {
        return 9;
    }

    @Override
    public ByteBuffer toBinary(ByteBuffer b) {
        super.toBinary(b);
        b.putLong(value);
        return b;
    }

    @Override
    public void accept(QVisitor v) {
        v.accept(this);
    }

    public static StringBuilder toString(StringBuilder sb, long value, boolean suffix) {
        if (value == NULL_VALUE) {
            sb.append("0N");
            if (suffix) sb.append('n');
        } else if (value == NEGATIVE_INFINITY_VALUE) {
            sb.append("-0W");
            if (suffix) sb.append('n');
        } else if (value == POSITIVE_INFINITY_VALUE) {
            sb.append("0W");
            if (suffix) sb.append('n');
        } else {
            if (value < 0L) {
                sb.append('-');
            }
            final long absValue = Math.abs(value);
            final int days = (int) TimeUnit.NANOSECONDS.toDays(absValue);
            //Use abs() in case of negative time eg. -1D11:30:45.123456789
            final int hours = (int) TimeUnit.NANOSECONDS.toHours(absValue) % 24;
            final int minutes = (int) TimeUnit.NANOSECONDS.toMinutes(absValue) % 60;
            final int seconds = (int) TimeUnit.NANOSECONDS.toSeconds(absValue) % 60;
            final int nanos = (int) (absValue % Util.NANOS_PER_SECOND);
            sb.append(String.format("%dD%02d:%02d:%02d.%09d", days, hours, minutes, seconds, nanos));
        }
        return sb;
    }

    public static QTimespan create(long value) {
        if (value == NULL_VALUE) {
            return NULL;
        } else if (value == NEGATIVE_INFINITY_VALUE) {
            return NEGATIVE_INFINITY;
        } else if (value == POSITIVE_INFINITY_VALUE) {
            return POSITIVE_INFINITY;
        } else {
            return new QTimespan(value);
        }
    }
}
