package com.swiftalgo.kdb.q;

/*-
 * #%L
 * KDB+ API
 * %%
 * Copyright (C) 2017 SwiftAlgo Limited
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.nio.ByteBuffer;

public class QFloat extends QAtom {

    public static final double NULL_VALUE = Double.longBitsToDouble(0xfff8000000000000L);
    public static final double NEGATIVE_INFINITY_VALUE = Double.NEGATIVE_INFINITY;
    public static final double POSITIVE_INFINITY_VALUE = Double.POSITIVE_INFINITY;

    public static final QFloat NULL = new QFloat(NULL_VALUE);
    public final static QFloat NEGATIVE_INFINITY = new QFloat(NEGATIVE_INFINITY_VALUE);
    public final static QFloat POSITIVE_INFINITY = new QFloat(POSITIVE_INFINITY_VALUE);

    private final double value;

    public QFloat(double value) {
        this.value = value;
    }

    public QFloat(ByteBuffer buffer) {
        super(buffer);
        this.value = buffer.getDouble();
    }

    public double getValue() {
        return value;
    }

    @Override
    public byte typeNum() {
        return FLOAT;
    }

    @Override
    public void toString(StringBuilder sb, ToStringContext ctx) {
        toString(sb, value, true, ctx);
    }

    @Override
    public int bytes() {
        return 5;
    }

    @Override
    public ByteBuffer toBinary(ByteBuffer b) {
        super.toBinary(b);
        b.putDouble(value);
        return b;
    }

    @Override
    public void accept(QVisitor v) {
        v.accept(this);
    }

    public static StringBuilder toString(StringBuilder sb, double value, boolean suffix, ToStringContext ctx) {
        if (Double.isNaN(value)) {
            sb.append("0n");
        } else if (value == NEGATIVE_INFINITY_VALUE) {
            sb.append("-0w");
        } else if (value == POSITIVE_INFINITY_VALUE) {
            sb.append("0w");
        } else {
            if (value >= Long.MIN_VALUE && value <= Long.MAX_VALUE && Math.ceil(value) == value) {
                sb.append(Long.toString((long) value));
                if (suffix) sb.append('f');
            }
            else {
                final String fp = Double.toString(value);
                int eIndex;
                //Need to fix 3.1415927E32 to 3.1415927e+32 or 3.1415927E-32 to 3.1415927e-32.
                if ((eIndex = fp.indexOf('E')) > -1) {
                    sb.append(fp.substring(0, eIndex));
                    sb.append('e');
                    if (Character.isDigit(fp.charAt(eIndex + 1))) {
                        sb.append('+');
                    }
                    sb.append(fp.substring(eIndex+1));
                }
                else {
                    sb.append(fp);
                }
                //ctx.floatingDecimalToString(sb, value);
            }
        }
        return sb;
    }

}
