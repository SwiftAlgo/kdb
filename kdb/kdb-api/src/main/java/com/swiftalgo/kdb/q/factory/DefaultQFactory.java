package com.swiftalgo.kdb.q.factory;

/*-
 * #%L
 * KDB+ API
 * %%
 * Copyright (C) 2017 SwiftAlgo Limited
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.swiftalgo.kdb.q.*;
import com.swiftalgo.util.ByteBufferException;

import java.nio.ByteBuffer;

public class DefaultQFactory implements QFactory {

    @Override
    public QType newType(ByteBuffer buffer) {
        final byte type = buffer.get();
        buffer.position(buffer.position() - 1);
        if (type < QType.ZERO) {
            return newAtom(buffer, type);
        } else {
            if (type < QType.TABLE) {
                return newList(buffer, type);
            } else {
                switch (type) {
                    case QType.TABLE:
                        return new QTable(this, buffer);
                    case QType.DICT:
                        return new QDict(this, buffer);
                    case QType.SORTED_DICT:
                        return new QSortedDict(this, buffer);
                }
            }
        }
        throw new ByteBufferException(String.format("Unrecognized type 0x%02x.", type), buffer);
    }

    public QList newList(ByteBuffer buffer, byte type) {
        switch (type) {
            case QType.MIXED_V:
                return new QMixedV(this, buffer);
            case QType.BOOLEAN_V:
                return new QBooleanV(buffer);
            case QType.GUID_V:
                return new QGuidV(buffer);
            case QType.BYTE_V:
                return new QByteV(buffer);
            case QType.SHORT_V:
                return new QShortV(buffer);
            case QType.INT_V:
                return new QIntV(buffer);
            case QType.LONG_V:
                return new QLongV(buffer);
            case QType.REAL_V:
                return new QRealV(buffer);
            case QType.FLOAT_V:
                return new QFloatV(buffer);
            case QType.CHAR_V:
                return new QCharV(buffer);
            case QType.SYMBOL_V:
                return new QSymbolV(buffer);
            case QType.TIMESTAMP_V:
                return new QTimestampV(buffer);
            case QType.MONTH_V:
                return new QMonthV(buffer);
            case QType.DATE_V:
                return new QDateV(buffer);
            case QType.DATETIME_V:
                return new QDatetimeV(buffer);
            case QType.TIMESPAN_V:
                return new QTimespanV(buffer);
            case QType.MINUTE_V:
                return new QMinuteV(buffer);
            case QType.SECOND_V:
                return new QSecondV(buffer);
            case QType.TIME_V:
                return new QTimeV(buffer);
            default:
                throw new ByteBufferException(String.format("Unrecognized type 0x%02x.", type), buffer);
        }

    }

    public QAtom newAtom(ByteBuffer buffer, byte type) {
        switch (type) {
            case QType.BOOLEAN:
                return new QBoolean(buffer);
            case QType.GUID:
                return new QGuid(buffer);
            case QType.BYTE:
                return new QByte(buffer);
            case QType.SHORT:
                return new QShort(buffer);
            case QType.INT:
                return new QInt(buffer);
            case QType.LONG:
                return new QLong(buffer);
            case QType.REAL:
                return new QReal(buffer);
            case QType.FLOAT:
                return new QFloat(buffer);
            case QType.CHAR:
                return new QChar(buffer);
            case QType.SYMBOL:
                return new QSymbol(buffer);
            case QType.TIMESTAMP:
                return new QTimestamp(buffer);
            case QType.MONTH:
                return new QMonth(buffer);
            case QType.DATE:
                return new QDate(buffer);
            case QType.DATETIME:
                return new QDatetime(buffer);
            case QType.TIMESPAN:
                return new QTimespan(buffer);
            case QType.MINUTE:
                return new QMinute(buffer);
            case QType.SECOND:
                return new QSecond(buffer);
            case QType.TIME:
                return new QTime(buffer);
            default:
                throw new ByteBufferException(String.format("Unrecognized type 0x%02x.", type), buffer);
        }
    }
}
