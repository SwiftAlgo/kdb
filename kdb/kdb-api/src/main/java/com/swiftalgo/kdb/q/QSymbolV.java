package com.swiftalgo.kdb.q;

/*-
 * #%L
 * KDB+ API
 * %%
 * Copyright (C) 2017 SwiftAlgo Limited
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Objects;

public class QSymbolV extends AbstractQList {


    private final byte[] utf8SymbolsNT; //UTF-8 NUL-terminated

    private final String[] symbols; //TODO determine whether needed.

    public QSymbolV(QAttr attr, String[] symbols) {
        super(attr);
        this.symbols = Objects.requireNonNull(symbols);
        byte[][] utf8Arrays = new byte[symbols.length][];

        for (int sIndex = 0; sIndex < symbols.length; ++sIndex) {
            final String s = symbols[sIndex];
            utf8Arrays[sIndex] = s.getBytes(StandardCharsets.UTF_8);
        }
        utf8SymbolsNT = new byte[totalBytes(utf8Arrays) + symbols.length];
        int index = 0;
        for (int i = 0; i < utf8Arrays.length; ++i) {
            System.arraycopy(utf8Arrays[i], 0, utf8SymbolsNT, index, utf8Arrays[i].length);
            index += utf8Arrays[i].length;
            utf8SymbolsNT[index++] = ZERO;
        }
    }

    private QSymbolV(QAttr attr, String[] symbols, byte[] utf8SymbolsNT) {
        super(attr);
        this.symbols = symbols;
        this.utf8SymbolsNT = utf8SymbolsNT;
    }

    @Override
    public QSymbolV clone(QAttr attr) {
        return attr == attr() ? this : new QSymbolV(attr, symbols, utf8SymbolsNT);
    }

    @Override
    public boolean isAtom(int i) {
        return true;
    }

    private int totalBytes(byte[][] byteArrays) {
        int bytes = 0;
        for (int i = 0; i < byteArrays.length; ++i) {
            bytes += byteArrays[i].length;
        }
        return bytes;
    }


    public QSymbolV(ByteBuffer buffer) {
        super(buffer);
        final int initialPos = buffer.position() - 2;
        final int itemCount = buffer.getInt();
        System.out.println("itemCount=" + itemCount);
        symbols = new String[itemCount];
        int foundCount = 0;
        byte v;
        int symbolStartPos = buffer.position();
        while (foundCount < itemCount) {
            v = buffer.get();
            if (v == ZERO) {
                symbols[foundCount] = new String(buffer.array(), symbolStartPos, buffer.position() - symbolStartPos - 1, StandardCharsets.UTF_8);
                ++foundCount;
                symbolStartPos = buffer.position();
            }
        }
        utf8SymbolsNT = Arrays.copyOfRange(buffer.array(), initialPos + 6, buffer.position());
    }

    public QSymbolV(QAttr attr, QSymbolV toCopy) {
        super(attr);
        this.symbols = toCopy.symbols;
        this.utf8SymbolsNT = toCopy.utf8SymbolsNT;
    }

    @Override
    public byte typeNum() {
        return SYMBOL_V;
    }

    @Override
    public void toString(StringBuilder sb, ToStringContext ctx) {
        sb.append(attr().toString());
        if (symbols.length == 1) {
            sb.append(','); //singleton eg. ,`IBM or somewhat trivial `s#`IBM.
        }
        for (int i = 0; i < symbols.length; ++i) {
            sb.append('`').append(symbols[i]);
        }
    }

    @Override
    public void toString(int index, StringBuilder sb, ToStringContext ctx) {
        sb.append('`').append(symbols[index]);
    }

    @Override
    public int bytes() {
        return super.bytes() + utf8SymbolsNT.length;
    }

    @Override
    public ByteBuffer toBinary(ByteBuffer b) {
        super.toBinary(b);
        b.put(utf8SymbolsNT);
        return b;
    }

    @Override
    public void accept(QVisitor v) {
        v.accept(this);
    }

    @Override
    public int count() {
        return symbols.length;
    }
}
