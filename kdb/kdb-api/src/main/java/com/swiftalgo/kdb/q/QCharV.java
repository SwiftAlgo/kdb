package com.swiftalgo.kdb.q;

/*-
 * #%L
 * KDB+ API
 * %%
 * Copyright (C) 2017 SwiftAlgo Limited
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.swiftalgo.util.ByteBufferException;
import sun.nio.cs.ThreadLocalCoders;

import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.CoderResult;
import java.nio.charset.CodingErrorAction;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;

public class QCharV extends AbstractQList {

    private final byte[] utf8;
    private final String value;

    public QCharV(QAttr attr, String value) {
        super(attr);
        this.value = value;
        utf8 = value.getBytes(StandardCharsets.UTF_8);
    }

    public QCharV(ByteBuffer buffer) {
        super(buffer);
        final int initialPos = buffer.position() - 2;
        final int count = buffer.getInt();
        final int startPos = buffer.position();
        final CharBuffer charBuffer = CharBuffer.allocate(count);
        final CoderResult cr = ThreadLocalCoders.decoderFor(StandardCharsets.UTF_8)
                .onMalformedInput(CodingErrorAction.REPORT)
                .onUnmappableCharacter(CodingErrorAction.REPORT)
                .decode(buffer, charBuffer, true);
        if (cr.isError()) {
            throw new ByteBufferException("Failed to decode from UTF-8.", (ByteBuffer) buffer.position(initialPos));
        }
        value = charBuffer.flip().toString();
        utf8 = Arrays.copyOfRange(buffer.array(), startPos, buffer.position());
    }

    public QCharV(QAttr attr, QCharV toCopy) {
        super(attr);
        this.value = toCopy.value;
        this.utf8 = toCopy.utf8;
    }

    private QCharV(QAttr attr, String value, byte[] utf8) {
        super(attr);
        this.value = value;
        this.utf8 = utf8;
    }

    public String getValue() {
        return value;
    }

    @Override
    public QCharV clone(QAttr attr) {
        return attr == attr() ? this : new QCharV(attr, value, utf8);
    }

    @Override
    public boolean isAtom(int i) {
        return true;
    }

    @Override
    public byte typeNum() {
        return CHAR_V;
    }

    @Override
    public void toString(StringBuilder sb, ToStringContext ctx) {
        sb.append(attr().toString());
        if (value.length() == 1) {
            sb.append(","); //singleton list eg. ,"a"
        }
        sb.append("\"");
        sb.append(value);
        sb.append("\"");
    }

    @Override
    public void toString(int index, StringBuilder sb, ToStringContext ctx) {
        sb.append("\"");
        sb.append(value.charAt(index));
        sb.append("\"");
    }

    @Override
    public int bytes() {
        return super.bytes() + utf8.length;
    }

    @Override
    public ByteBuffer toBinary(ByteBuffer b) {
        super.toBinary(b);
        b.put(utf8);
        return b;
    }

    @Override
    public void accept(QVisitor v) {
        v.accept(this);
    }

    @Override
    public int count() {
        return value.length();
    }
}
