package com.swiftalgo.kdb.q;

/*-
 * #%L
 * KDB+ API
 * %%
 * Copyright (C) 2017 SwiftAlgo Limited
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.nio.ByteBuffer;

public class QShort extends QAtom {

    public static final short NULL_VALUE = Short.MIN_VALUE;
    public static final short NEGATIVE_INFINITY_VALUE = Short.MIN_VALUE + 1;
    public static final short POSITIVE_INFINITY_VALUE = Short.MAX_VALUE;

    public final static QShort NULL = new QShort(NULL_VALUE);
    public final static QShort NEGATIVE_INFINITY = new QShort(NEGATIVE_INFINITY_VALUE);
    public final static QShort POSITIVE_INFINITY = new QShort(POSITIVE_INFINITY_VALUE);


    private final short value;

    public QShort(short value) {
        this.value = value;
    }

    public QShort(ByteBuffer buffer) {
        super(buffer);
        this.value = buffer.getShort();
    }

    public short getValue() {
        return value;
    }

    @Override
    public byte typeNum() {
        return SHORT;
    }

    @Override
    public void toString(StringBuilder sb, ToStringContext ctx) {
        toString(sb, value, true);
    }

    @Override
    public int bytes() {
        return 3;
    }

    @Override
    public ByteBuffer toBinary(ByteBuffer b) {
        super.toBinary(b);
        b.putShort(value);
        return b;
    }

    @Override
    public void accept(QVisitor v) {
        v.accept(this);
    }

    public static StringBuilder toString(StringBuilder sb, short value, boolean suffix) {
        switch (value) {
            case NULL_VALUE:
                sb.append("0N");
                break;
            default:
                sb.append(Short.toString(value));
        }
        if (suffix) sb.append('h');
        return sb;
    }

}
