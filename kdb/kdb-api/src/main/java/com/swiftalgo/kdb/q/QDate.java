package com.swiftalgo.kdb.q;

/*-
 * #%L
 * KDB+ API
 * %%
 * Copyright (C) 2017 SwiftAlgo Limited
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.swiftalgo.kdb.q.util.Util;

import java.nio.ByteBuffer;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.format.SignStyle;

import static java.time.temporal.ChronoField.*;

public class QDate extends QAtom {

    public static final int NULL_VALUE = Integer.MIN_VALUE;
    public static final int NEGATIVE_INFINITY_VALUE = Integer.MIN_VALUE + 1;
    public static final int POSITIVE_INFINITY_VALUE = Integer.MAX_VALUE;

    public final static QDate NULL = new QDate(NULL_VALUE);
    public final static QDate NEGATIVE_INFINITY = new QDate(NEGATIVE_INFINITY_VALUE);
    public final static QDate POSITIVE_INFINITY = new QDate(POSITIVE_INFINITY_VALUE);

    public static final DateTimeFormatter Q_FORMATTER = new DateTimeFormatterBuilder()
            .appendValue(YEAR, 4, 10, SignStyle.EXCEEDS_PAD)
            .appendLiteral('.')
            .appendValue(MONTH_OF_YEAR, 2)
            .appendLiteral('.')
            .appendValue(DAY_OF_MONTH, 2)
            .toFormatter();

    private final int value;

    public QDate(int value) {
        this.value = value;
    }

    public QDate(ByteBuffer buffer) {
        super(buffer);
        this.value = buffer.getInt();
    }

    public int getValue() {
        return value;
    }

    @Override
    public byte typeNum() {
        return DATE;
    }

    @Override
    public void toString(StringBuilder sb, ToStringContext ctx) {
        toString(sb, value, true);
    }

    @Override
    public int bytes() {
        return 5;
    }

    @Override
    public ByteBuffer toBinary(ByteBuffer b) {
        super.toBinary(b);
        b.putInt(value);
        return b;
    }

    @Override
    public void accept(QVisitor v) {
        v.accept(this);
    }

    public static StringBuilder toString(StringBuilder sb, int value, boolean suffix) {
        switch (value) {
            case NULL_VALUE:
                sb.append("0N");
                if (suffix) sb.append('d');
                break;
            case NEGATIVE_INFINITY_VALUE:
                sb.append("-");
            case POSITIVE_INFINITY_VALUE:
                sb.append("0W");
                if (suffix) sb.append('d');
                break;
            default:
                Q_FORMATTER.formatTo(LocalDate.ofEpochDay(value + Util.Q_TO_JAVA_EPOCH_DAYS), sb);
        }
        return sb;
    }

    public static QDate create(int value) {
        switch (value) {
            case NULL_VALUE:
                return NULL;
            case NEGATIVE_INFINITY_VALUE:
                return NEGATIVE_INFINITY;
            case POSITIVE_INFINITY_VALUE:
                return POSITIVE_INFINITY;
            default:
                return new QDate(value);
        }
    }

}
