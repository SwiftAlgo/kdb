package com.swiftalgo.kdb.q;

/*-
 * #%L
 * KDB+ API
 * %%
 * Copyright (C) 2017 SwiftAlgo Limited
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.swiftalgo.kdb.q.factory.QFactory;

import java.nio.ByteBuffer;
import java.util.Objects;

public class QTable extends AbstractQComp implements QType {

    private QDict dict;

    public QTable(QAttr attr, QDict dict) {
        super(attr);
        this.dict = Objects.requireNonNull(dict);
        if (!dict.isRectangular()) {
            throw new IllegalStateException("'length");
        }
    }

    public QTable(QFactory factory, ByteBuffer b) {
        super(b);
        dict = Objects.requireNonNull((QDict) factory.newType(b));
        if (!dict.isRectangular()) {
            throw new IllegalStateException("'length");
        }
    }

    public boolean isAtom() {
        return false;
    }

    @Override
    public boolean isList() {
        return false;
    }

    public QDict getValue() {
        return dict;
    }

    @Override
    public void accept(QVisitor v) {
        v.accept(this);
    }

    @Override
    public byte typeNum() {
        return TABLE;
    }

    @Override
    public void toString(StringBuilder sb, ToStringContext ctx) {
        super.toString(sb, ctx);
        sb.append("+");
        dict.toString(sb, ctx);
    }

    @Override
    public int bytes() {
        return super.bytes() + dict.bytes();
    }

    @Override
    public ByteBuffer toBinary(ByteBuffer b) {
        super.toBinary(b);
        dict.toBinary(b);
        return b;
    }
}
