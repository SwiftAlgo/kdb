package com.swiftalgo.kdb.q.factory;

/*-
 * #%L
 * KDB+ API
 * %%
 * Copyright (C) 2017 SwiftAlgo Limited
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.swiftalgo.kdb.q.QAtom;
import com.swiftalgo.kdb.q.QList;
import com.swiftalgo.kdb.q.QType;

import java.nio.ByteBuffer;

public interface QFactory {

    QType newType(ByteBuffer buffer);

    QAtom newAtom(ByteBuffer buffer, byte type);

    QList newList(ByteBuffer buffer, byte type);
}
