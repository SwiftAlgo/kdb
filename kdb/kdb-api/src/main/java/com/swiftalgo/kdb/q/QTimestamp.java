package com.swiftalgo.kdb.q;

/*-
 * #%L
 * KDB+ API
 * %%
 * Copyright (C) 2017 SwiftAlgo Limited
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.swiftalgo.kdb.q.util.Util;
import com.swiftalgo.util.DataUtil;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.format.SignStyle;
import java.util.concurrent.TimeUnit;

import static java.time.temporal.ChronoField.*;

public class QTimestamp extends QAtom {

    public static final long NULL_VALUE = Long.MIN_VALUE;
    public static final long NEGATIVE_INFINITY_VALUE = Long.MIN_VALUE + 1;
    public static final long POSITIVE_INFINITY_VALUE = Long.MAX_VALUE;

    public final static QTimestamp NULL = new QTimestamp(NULL_VALUE);
    public final static QTimestamp NEGATIVE_INFINITY = new QTimestamp(NEGATIVE_INFINITY_VALUE);
    public final static QTimestamp POSITIVE_INFINITY = new QTimestamp(POSITIVE_INFINITY_VALUE);

    public static final DateTimeFormatter Q_FORMATTER = new DateTimeFormatterBuilder()
            .appendValue(YEAR, 4, 10, SignStyle.EXCEEDS_PAD)
            .appendLiteral('.')
            .appendValue(MONTH_OF_YEAR, 2)
            .appendLiteral('.')
            .appendValue(DAY_OF_MONTH, 2)
            .appendLiteral('D')
            .appendValue(HOUR_OF_DAY, 2)
            .appendLiteral(':')
            .appendValue(MINUTE_OF_HOUR, 2)
            .appendLiteral(':')
            .appendValue(SECOND_OF_MINUTE, 2)
            .appendLiteral('.')
            .appendValue(NANO_OF_SECOND, 9)
            .toFormatter();


    private final long value;

    public QTimestamp(long value) {
        this.value = value;
        ByteBuffer b = (ByteBuffer) toBinary(ByteBuffer.allocate(256).order(ByteOrder.LITTLE_ENDIAN)).flip();
        System.out.println(DataUtil.byteBufferToHex(b, new StringBuilder()));
    }

    public QTimestamp(QDate date, QTimespan timespan) {
        this.value = TimeUnit.DAYS.toNanos(date.getValue()) + timespan.getValue();
    }

    public QTimestamp(ByteBuffer buffer) {
        super(buffer);
        this.value = buffer.getLong();
    }

    public long getValue() {
        return value;
    }

    @Override
    public byte typeNum() {
        return TIMESTAMP;
    }

    @Override
    public void toString(StringBuilder sb, ToStringContext ctx) {
        toString(sb, value, true);
    }

    @Override
    public int bytes() {
        return 9;
    }

    @Override
    public ByteBuffer toBinary(ByteBuffer b) {
        super.toBinary(b);
        b.putLong(value);
        return b;
    }

    @Override
    public void accept(QVisitor v) {
        v.accept(this);
    }

    public static StringBuilder toString(StringBuilder sb, long value, boolean suffix) {
        if (value == NULL_VALUE) {
            sb.append("0N");
            if (suffix) sb.append('p');
        } else if (value == NEGATIVE_INFINITY_VALUE) {
            sb.append("-0W");
            if (suffix) sb.append('p');
        } else if (value == POSITIVE_INFINITY_VALUE) {
            sb.append("0W");
            if (suffix) sb.append('p');
        } else {
            final long seconds = Math.floorDiv(value, Util.NANOS_PER_SECOND);
            final int nanos = StrictMath.toIntExact(value - seconds * Util.NANOS_PER_SECOND);

            final LocalDateTime ldt = LocalDateTime.ofEpochSecond(seconds + Util.Q_TO_JAVA_EPOCH_SECONDS, nanos, ZoneOffset.UTC);
            Q_FORMATTER.formatTo(ldt, sb);
        }
        return sb;
    }

    public static QTimestamp create(long value) {
        if (value == NULL_VALUE) {
            return NULL;
        } else if (value == NEGATIVE_INFINITY_VALUE) {
            return NEGATIVE_INFINITY;
        } else if (value == POSITIVE_INFINITY_VALUE) {
            return POSITIVE_INFINITY;
        } else {
            return new QTimestamp(value);
        }
    }

}
