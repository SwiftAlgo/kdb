package com.swiftalgo.kdb.q;

/*-
 * #%L
 * KDB+ API
 * %%
 * Copyright (C) 2017 SwiftAlgo Limited
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.swiftalgo.kdb.q.factory.QFactory;

import java.nio.ByteBuffer;

public class QMixedV extends AbstractQList {

    private final QType[] items;

    public QMixedV(QFactory factory, ByteBuffer b) {
        super(b);
        //final int initialPos = buffer.position() - 2;
        int itemCount = b.getInt();
        this.items = new QType[itemCount];
        for (int i = 0; i < itemCount; ++i) {
            this.items[i] = factory.newType(b);
        }
    }

    public QMixedV(QAttr attr, QType[] items) {
        super(attr);
        this.items = items;
    }

    public QType get(int i) {
        return items[i];
    }

    @Override
    public QMixedV clone(QAttr attr) {
        return attr == attr() ? this : new QMixedV(attr, items);
    }

    @Override
    public boolean isAtom(int i) {
        return items[i].isAtom();
    }

    @Override
    public void accept(QVisitor v) {
        v.accept(this);
    }

    @Override
    public byte typeNum() {
        return MIXED_V;
    }

    @Override
    public void toString(StringBuilder sb, ToStringContext ctx) {
        final boolean inner = true;//ctx.isInner();
        if (inner) {
            sb.append('(');
        }
        final ToStringContext innerCtx = ctx.setInner(true);
        for (int i = 0; i < items.length; ++i) {
            items[i].toString(sb, innerCtx);
            if (i < items.length - 1) {
                if (inner) {
                    sb.append(';');
                } else {
                    sb.append('\n');
                }
            }
        }
        if (inner) {
            sb.append(')');
        }
    }

    @Override
    public void toString(int index, StringBuilder sb, ToStringContext ctx) {
        items[index].toString(sb, ctx);
    }

    @Override
    public int bytes() {
        int bytes = super.bytes();
        for (int i = 0; i < items.length; ++i) {
            bytes += items[i].bytes();
        }
        return bytes;
    }

    @Override
    public ByteBuffer toBinary(ByteBuffer b) {
        super.toBinary(b);
        for (int i = 0; i < items.length; ++i) {
            items[i].toBinary(b);
        }
        return b;
    }

    @Override
    public int count() {
        return items.length;
    }
}
