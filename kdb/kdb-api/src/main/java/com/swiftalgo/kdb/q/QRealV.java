package com.swiftalgo.kdb.q;

/*-
 * #%L
 * KDB+ API
 * %%
 * Copyright (C) 2017 SwiftAlgo Limited
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.nio.ByteBuffer;

public class QRealV extends AbstractQList {

    private final float[] values;

    public QRealV(QAttr attr, float[] values) {
        super(attr);
        this.values = values;
    }

    public QRealV(ByteBuffer buffer) {
        super(buffer);
        final int count = buffer.getInt();
        values = new float[count];
        for (int i = 0; i < count; ++i) {
            values[i] = buffer.getFloat();
        }
    }

    public QRealV(QAttr attr, QRealV toCopy) {
        super(attr);
        this.values = toCopy.values;
    }

    @Override
    public QRealV clone(QAttr attr) {
        return attr == attr() ? this : new QRealV(attr, values);
    }

    @Override
    public boolean isAtom(int i) {
        return true;
    }

    @Override
    public byte typeNum() {
        return REAL_V;
    }

    @Override
    public void toString(StringBuilder sb, ToStringContext ctx) {
        sb.append(attr().toString());
        if (values.length == 1) {
            sb.append(','); //singleton list eg. ,1e
        }
        int i = 0;
        for (; i < values.length - 1; ++i) {
            QReal.toString(sb, values[i], false, ctx);
            sb.append(' ');
        }
        QReal.toString(sb, values[i], true, ctx);
    }

    @Override
    public void toString(int index, StringBuilder buffer, ToStringContext ctx) {
        QReal.toString(buffer, values[index], true, ctx);
    }

    @Override
    public int bytes() {
        return super.bytes() + (values.length << 2); //x4 bytes per value
    }

    @Override
    public ByteBuffer toBinary(ByteBuffer b) {
        super.toBinary(b);
        for (int i = 0; i < values.length; ++i) {
            b.putFloat(values[i]);
        }
        return b;
    }

    @Override
    public void accept(QVisitor v) {
        v.accept(this);
    }

    @Override
    public int count() {
        return values.length;
    }
}
