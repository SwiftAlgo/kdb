package com.swiftalgo.kdb.q;

/*-
 * #%L
 * KDB+ API
 * %%
 * Copyright (C) 2017 SwiftAlgo Limited
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.nio.ByteBuffer;

public interface QType {
    void accept(QVisitor v);

    byte typeNum();

    byte wireTypeNum();

    void toString(StringBuilder sb, ToStringContext ctx);

    String toString(ToStringContext ctx);

    ByteBuffer toBinary(ByteBuffer b);

    int bytes();

    boolean isAtom();

    boolean isList();

    static final byte ZERO = 0b0;
    static final byte ONE = 0b1;
    static final String NULL_SYMBOL = "`";

    static final byte MIXED_V = 0;
    static final byte BOOLEAN_V = 1;
    static final byte GUID_V = 2;
    static final byte BYTE_V = 4;
    static final byte SHORT_V = 5;
    static final byte INT_V = 6;
    static final byte LONG_V = 7;
    static final byte REAL_V = 8;
    static final byte FLOAT_V = 9;
    static final byte CHAR_V = 10;
    static final byte SYMBOL_V = 11;
    static final byte TIMESTAMP_V = 12;
    static final byte MONTH_V = 13;
    static final byte DATE_V = 14;
    static final byte DATETIME_V = 15;
    static final byte TIMESPAN_V = 16;
    static final byte MINUTE_V = 17;
    static final byte SECOND_V = 18;
    static final byte TIME_V = 19;
    static final byte NESTED = 77;

    static final byte TABLE = 98;
    static final byte DICT = 99;
    static final byte SORTED_DICT = 127;

    static final byte BOOLEAN = -BOOLEAN_V;
    static final byte GUID = -GUID_V;
    static final byte BYTE = -BYTE_V;
    static final byte SHORT = -SHORT_V;
    static final byte INT = -INT_V;
    static final byte LONG = -LONG_V;
    static final byte REAL = -REAL_V;
    static final byte FLOAT = -FLOAT_V;
    static final byte CHAR = -CHAR_V;
    static final byte SYMBOL = -SYMBOL_V;
    static final byte TIMESTAMP = -TIMESTAMP_V;
    static final byte MONTH = -MONTH_V;
    static final byte DATE = -DATE_V;
    static final byte DATETIME = -DATETIME_V;
    static final byte TIMESPAN = -TIMESPAN_V;
    static final byte MINUTE = -MINUTE_V;
    static final byte SECOND = -SECOND_V;
    static final byte TIME = -TIME_V;

}
