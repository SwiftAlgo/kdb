package com.swiftalgo.kdb.q;

/*-
 * #%L
 * KDB+ API
 * %%
 * Copyright (C) 2017 SwiftAlgo Limited
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.swiftalgo.util.DataUtil;

import java.nio.ByteBuffer;
import java.util.Arrays;

public class QGuid extends QAtom {

    private final byte[] bytes;

    public QGuid(String text) {
        bytes = fromString(text);
    }

    public QGuid(byte[] bytes) {
        if (bytes.length != 16) {
            throw new IllegalArgumentException(String.format("Expected 16 bytes, was %d.", bytes.length));
        }
        this.bytes = bytes;
    }

    public QGuid(ByteBuffer b) {
        super(b);
        bytes = Arrays.copyOfRange(b.array(), b.position(), b.position() + 16);
    }

    public byte[] getValue() {
        return bytes;
    }

    @Override
    public byte typeNum() {
        return GUID;
    }

    @Override
    public void toString(StringBuilder sb, ToStringContext ctx) {
        toString(bytes, 0, sb);
    }

    @Override
    public int bytes() {
        return super.bytes() + bytes.length;
    }

    @Override
    public ByteBuffer toBinary(ByteBuffer b) {
        super.toBinary(b);
        b.put(bytes);
        return b;
    }

    @Override
    public void accept(QVisitor v) {
        v.accept(this);
    }

    public static void toString(byte[] bytes, int beginIndex, StringBuilder b) {
        DataUtil.byteArrayToHex(bytes, beginIndex, beginIndex += 4, b);
        b.append("-");
        DataUtil.byteArrayToHex(bytes, beginIndex, beginIndex += 2, b);
        b.append("-");
        DataUtil.byteArrayToHex(bytes, beginIndex, beginIndex += 2, b);
        b.append("-");
        DataUtil.byteArrayToHex(bytes, beginIndex, beginIndex += 2, b);
        b.append("-");
        DataUtil.byteArrayToHex(bytes, beginIndex, beginIndex += 6, b);
    }

    public static byte[] fromString(String text) {
        if (text.length() != 36 || text.charAt(8) != '-' || text.charAt(13) != '-' || text.charAt(18) != '-' || text.charAt(23) != '-') {
            throw new IllegalArgumentException();
        }
        final byte[] target = new byte[16];
        DataUtil.hexStringToByteArray(text.substring(0, 8), target, 0);
        DataUtil.hexStringToByteArray(text.substring(9, 13), target, 4);
        DataUtil.hexStringToByteArray(text.substring(14, 18), target, 6);
        DataUtil.hexStringToByteArray(text.substring(19, 23), target, 8);
        DataUtil.hexStringToByteArray(text.substring(24, 36), target, 10);
        return target;
    }
}
