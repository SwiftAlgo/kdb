package com.swiftalgo.kdb.q;

/*-
 * #%L
 * KDB+ API
 * %%
 * Copyright (C) 2017 SwiftAlgo Limited
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.HashMap;
import java.util.Map;

public enum QAttr {

    NONE(QAttr.NONE_BYTE, ""), SORTED(QAttr.SORTED_BYTE, "`s#"), U(QAttr.U_BYTE, "`u#"), P(QAttr.P_BYTE, "`p#"), G(QAttr.G_BYTE, "`g#");

    public static final byte NONE_BYTE = 0;
    public static final byte SORTED_BYTE = 1;
    public static final byte U_BYTE = 2;
    public static final byte P_BYTE = 3;
    public static final byte G_BYTE = 4;

    private byte wireValue;
    private String text;

    private QAttr(byte wireValue, String text) {
        this.wireValue = wireValue;
        this.text = text;
    }

    public byte getWireValue() {
        return wireValue;
    }

    public String toString() {
        return text;
    }

    private static final Map<String, QAttr> TEXT_INDEX = new HashMap<>(6);

    static {
        TEXT_INDEX.put(NONE.text, NONE);
        TEXT_INDEX.put(QType.NULL_SYMBOL, NONE);
        TEXT_INDEX.put(SORTED.text, SORTED);
        TEXT_INDEX.put(U.text, U);
        TEXT_INDEX.put(P.text, P);
        TEXT_INDEX.put(G.text, G);
    }

    public static QAttr get(String q) {
        final QAttr result = TEXT_INDEX.get(q);
        if (result == null) {
            throw new IllegalStateException(String.format("Unrecognised attribute '%s'", q));
        } else {
            return result;
        }
    }

    public static QAttr get(byte wire) {
        if (wire >= NONE_BYTE && wire <= G_BYTE)
            return values()[wire];
        else
            throw new IllegalStateException(String.format("Unexpected attribute type 0x%h.", wire));
    }

}
