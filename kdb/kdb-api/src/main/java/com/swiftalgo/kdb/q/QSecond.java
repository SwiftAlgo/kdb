package com.swiftalgo.kdb.q;

/*-
 * #%L
 * KDB+ API
 * %%
 * Copyright (C) 2017 SwiftAlgo Limited
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.nio.ByteBuffer;

public class QSecond extends QAtom {

    public static final int NULL_VALUE = Integer.MIN_VALUE;
    public static final int NEGATIVE_INFINITY_VALUE = Integer.MIN_VALUE + 1;
    public static final int POSITIVE_INFINITY_VALUE = Integer.MAX_VALUE;

    public final static QSecond NULL = new QSecond(NULL_VALUE);
    public final static QSecond NEGATIVE_INFINITY = new QSecond(NEGATIVE_INFINITY_VALUE);
    public final static QSecond POSITIVE_INFINITY = new QSecond(POSITIVE_INFINITY_VALUE);

    private final int value;

    public QSecond(int value) {
        this.value = value;
    }

    public QSecond(ByteBuffer buffer) {
        super(buffer);
        this.value = buffer.getInt();
    }

    public int getValue() {
        return value;
    }

    @Override
    public byte typeNum() {
        return SECOND;
    }

    @Override
    public void toString(StringBuilder sb, ToStringContext ctx) {
        toString(sb, value, true);
    }

    @Override
    public int bytes() {
        return 5;
    }

    @Override
    public ByteBuffer toBinary(ByteBuffer b) {
        super.toBinary(b);
        b.putInt(value);
        return b;
    }

    @Override
    public void accept(QVisitor v) {
        v.accept(this);
    }

    public static StringBuilder toString(StringBuilder sb, int value, boolean suffix) {
        switch (value) {
            case NULL_VALUE:
                sb.append("0N");
                if (suffix) sb.append('v');
                break;
            case NEGATIVE_INFINITY_VALUE:
                sb.append('-');
            case POSITIVE_INFINITY_VALUE:
                sb.append("0W");
                if (suffix) sb.append('v');
                break;
            default:
                if (value < 0) {
                    sb.append('-');
                }
                final int absValue = Math.abs(value);
                final int hours = absValue / (60 * 60);
                final int minutes = (absValue / 60) % 60;
                final int seconds = absValue % 60;
                sb.append(String.format("%02d:%02d:%02d", hours, minutes, seconds));
        }
        return sb;
    }

    public static QSecond create(int value) {
        switch (value) {
            case NULL_VALUE:
                return NULL;
            case NEGATIVE_INFINITY_VALUE:
                return NEGATIVE_INFINITY;
            case POSITIVE_INFINITY_VALUE:
                return POSITIVE_INFINITY;
            default:
                return new QSecond(value);
        }
    }
}
