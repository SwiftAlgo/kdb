package com.swiftalgo.kdb.q.util;

/*-
 * #%L
 * KDB+ API
 * %%
 * Copyright (C) 2017 SwiftAlgo Limited
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.time.LocalDate;
import java.util.concurrent.TimeUnit;

public class Util {
    /**
     * Number of days in the Q epoch at which the Java epoch starts ie. days from 1970 to 2000. Note that there are 7 leap years from 1970 to 2000.
     */
    public static final int Q_TO_JAVA_EPOCH_DAYS = 30 * 365 + 7; //==10957
    public static final int SECONDS_PER_DAY = 86400;
    public static final int MILLIS_PER_DAY = SECONDS_PER_DAY * 1000;
    public static final int Q_TO_JAVA_EPOCH_SECONDS = Q_TO_JAVA_EPOCH_DAYS * SECONDS_PER_DAY; //10957 * 86400
    public static final int Q_EPOCH_YEAR = 2000;
    public static final int SECONDS_PER_MINUTE = 60;
    public static final int MILLIS_PER_SECOND = 1000;
    public static final int NANOS_PER_MILLI = 1000_000;
    public static final int NANOS_PER_SECOND = 1000_000_000;

    public static final int MILLIS_DIGITS = 3;
    public static final int NANOS_DIGITS = 9;

    public static final int toQEpochDays(int year, int month, int day) {
        return (int) LocalDate.of(year, month, day).toEpochDay() - Q_TO_JAVA_EPOCH_DAYS;
    }

    public static final int toQEpochMonths(int year, int month) {
        if (year < 1709 || month < 1 || month > 12) {
            throw new IllegalArgumentException();
        }
        return (year - Q_EPOCH_YEAR) * 12 + (month - 1);
    }

    public static final long toNanos(int hour, int minute, int seconds, int nanos) {
        return TimeUnit.HOURS.toNanos(hour) + TimeUnit.MINUTES.toNanos(minute) + TimeUnit.SECONDS.toNanos(seconds) + nanos;
    }

    public static final long toMillis(int hour, int minute, int seconds, int millis) {
        return TimeUnit.HOURS.toMillis(hour) + TimeUnit.MINUTES.toMillis(minute) + TimeUnit.SECONDS.toMillis(seconds) + millis;
    }

    public static String truncate(String x, int maxLength) {
        if (x == null || x.length() <= maxLength) {
            return x;
        } else {
            return x.substring(0, maxLength);
        }
    }

}
