package com.swiftalgo.kdb.q;

/*-
 * #%L
 * KDB+ API
 * %%
 * Copyright (C) 2017 SwiftAlgo Limited
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.swiftalgo.util.ByteBufferException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sun.nio.cs.ThreadLocalCoders;

import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.CharsetEncoder;
import java.nio.charset.CoderResult;
import java.nio.charset.CodingErrorAction;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;

public class QChar extends QAtom {

    private static final Logger logger = LoggerFactory.getLogger(QChar.class);

    private final byte[] utf8;
    private final char value;

    public QChar(char value) {
        this.value = value;
        if (value > Character.MIN_VALUE) {
            final CharBuffer charBuffer = CharBuffer.allocate(1).append(value);
            charBuffer.flip();
            //utf8 = charBuffer.toString().getBytes(StandardCharsets.UTF_8);

            final CharsetEncoder encoder = ThreadLocalCoders.encoderFor(StandardCharsets.UTF_8);
            final ByteBuffer buffer = ByteBuffer.allocate((int) encoder.maxBytesPerChar());
            //logger.info("Created buffer with capacity {}", buffer.capacity());
            final CoderResult result = encoder.onMalformedInput(CodingErrorAction.REPORT)
                    .onUnmappableCharacter(CodingErrorAction.REPORT)
                    .encode(charBuffer, buffer, true);
            if (result.isError()) {
                throw new IllegalStateException(String.format("Failed to encode '%s' to UTF-8.", charBuffer.position(0).toString()));
            }
            //logger.info("{} bytes encoded to UTF-8", buffer.position());
            utf8 = buffer.hasRemaining() ? Arrays.copyOfRange(buffer.array(), 0, buffer.position()) : buffer.array();
        } else {
            utf8 = new byte[0];
        }
        logger.info("Buffer length={} bytes", utf8.length);
    }

    public QChar(ByteBuffer b) {
        super(b);
        final int startPos = b.position();
        final CharBuffer charBuffer = CharBuffer.allocate(1);
        final CoderResult result = ThreadLocalCoders.decoderFor(StandardCharsets.UTF_8)
                .onMalformedInput(CodingErrorAction.REPORT)
                .onUnmappableCharacter(CodingErrorAction.REPORT)
                .decode(b, charBuffer, true);
        charBuffer.flip();
        if (result.isError() || !charBuffer.hasRemaining()) {
            throw new ByteBufferException("Failed to decode from UTF-8.", (ByteBuffer) b.position(startPos - 1));
        }
        value = charBuffer.charAt(0);
        utf8 = Arrays.copyOfRange(b.array(), startPos, b.position());
    }

    public char getValue() {
        return value;
    }

    @Override
    public byte typeNum() {
        return CHAR;
    }

    @Override
    public void toString(StringBuilder sb, ToStringContext ctx) {
        sb.append("\"");
        if (value > Character.MIN_VALUE) {
            sb.append(value);
        }
        sb.append("\"");
    }

    @Override
    public int bytes() {
        return 1 + utf8.length;
    }

    @Override
    public ByteBuffer toBinary(ByteBuffer b) {
        super.toBinary(b);
        b.put(utf8);
        return b;
    }

    @Override
    public void accept(QVisitor v) {
        v.accept(this);
    }

}
