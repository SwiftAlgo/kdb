package com.swiftalgo.kdb.q;

/*-
 * #%L
 * KDB+ API
 * %%
 * Copyright (C) 2017 SwiftAlgo Limited
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.swiftalgo.util.ByteBufferException;

import java.nio.ByteBuffer;

public abstract class AbstractQType implements QType {

    public AbstractQType() {
    }

    public AbstractQType(ByteBuffer buffer) {
        checkWireType(buffer);
    }

    public byte wireTypeNum() {
        return typeNum();
    }

    protected void checkWireType(ByteBuffer buffer) {
        byte type;
        if ((type = buffer.get()) != wireTypeNum()) {
            throw new ByteBufferException(String.format("Unexpected type 0x%02x.", type), (ByteBuffer) buffer.position(buffer.position() - 1));
        }
    }

    @Override
    public int bytes() {
        return 1;
    }

    @Override
    public ByteBuffer toBinary(ByteBuffer b) {
        b.put(wireTypeNum());
        return b;
    }

    @Override
    public String toString(ToStringContext ctx) {
        StringBuilder sb = new StringBuilder();
        toString(sb, ctx);
        return sb.toString();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        toString(sb, new ToStringContext(false, 0));
        return sb.toString();
    }
}
