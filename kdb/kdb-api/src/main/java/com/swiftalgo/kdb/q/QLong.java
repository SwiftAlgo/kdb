package com.swiftalgo.kdb.q;

/*-
 * #%L
 * KDB+ API
 * %%
 * Copyright (C) 2017 SwiftAlgo Limited
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.nio.ByteBuffer;

public class QLong extends QAtom {

    public static final long NULL_VALUE = Long.MIN_VALUE;
    public static final long NEGATIVE_INFINITY_VALUE = Long.MIN_VALUE + 1;
    public static final long POSITIVE_INFINITY_VALUE = Long.MAX_VALUE;

    public final static QLong NULL = new QLong(NULL_VALUE);
    public final static QLong NEGATIVE_INFINITY = new QLong(NEGATIVE_INFINITY_VALUE);
    public final static QLong POSITIVE_INFINITY = new QLong(POSITIVE_INFINITY_VALUE);

    private final long value;

    public QLong(long value) {
        this.value = value;
    }

    public QLong(ByteBuffer buffer) {
        super(buffer);
        this.value = buffer.getLong();
    }

    public long getValue() {
        return value;
    }

    @Override
    public byte typeNum() {
        return LONG;
    }

    @Override
    public void toString(StringBuilder sb, ToStringContext ctx) {
        toString(sb, value);
    }

    @Override
    public int bytes() {
        return 5;
    }

    @Override
    public ByteBuffer toBinary(ByteBuffer b) {
        super.toBinary(b);
        b.putLong(value);
        return b;
    }

    @Override
    public void accept(QVisitor v) {
        v.accept(this);
    }

    public static StringBuilder toString(StringBuilder buffer, long value) {
        if (value == NULL_VALUE) {
            buffer.append("0N");
        } else if (value == NEGATIVE_INFINITY_VALUE) {
            buffer.append("-0W");
        } else if (value == POSITIVE_INFINITY_VALUE) {
            buffer.append("0W");
        } else {
            buffer.append(Long.toString(value));
        }
        return buffer;
    }

}
