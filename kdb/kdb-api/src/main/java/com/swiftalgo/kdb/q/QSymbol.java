package com.swiftalgo.kdb.q;

/*-
 * #%L
 * KDB+ API
 * %%
 * Copyright (C) 2017 SwiftAlgo Limited
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;

public class QSymbol extends QAtom {

    private final byte[] utf8;
    private final String symbol;

    public QSymbol(String symbol) {
        this.symbol = symbol;
        if (symbol == null) {
            utf8 = new byte[0];
        } else {
            utf8 = symbol.getBytes(StandardCharsets.UTF_8);
        }
    }

    public QSymbol(ByteBuffer buffer) {
        super(buffer);
        final int startPos = buffer.position();
        while (buffer.get() != ZERO) ;
        int byteLength = buffer.position() - startPos - 1;
        symbol = new String(buffer.array(), startPos, byteLength, StandardCharsets.UTF_8);
        utf8 = Arrays.copyOfRange(buffer.array(), startPos, buffer.position() - 1); //ignore terminating 0x00 byte.
    }

    public String getValue() {
        return symbol;
    }

    @Override
    public byte typeNum() {
        return SYMBOL;
    }

    @Override
    public void toString(StringBuilder sb, ToStringContext ctx) {
        sb.append('`');
        sb.append(symbol);
    }

    @Override
    public int bytes() {
        return 2 + utf8.length;
    } //type, utf8, ZERO (ie. null terminator).

    @Override
    public ByteBuffer toBinary(ByteBuffer b) {
        super.toBinary(b);
        b.put(utf8);
        b.put(ZERO);
        return b;
    }

    @Override
    public void accept(QVisitor v) {
        v.accept(this);
    }


}
