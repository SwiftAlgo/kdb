package com.swiftalgo.kdb.q;

/*-
 * #%L
 * KDB+ API
 * %%
 * Copyright (C) 2017 SwiftAlgo Limited
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.nio.ByteBuffer;
import java.util.Objects;

public abstract class AbstractQComp extends AbstractQType implements QComp {

    private QAttr attr;

    public AbstractQComp(QAttr attr) {
        this.attr = Objects.requireNonNull(attr);
    }

    public AbstractQComp(ByteBuffer buffer) {
        super(buffer);
        attr = QAttr.get(buffer.get());
    }

    @Override
    public QAttr attr() {
        return attr;
    }

    @Override
    public void toString(StringBuilder sb, ToStringContext ctx) {
        sb.append(attr().toString());
    }

    @Override
    public int bytes() {
        return 2;
    }

    @Override
    public ByteBuffer toBinary(ByteBuffer b) {
        super.toBinary(b);
        b.put(attr.getWireValue());
        return b;
    }

}
