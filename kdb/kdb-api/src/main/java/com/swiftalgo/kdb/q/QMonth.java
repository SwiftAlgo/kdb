package com.swiftalgo.kdb.q;

/*-
 * #%L
 * KDB+ API
 * %%
 * Copyright (C) 2017 SwiftAlgo Limited
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.swiftalgo.kdb.q.util.Util;

import java.nio.ByteBuffer;

public class QMonth extends QAtom {

    public static final int NULL_VALUE = Integer.MIN_VALUE;
    public static final int NEGATIVE_INFINITY_VALUE = Integer.MIN_VALUE + 1;
    public static final int POSITIVE_INFINITY_VALUE = Integer.MAX_VALUE;

    public final static QMonth NULL = new QMonth(NULL_VALUE);
    public final static QMonth NEGATIVE_INFINITY = new QMonth(NEGATIVE_INFINITY_VALUE);
    public final static QMonth POSITIVE_INFINITY = new QMonth(POSITIVE_INFINITY_VALUE);

    private final int value;

    public QMonth(int value) {
        this.value = value;
    }

    /*
    public QMonth(int year, int month) {
        if (year < 1709) {
            throw new IllegalArgumentException();
        }
        this.value = (year - Q_EPOCH_YEAR) * 12 + (month - 1);
    }
    */

    public QMonth(ByteBuffer b) {
        super(b);
        this.value = b.getInt();
    }

    public int getValue() {
        return value;
    }

    @Override
    public byte typeNum() {
        return MONTH;
    }

    @Override
    public void toString(StringBuilder sb, ToStringContext ctx) {
        toString(sb, value, true);
    }

    @Override
    public int bytes() {
        return 5;
    }

    @Override
    public ByteBuffer toBinary(ByteBuffer b) {
        super.toBinary(b);
        b.putInt(value);
        return b;
    }

    @Override
    public void accept(QVisitor v) {
        v.accept(this);
    }

    public static StringBuilder toString(StringBuilder sb, int value, boolean suffix) {
        switch (value) {
            case NULL_VALUE:
                sb.append("0N");
                break;
            case NEGATIVE_INFINITY_VALUE:
                sb.append('-');
            case POSITIVE_INFINITY_VALUE:
                sb.append("0W");
                break;
            default:
                final int rawYear = Math.floorDiv(value, 12);
                final int month = value - rawYear * 12;
                sb.append(String.format("%04d.%02d", rawYear + Util.Q_EPOCH_YEAR, month + 1));
        }
        if (suffix) sb.append('m');
        return sb;
    }

    public static QMonth create(int value) {
        switch (value) {
            case NULL_VALUE:
                return NULL;
            case NEGATIVE_INFINITY_VALUE:
                return NEGATIVE_INFINITY;
            case POSITIVE_INFINITY_VALUE:
                return POSITIVE_INFINITY;
            default:
                return new QMonth(value);
        }
    }

}
