package com.swiftalgo.kdb.q;

/*-
 * #%L
 * KDB+ API
 * %%
 * Copyright (C) 2017 SwiftAlgo Limited
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.commons.lang3.StringUtils;

import java.text.DecimalFormat;

public class ToStringContext {

    private final boolean inner;
    private final int realPrecision;
    private final DecimalFormat realFormat;
    private final int floatPrecision;
    private final DecimalFormat floatFormat;

    private ToStringContext(boolean inner, int realPrecision, DecimalFormat realFormat, int floatPrecision, DecimalFormat floatFormat) {
        this.inner = inner;
        this.realPrecision = realPrecision;
        this.realFormat = realFormat;
        this.floatPrecision = floatPrecision;
        this.floatFormat = floatFormat;
    }

    public ToStringContext(boolean inner, int precision) {
        this.inner = inner;
        this.realPrecision = precision > 0? Math.min(8, precision) : 8;
        this.realFormat = createDecimalFormat(realPrecision);
        this.floatPrecision = precision > 0? Math.min(17, precision) : 17;
        this.floatFormat = createDecimalFormat(floatPrecision);
    }

    public boolean isInner() {
        return inner;
    }

    public ToStringContext setInner(boolean inner) {
        if (inner == this.inner) {
            return this;
        }
        else {
            return new ToStringContext(inner, realPrecision, realFormat, floatPrecision, floatFormat);
        }
    }

    public StringBuilder floatingDecimalToString(StringBuilder sb, float f) {
        final String raw = realFormat.format(f);
        sb.append(trim(raw, realPrecision));
        return sb;
    }

    public StringBuilder floatingDecimalToString(StringBuilder sb, double d) {
        final String raw = floatFormat.format(d);
        sb.append(trim(raw, floatPrecision));
        return sb;
    }

    private DecimalFormat createDecimalFormat(int precision) {
        StringBuilder sbF = new StringBuilder();
        sbF.append("#0.");
        for (int i=0; i< precision; ++i) {
            sbF.append('0');
        }
        return new DecimalFormat(sbF.toString());
    }

    private String trim(String raw, int precision) {
        return StringUtils.stripEnd(raw, "0");
    }

}
