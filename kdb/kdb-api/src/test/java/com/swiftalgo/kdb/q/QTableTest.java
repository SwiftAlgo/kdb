package com.swiftalgo.kdb.q;

/*-
 * #%L
 * KDB+ API
 * %%
 * Copyright (C) 2017 SwiftAlgo Limited
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.swiftalgo.kdb.q.factory.DefaultQFactory;
import org.junit.Test;

import java.nio.ByteBuffer;

public class QTableTest extends AbstractQTest<QTable> {

    @Override
    QTable create(ByteBuffer b) {
        return new QTable(new DefaultQFactory(), b);
    }

    @Test
    public void basicTest() {
        //test(sourceB, "+`a`b`c!((1 2;3 4);(5 6;7 8);(9 10;11 12))");
        test(createBasicBuffer(), "+`a`b`c!(1 2i;3 4i;5 6i)");
    }

    private ByteBuffer createBasicBuffer() {
        final ByteBuffer b = ByteBuffer.allocate(87);
        b.put(QType.TABLE);
        b.put(QAttr.NONE_BYTE);
        b.put(QType.DICT);
        b.put(QType.SYMBOL_V);
        b.put(QAttr.NONE_BYTE);
        b.putInt(3);
        b.put((byte) 0x61); //`a
        b.put(QType.ZERO);
        b.put((byte) 0x62); //`b
        b.put(QType.ZERO);
        b.put((byte) 0x63); //`c
        b.put(QType.ZERO);
        b.put(QType.MIXED_V);
        b.put(QAttr.NONE_BYTE);
        b.putInt(3);
        b.put(QType.INT_V);
        b.put(QAttr.NONE_BYTE);
        b.putInt(2);
        b.putInt(1);
        b.putInt(2);
        b.put(QType.INT_V);
        b.put(QAttr.NONE_BYTE);
        b.putInt(2);
        b.putInt(3);
        b.putInt(4);
        b.put(QType.INT_V);
        b.put(QAttr.NONE_BYTE);
        b.putInt(2);
        b.putInt(5);
        b.putInt(6);
        return b;
    }

}
