package com.swiftalgo.kdb.q;

/*-
 * #%L
 * KDB+ API
 * %%
 * Copyright (C) 2017 SwiftAlgo Limited
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.junit.Test;

import java.nio.ByteBuffer;

public class QBooleanVTest extends AbstractQTest<QBooleanV> {

    @Test
    public void testBasicQBooleanV() {
        test(create(), "101b");
    }

    @Test
    public void testSortedQBooleanV() {
        test(createSorted(), "`s#011b");
    }

    @Override
    public QBooleanV create(ByteBuffer buffer) {
        return new QBooleanV(buffer);
    }

    private ByteBuffer create() {
        ByteBuffer buffer = ByteBuffer.allocate(9);
        buffer.put(QType.BOOLEAN_V);
        buffer.put(QAttr.NONE_BYTE);
        buffer.putInt(3);
        buffer.put(QType.ONE);
        buffer.put(QType.ZERO);
        buffer.put(QType.ONE);
        return buffer;
    }

    private ByteBuffer createSorted() {
        ByteBuffer buffer = ByteBuffer.allocate(9);
        buffer.put(QType.BOOLEAN_V);
        buffer.put(QAttr.SORTED_BYTE);
        buffer.putInt(3);
        buffer.put(QType.ZERO);
        buffer.put(QType.ONE);
        buffer.put(QType.ONE);
        return buffer;
    }

}
