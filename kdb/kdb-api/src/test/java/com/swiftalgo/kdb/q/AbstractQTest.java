package com.swiftalgo.kdb.q;

/*-
 * #%L
 * KDB+ API
 * %%
 * Copyright (C) 2017 SwiftAlgo Limited
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.nio.ByteBuffer;

import static org.junit.Assert.assertEquals;

public abstract class AbstractQTest<T extends QType> {

    abstract T create(ByteBuffer b);

    protected void test(ByteBuffer sourceB, String expected) {
        sourceB.flip();
        final T q = create(sourceB);
        assertEquals(0, sourceB.remaining());
        ByteBuffer testB = ByteBuffer.allocate(sourceB.capacity());
        q.toBinary(testB);
        sourceB.flip();
        testB.flip();
        assertEquals(sourceB, testB);
        final StringBuilder sb = new StringBuilder(expected.length());
        q.toString(sb, new ToStringContext(false, 0));
        assertEquals(expected, sb.toString());
    }
}
