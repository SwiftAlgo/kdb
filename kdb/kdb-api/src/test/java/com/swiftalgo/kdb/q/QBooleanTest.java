package com.swiftalgo.kdb.q;

/*-
 * #%L
 * KDB+ API
 * %%
 * Copyright (C) 2017 SwiftAlgo Limited
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.swiftalgo.util.ByteBufferException;
import org.junit.Test;

import java.nio.ByteBuffer;

public class QBooleanTest extends AbstractQTest<QBoolean> {

    @Override
    QBoolean create(ByteBuffer b) {
        return new QBoolean(b);
    }

    @Test
    public void booleanTrueTest() {
        final ByteBuffer trueB = createTrueBuffer();
        test(trueB, "1b");
    }

    @Test
    public void booleanFalseTest() {
        final ByteBuffer falseB = createFalseBuffer();
        test(falseB, "0b");
    }

    @Test(expected = ByteBufferException.class)
    public void booleanInvalidTest() {
        final ByteBuffer b = createInvalidBuffer();
        test(b, "0b");
    }

    private ByteBuffer createFalseBuffer() {
        ByteBuffer b = ByteBuffer.allocate(2);
        b.put(QType.BOOLEAN);
        b.put(QType.ZERO);
        return b;
    }

    private ByteBuffer createTrueBuffer() {
        ByteBuffer b = ByteBuffer.allocate(2);
        b.put(QType.BOOLEAN);
        b.put(QType.ONE);
        return b;
    }

    private ByteBuffer createInvalidBuffer() {
        ByteBuffer b = ByteBuffer.allocate(2);
        b.put(QType.BOOLEAN);
        b.put((byte) 2);
        return b;
    }

}
