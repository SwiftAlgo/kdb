package com.swiftalgo.util;

/*-
 * #%L
 * SwiftAlgo Util
 * %%
 * Copyright (C) 2017 SwiftAlgo Limited
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.nio.ByteBuffer;
import java.util.Objects;

public class ByteBufferException extends RuntimeException {

    private static final int BYTES_TO_PRINT = 1 << 7;

    private ByteBuffer buffer;

    public ByteBufferException(String message, ByteBuffer buffer) {
        super(message);
        this.buffer = Objects.requireNonNull(buffer);
    }

    public String toString() {
        final StringBuilder sb = new StringBuilder(super.toString());
        sb.append(String.format("ByteBuffer [pos=%d, limit=%d]", buffer.position(), buffer.limit()));
        buffer.mark();
        int i = 0;
        while (buffer.hasRemaining() && i < BYTES_TO_PRINT) {
            if (i % 16 == 0) {
                sb.append("\n").append(Integer.toHexString(i));
            }
            if (i % 8 == 0) {
                sb.append(" ");
            }
            sb.append(" ").append(Integer.toHexString(buffer.get()));
        }
        if (buffer.hasRemaining()) {
            sb.append(String.format("\nOmitted another %d bytes.", buffer.remaining()));
        }
        return sb.toString();
    }
}
