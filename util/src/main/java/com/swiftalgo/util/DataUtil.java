package com.swiftalgo.util;

/*-
 * #%L
 * SwiftAlgo Util
 * %%
 * Copyright (C) 2017 SwiftAlgo Limited
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.nio.ByteBuffer;

public class
        DataUtil {

    public static byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4) + Character.digit(s.charAt(i + 1), 16));
        }
        return data;
    }

    public static void hexStringToByteArray(String s, byte[] target, int targetIndex) {
        for (int i = 0; i < s.length(); i += 2) {
            target[targetIndex + (i / 2)] = (byte) ((Character.digit(s.charAt(i), 16) << 4) + Character.digit(s.charAt(i + 1), 16));
        }
    }

    public static StringBuilder byteArrayToHex(byte[] bb, StringBuilder sb) {
        return byteArrayToHex(bb, 0, bb.length, sb);
    }

    public static StringBuilder byteArrayToHex(byte[] bb, int beginIndex, int endIndex, StringBuilder sb) {
        for (int i = beginIndex; i < endIndex; ++i) {
            byteToHex(bb[i], sb);
        }
        return sb;
    }

    public static StringBuilder byteBufferToHex(ByteBuffer bb, StringBuilder sb) {
        while (bb.hasRemaining()) {
            byteToHex(bb.get(), sb);
        }
        return sb;
    }

    public static StringBuilder byteToHex(byte b, StringBuilder sb) {
        final char c1 = Character.forDigit(b >>> 4 & 0x0F, 16);
        final char c2 = Character.forDigit(b & 0x0F, 16);
        sb.append(c1).append(c2);
        return sb;
    }

    public static String byteToHex(byte b) {
        final char c1 = Character.forDigit(b >>> 4 & 0x0F, 16);
        final char c2 = Character.forDigit(b & 0x0F, 16);
        return new String(new char[]{c1, c2});
    }

    public static byte[] concat(byte[]... byteArrays) {
        int totalLength = 0;
        for (int i = 0; i < byteArrays.length; ++i) {
            totalLength += byteArrays[i].length;
        }
        final byte[] result = new byte[totalLength];
        int index = 0;
        for (int i = 0; i < byteArrays.length; ++i) {
            System.arraycopy(byteArrays[i], 0, result, index, byteArrays[i].length);
            index += byteArrays[i].length;
        }
        return result;
    }

}
